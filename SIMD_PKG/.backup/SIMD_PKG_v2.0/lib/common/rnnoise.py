#! /bin/python
# -*- coding:utf-8 -*-
import os, time
import sys
import wave
import struct
#import ffmpeg #pip install ffmpeg-python
import numpy as np #pip install numpy
import librosa #pip install librosa

def f(y, o, sr, cal_time):
    noise = []
    if cal_time == 0:
        c_time = 1
    else:
        c_time = cal_time

    l = min(y.shape[0], o.shape[0])
    for idx, i in enumerate(range(l//sr//c_time)):
        y_sum = sum(np.abs(y[i*sr:(i+c_time)*sr]))
        o_sum = sum(np.abs(o[i*sr:(i+c_time)*sr]))
        a=(y_sum - o_sum) / (o_sum + 100.) * 100.
        if a < 0:
            a = a * -1
        noise.append(a)

    if not cal_time == 0:
        return noise
    else:
        avg= sum(noise, 0.0) /len(noise)
        return avg


class NoiseCalculator:
    def __init__(self, log):
        self.log = log
        self.sr = 48000
        self.ac = 1
        self.encode = 's16le'
        self.cal_time = 0
        self.rnnoise_path = os.path.join('./rnnoise_tool')
        self.orig_path = os.path.join('./orig.pcm')
        self.wnoise_16k_wav_path = os.path.join('./with_noise_16k.wav')
        self.wnoise_wav_path = os.path.join('./with_noise.wav')
        self.wnoise_raw_path = os.path.join('./with_noise.raw')
        self.wnoise_48k_wav_path = os.path.join('./with_noise_48k.wav')
        self.rnoise_wav_path = os.path.join('./without_noise.wav')
        self.rnoise_raw_path = os.path.join('./without_noise.raw')

    def set_input_path(self, wnoise_16k_raw_path):
        tmp_buf = wnoise_16k_raw_path[:-8]
        self.orig_path = wnoise_16k_raw_path
        self.wnoise_16k_wav_path = os.path.join(tmp_buf+'_16k.wav')
        self.wnoise_wav_path = os.path.join(tmp_buf+'.wav')
        self.wnoise_raw_path = os.path.join(tmp_buf+'.raw')
        self.wnoise_48k_wav_path = os.path.join(tmp_buf+'_48k.wav')
        self.rnoise_wav_path = os.path.join(tmp_buf+'_rnoise.wav')
        self.rnoise_raw_path = os.path.join(tmp_buf+'_rnoise.raw')
        self.log.debug('w_16k_wav path :: ' + self.wnoise_16k_wav_path)
        self.log.debug('w_wav path :: ' + self.wnoise_wav_path)
        self.log.debug('w_raw path :: ' + self.wnoise_raw_path)
        self.log.debug('r_wav path :: ' + self.rnoise_wav_path)
        self.log.debug('r_raw path :: ' + self.rnoise_raw_path)

    def set_rnnoise_path(self, rnnoise_path):
        self.rnnoise_path = os.path.join(rnnoise_path)

    def set_param(self, sr, ac, encode, cal_time):
        if not sr == '':
            self.sr = sr
        if not ac == '':
            self.ac = ac
        if not encode == '':
            self.encode = encode
        if not cal_time == '':
            self.cal_time = cal_time

    def convert_wav_to_pcm(self, wnoise_wav_path, wnoise_raw_path, sr, ac, encode):
        command='ffmpeg -i ' + wnoise_wav_path + ' -ac ' + str(ac) +' -ar ' + str(sr) + ' -f ' + encode + ' ' + wnoise_raw_path
        process = os.popen(command)
        process.read()
        """
        wavfile=wave.open(wnoise_wav_path, 'rb')
        nchannels, sampwidth, framerate, nframes, comptype, compname =  wavfile.getparams()
        print(nchannels, sampwidth, nframes, comptype, compname)
        """

    def convert_wav_sr(self, wnoise_16k_wav_path, wnoise_wav_path, sr):
        command='ffmpeg -i ' + wnoise_16k_wav_path + ' -ar ' + str(sr) + ' ' + wnoise_wav_path
        self.log.debug(command)
        process = os.popen(command)
        process.read()

    def convert_pcm_to_wav(self, rnoise_raw_path, rnoise_wav_path, sr, ac, encode):
        with open(rnoise_raw_path, 'rb') as pcmfile:
            pcmdata = pcmfile.read()
            try:
                wavfile = wave.open(rnoise_wav_path, 'wb')
				#nchannel, sampwidth, framerate, nframes, comptype, compname
                wavfile.setparams((int(ac), 2*int(ac), int(sr), 0,'NONE', 'NONE'))
                wavfile.writeframes(pcmdata)
                wavfile.close()
            except:
                wavfile.close()

        """
        out, _ = (ffmpeg
            .input(rnoise_raw_path, format=encode, ac=str(ac), ar=str(sr))
            .output(rnoise_wav_path, format='wav')
            .run(capture_stdout=False)
            )
        """

    def remove_noise(self, rnnoise_path, wnoise_raw_path, rnoise_raw_path):
        #self.log.debug(rnnoise_path + ' ' + wnoise_raw_path + ' ' + rnoise_raw_path)
        process = os.popen(rnnoise_path + ' ' + wnoise_raw_path + ' ' + rnoise_raw_path)
        process.read()

    def calculate_noise(self, wnoise_wav_path, rnoise_wav_path, cal_time):
        input_wav, _ = librosa.load(wnoise_wav_path, sr=self.sr)
        output_wav, _ = librosa.load(rnoise_wav_path, sr=self.sr)
        noise = f (input_wav, output_wav, self.sr, cal_time)
        return noise 

    def calculate_noise_allstep(self, wnoise_16k_raw_path, cal_time):
        try:
            self.set_input_path(wnoise_16k_raw_path)
            self.set_param(self.sr, self.ac, self.encode, cal_time)
            self.convert_pcm_to_wav(wnoise_16k_raw_path, self.wnoise_16k_wav_path, 16000, self.ac, self.encode)
            self.convert_wav_sr(self.wnoise_16k_wav_path, self.wnoise_wav_path, self.sr)
            self.convert_wav_to_pcm(self.wnoise_wav_path, self.wnoise_raw_path, self.sr, self.ac, self.encode)
            self.convert_pcm_to_wav(self.wnoise_raw_path, self.wnoise_48k_wav_path, self.sr, self.ac, self.encode)
            self.remove_noise(self.rnnoise_path, self.wnoise_raw_path, self.rnoise_raw_path)
            self.convert_pcm_to_wav(self.rnoise_raw_path, self.rnoise_wav_path, self.sr, self.ac, self.encode)
            noise = self.calculate_noise(self.wnoise_48k_wav_path, self.rnoise_wav_path, self.cal_time)
        except Exception as e:
            self.log.error(e)

        try:
            os.remove(self.orig_path)
        except Exception as e:
            self.log.error(e)
        try:
            os.remove(self.wnoise_16k_wav_path)
        except Exception as e:
            self.log.error(e)
        try:
            os.remove(self.wnoise_raw_path)
        except Exception as e:
            self.log.error(e)
        try:
            os.remove(self.wnoise_48k_wav_path)
        except Exception as e:
            self.log.error(e)
        try:
            os.remove(self.rnoise_raw_path)
        except Exception as e:
            self.log.error(e)
        try:
            os.remove(self.rnoise_wav_path)
        except Exception as e:
            self.log.error(e)
        return self.wnoise_wav_path, sum(noise, 0.0) / len(noise)
        
    def calculate_noise_test(self, wnoise_wav_path, cal_time):
        try:
            self.set_input_path(wnoise_wav_path)
            self.set_param(self.sr, self.ac, self.encode, cal_time)
            self.convert_pcm_to_wav(self.wnoise_raw_path, self.wnoise_48k_wav_path, self.sr, self.ac, self.encode)
        except Exception as e:
            self.log.error(e)

