#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import struct
import binascii
import hashlib
import base64
from base64 import b64decode
from base64 import b64encode
from base64 import b64decode
from Crypto.Cipher import AES #pip install pycrypto
from Crypto import Random

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(0)
unpad = lambda s: s[:-ord(s[len(s)-1:])]

class MyCipher():
	def __init__(self, log, key):
		self.log = log	
		self.bs = 32
		self.key = hashlib.sha256(key.encode()).digest()
	
	def pad(self, s):
		return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

	def unpad(self, s):
		return s[:-ord(s[len(s)-1:])]

	def aes_encrypt_str(self, raw):
		raw = self.pad(raw)
		iv = Random.new().read(AES.block_size)
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return base64.b64encode(iv + cipher.encrypt(raw))

	def aes_decrypt_str(self, enc):
		enc = base64.b64decode(enc)
		iv = enc[:AES.block_size]
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return self.unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
	
	def aes_decrypt_file(self, in_filename, out_filename, chunksize=24*1024):
		with open(in_filename, 'rb') as infile:
			origsize = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
			iv = infile.read(16)
			decryptor = AES.new(self.key, AES.MODE_CBC, iv)
			with open(out_filename, 'wb') as outfile:
				while True:
					chunk = infile.read(chunksize)
					if len(chunk) == 0:
						break
					outfile.write(decryptor.decrypt(chunk))
				outfile.truncate(origsize)
		return

	def aes_encrypt_file(self, in_filename, out_filename=None, del_infile=True, chunksize=65536):
		if not out_filename:
			out_filename = in_filename + '.enc'
		iv = 'initialvector123'
		encryptor = AES.new(self.key, AES.MODE_CBC, iv)
		filesize = os.path.getsize(in_filename)
		with open(in_filename, 'rb') as infile:
			with open(out_filename, 'wb') as outfile:
				outfile.write(struct.pack('<Q', filesize))
				outfile.write(iv)
				while True:
					chunk = infile.read(chunksize)
					if len(chunk) == 0:
						break
					elif len(chunk) % 16 != 0:
						chunk += ' ' * (16 - len(chunk) % 16)
	 				outfile.write(encryptor.encrypt(chunk))
		if del_infile:
			os.remove(in_filename)
		return


if __name__ == "__main__": #테스트 코드
	cipher = MyCipher("test_key", None)
	cipher.aes_encrypt_file("test.py", "test.py.enc")
	cipher.aes_decrypt_file("test.py.enc", "test.py.dec")
	tmp = cipher.aes_encrypt_str("hello world")
	print tmp
	tmp = cipher.aes_decrypt_str(tmp)
	print tmp
