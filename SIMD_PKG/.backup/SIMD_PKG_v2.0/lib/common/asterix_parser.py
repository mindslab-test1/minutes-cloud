#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# INSTALL MODULE
#####################################################################################
# ASTERIX Module : https://pypi.org/project/pyzmq
import asterix

class AsterixParser:
	def __init__(self, log):
		self.log = log
		self.cat11 = {}
		self.cat21 = {}
		self.cat48 = {}
	
	def get_category(self, cat_no):
		if cat_no == 11: return self.cat11
		elif cat_no == 21: return self.cat21
		elif cat_no == 48: return self.cat48
		else: return None
	
	def decode_message(self, message, cat_no, indra_offset):
		cur_cat = -1
		result = []
		if len(message) <= indra_offset:
			return
		asterix_msg = message[indra_offset:]
		if asterix_msg[0] != cat_no:
			return

		parsed = asterix.parse(asterix_msg)
		#formatted = asterix.describe(parsed)
		#self.log.debug(formatted)
		self.log.debug(parsed)
		for i, packet in enumerate(parsed):
			for item in packet.items():
				###########################################################################
				# Category가 제일 첫 Item
				#--------------------------------------------------------------------------
				#| CAT=%d | LEN | FSPEC1 | ITEM1 | ITEM2 | | FSPEC2 | ITEM1 | ITEM2 | ...
				#--------------------------------------------------------------------------
				if item[0] == 'category':
					cur_cat = item[1]
					self.fill_default(cat_no)
				if cur_cat is cat_no:
					formatted = asterix.describe(parsed)
					self.log.critical(formatted)
					self.decode_item(item, cat_no)
			tmp_dict = self.get_category(cat_no)
			tmp_dict.update({'category':cat_no})
			result.append(tmp_dict)
		return result
	
	def fill_default(self, cat_no):
		if cat_no == 11:
			self.cat11 = {'category':0, 'cs':"UNKNOWN", 'tid':"UNKNOWN", 'reg':"UNKNOWN", 'tac':"UNKNOWN", 
						  'dep':"UNKNOWN", 'dst':"UNKNOWN",
						  'sac':-9999, 'sic':-9999,
						  'plat':-9999, 'plon':-9999, 'px':-9999, 'py':-9999, 'ptheta':-9999, 'prho':-9999, 'lat':-9999, 'lon':-9999, 
						  'alt':-9999, 'vx':-9999, 'vy':-9999, 'toti':-9999}
		if cat_no == 48:
			self.cat48 = {'category':0, 'cs':"UNKNOWN", 'tid':"UNKNOWN", 'reg':"UNKNOWN", 'tac':"UNKNOWN", 
						  'dep':"UNKNOWN", 'dst':"UNKNOWN",
						  'sac':-9999, 'sic':-9999,
						  'plat':-9999, 'plon':-9999, 'px':-9999, 'py':-9999, 'ptheta':-9999, 'prho':-9999, 'lat':-9999, 'lon':-9999, 
						  'alt':-9999, 'vx':-9999, 'vy':-9999, 'toti':-9999}

	def decode_item(self, item, cat_no):
		tmp = item[1]
		#CATEGORY:048-ARTS
		if cat_no == 48 and item[0] == 'I140': #Time Of Day
			self.cat48.update({'toti' : tmp['ToD']['val']})
		elif cat_no == 48 and item[0] == 'I042': #Calculated Track Position/Cartesian
			self.cat48.update({'px' : tmp['X']['val'], 'py' : tmp['Y']['val']})
		elif cat_no == 48 and item[0] == 'I040': #Measured Position In Polar Co-ordinates
			self.cat48.update({'ptheta': tmp['THETA']['val'], 'prho' : tmp['RHO']['val']})
		elif cat_no == 48 and item[0] == 'I090': #Flight Level
			self.cat48.update({'alt' : tmp['FL']['val']})
		elif cat_no == 48 and item[0] == 'I200': #Calculated Track Velocity in Polar Co-ordinates
			self.cat48.update({'vx' : tmp['CGS']['val'], 'vy' : tmp['CHdg']['val']})
		elif cat_no == 48 and item[0] == 'I240': #Target Identification
			self.cat48.update({'tid' : str(tmp['TId']['val'])})
			self.cat48.update({'cs' : str(tmp['TId']['val'])})
		elif cat_no == 48 and item[0] == 'I250': #Mode S Data
			pass
		elif cat_no == 48 and item[0] == 'I010':
			self.cat48.update({'sac' : tmp['SAC']['val']})
			self.cat48.update({'sic' : tmp['SIC']['val']})

		#CATEGORY:011-ASDE
		elif cat_no == 11 and item[0] == 'I140': #Time of Track Information
			self.cat11.update({'toti' : tmp['ToT']['val']})
		elif cat_no == 11 and item[0] == 'I042': #Calculated Track Position/Cartesian
			self.cat11.update({'px' : tmp['X']['val'], 'py' : tmp['Y']['val']})
		elif cat_no == 11 and item[0] == 'I041': #Calculated Position In WGS-84 Co-ordinates
			self.cat11.update({'plat': tmp['Lat']['val'], 'plon' : tmp['Lon']['val']})
		elif cat_no == 11 and item[0] == 'I390': #Flight Plan Related Data(CallSign)
			try:
				self.cat11.update({'cs' : str(tmp['CSN']['CS']['val'])}) #(CallSign)
			except:
				pass
			try:
				self.cat11.update({'tac' : str(tmp['TAC']['TYPE']['val'])}) #(TypeOfAircraft)
			except:
				pass
		elif cat_no == 11 and item[0] == 'I245': #Target Identification
			self.cat11.update({'tid' : str(tmp['TId']['val'])})
		elif cat_no == 11 and item[0] == 'I202': #Calculated Track Velocity/Cartesian
			self.cat11.update({'vx' : tmp['Vx']['val'], 'vy' : tmp['Vy']['val']})
	
