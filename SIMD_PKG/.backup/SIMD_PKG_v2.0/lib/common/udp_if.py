# -*- coding: utf-8 -*-
import socket
import struct

'''
[모듈 설명]
UDP 패킷을 받아 내부 버퍼에 데이터를 리턴한다
Blocking 모드로 동작한다.
'''

BUF_SIZE=212992

class UdpReceiver():
	def __init__(self, log, myip, myport, multicast_ip=None):
		self.log = log
		self.myip = myip
		self.myport = myport
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.multicast_ip = multicast_ip
		if self.multicast_ip:
			mreq = struct.pack("=4sl", socket.inet_aton(self.multicast_ip), socket.INADDR_ANY)
			self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

	def bind(self):
		self.sock.bind((self.myip, self.myport))
		self.log.critical("UdpReceiver:: bind() SUCC -> <%s:%d>" %(self.myip, self.myport))
	
	def receive_message(self):
		buf = self.sock.recv(BUF_SIZE)
		message = bytearray(buf)
		return message
	
	def disconnect(self):
		self.log.critical("UdpReceiver:: disconnect()")
		self.sock.close()

class UdpSender():
	def __init__(self, log, myip=None, myport=None, multicast_ip=None):
		self.log = log
		self.myip = myip
		self.myport = myport
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
		self.multicast_ip = multicast_ip
		if self.multicast_ip:
			self.sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)

	def bind(self):
		if self.myip and self.myport:
			self.sock.bind((self.myip, self.myport))
			self.log.critical("UdpSender:: bind() SUCC -> <%s:%d>" %(self.myip, self.myport))
	
	def send(self, msg, ip, port):
		self.sock.sendto(msg, (ip, port))

	def disconnect(self):
		self.log.critical("UdpSender:: disconnect()")
		self.sock.close()
