#! /bin/python
# -*- coding:utf-8 -*-
import os, time
import sys
import wave
import struct
#import ffmpeg #pip install ffmpeg-python
import numpy as np #pip install numpy
import librosa #pip install librosa
import common.logger as logger
import multiprocessing_logging

class SoundFileConverter:
	def __init__(self, log):
		self.log = log

	# WAV을 PCM 파일로 변환한다. Input:WAV.SR 과 Output:PCM.SR은 동일해야 한다. ex) 16K WAV -> 16K PCM
	def convert_wav_to_pcm(self, ipath, opath, sr=48000, ac=1, encode='s16le'):
		self.log.debug("wav({}) -> pcm({}): {}" .format(ipath, opath, sr))
		command='ffmpeg -i ' + ipath + ' -ac ' + str(ac) +' -ar ' + str(sr) + ' -f ' + encode + ' ' + opath
		process = os.popen(command)
		process.read()
        """
        wavfile=wave.open(ipath, 'rb')
        nchannels, sampwidth, framerate, nframes, comptype, compname =  wavfile.getparams()
        print(nchannels, sampwidth, nframes, comptype, compname)
        """

	# PCM을 WAV 파일로 변환한다. Input:PCM.SR 과 Output:WAV.SR은 동일해야 한다. ex) 16K PCM -> 16K WAV
	def convert_pcm_to_wav(self, ipath, opath, sr=48000, ac=1, encode='s16le'):
		self.log.debug("pcm({}) -> wav({}): {}" .format(ipath, opath, sr))
		with open(ipath, 'rb') as pcmfile:
			pcmdata = pcmfile.read()
			try:
				wavfile = wave.open(opath, 'wb')
				#nchannel, sampwidth, framerate, nframes, comptype, compname
				wavfile.setparams((int(ac), 2*int(ac), int(sr), 0,'NONE', 'NONE'))
				wavfile.writeframes(pcmdata)
				wavfile.close()
				return opath
			except Exception as e:
				self.log.error("convert_pcm_to_wav:: {}" .format(e))
				wavfile.close()
				return None
        """
        out, _ = (ffmpeg
            .input(ipath, format=encode, ac=str(ac), ar=str(sr))
            .outathrnoise_wav_path, format='wav')
            .run(capture_stdout=False)
            )
        """

	# WAV 파일의 SR 값을 변경한다.
	def convert_wav_sr(self, ipath, opath, sr=48000):
		self.log.debug("wav({}) -> wav({}): sample_rate:{}" .format(ipath, opath, sr))
		command='ffmpeg -i ' + ipath + ' -ar ' + str(sr) + ' ' + opath
		self.log.debug(command)
		process = os.popen(command)
		process.read()
		return opath
	
	# PCM 파일의 잡음을 제거한다.
	def remove_noise(self, rnnoise_path, input_pcm, output_pcm):
		self.log.debug("{}: {} -> {}" .format(rnnoise_path, input_pcm, output_pcm))
		rnnoise_path = os.path.join(rnnoise_path)
		process = os.popen(rnnoise_path + ' ' + input_pcm + ' ' + output_pcm)
	 	process.read()
	 	return output_pcm

	def split_file(start, end, ipath, opath):
		return
