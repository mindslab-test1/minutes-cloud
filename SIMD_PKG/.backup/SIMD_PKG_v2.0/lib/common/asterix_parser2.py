#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON MODULE
#####################################################################################
import json
import binascii

#####################################################################################
# INSTALL MODULE
#####################################################################################
# ASTERIX Module : https://pypi.org/project/pyzmq
import asterix

class AsterixParser:
	def __init__(self, log):
		self.log = log
		self.hexa_data = {}
		self.result = []
		#self.cat001_parser = json.loads(open("/srv/maum/etc/asterix_cat001_t.conf").read())
		self.cat001_parser = json.loads(open("/srv/maum/etc/asterix_cat001.conf").read())
		self.cat011_parser = json.loads(open("/srv/maum/etc/asterix_cat011.conf").read())
		self.cat062_parser = json.loads(open("/srv/maum/etc/asterix_cat062.conf").read())
	
	def decode_message(self, message, cat_no, indra_offset):
		##################################################################################
		# Category가 제일 첫 Byte
		#---------------------------------------------------------------------------------
		#| CAT=%d | LEN | FSPEC1 | RECORD1 | FSPEC2 | RECORD2 | ......
		#---------------------------------------------------------------------------------

		message = message[indra_offset:]

		# Parse Header
		total_len = len(message)
		self.log.debug("udp_hex: {}" .format(binascii.b2a_hex(message)))
		self.log.debug("udp_len: %d" %(total_len))
		self.log.debug("asterix_category: %02x (len:1)" %(message[0])) # First 1 Octet => Category
		self.log.debug("asterix_total_length: %02x %02x (len:2)" %(message[1], message[2])) # 2 Octet => Total Length

		if message[0] != cat_no:
			self.log.error("decode_message:: received invalid category fail <%d>" %(message[0]))
			return

		# Parse FSPEC
		index = 0
		offset = 3 # category(1) + len(2)
		self.result = []
		while offset < total_len:
			tmp_dict = {} # Init by records...
			self.hexa_data = {} 
			if index > 500:
				self.log.error("decode_message:: process hanging fail <%d>" %(index))
				break; # 프로세스 행업 방지
			fspec = self.get_extent_octet(message, offset) # Extract Fspec
			offset = self.parse_records(cat_no, fspec, message, offset + len(fspec), index) # Parsing records to self.hexa_data
			self.log.debug(self.hexa_data)
			tmp_dict = self.calc_data(cat_no, self.hexa_data)
			self.result.append(tmp_dict)
			index += 1

		return self.result
	
	def convert_hex_to_signed(self, hex_array, byte):
		data = int(binascii.hexlify(hex_array), 16)
		mask =  int('0b00000000000000001111111111111111', 2)
		if byte == 4:
			msb = int('0b10000000000000000000000000000000', 2)
		elif byte == 2:
			msb = int('0b00000000000000001000000000000000', 2)

		if data & msb:
			data = (~data)
			if byte == 2:
				data = data & mask
			data = data * -1

		return data


	
	def calc_data(self, cat_no, hexa_data):
		# ASDE
		tmp_dict = {}
		tmp_dict.update({'category':cat_no})
		if cat_no == 11:
			tmp_dict.update({'cs':str(hexa_data.get('390/CSN', "UNKNOWN"))}) # hex -> string
			#tmp_dict.update({'ast':str(hexa_data.get('390/AST', "UNKNOWN"))}) # hex -> string
			#self.log.debug("ast:%s" %(tmp_dict['ast']))
			tmp_dict.update({'tid':str(hexa_data.get('245', "UNKNOWN"))}) # hex -> string
			tmp_dict.update({'reg':"UNKNOWN"})
			tmp_dict.update({'tac':str(hexa_data.get('390/TAC', "UNKNOWN"))}) # hex -> string
			# hex: 8byte -> position (x,y) 
			wgs_84 = hexa_data.get('041', -9999) 
			if wgs_84 == -9999:
				tmp_dict.update({'plat':-9999})
				tmp_dict.update({'plon':-9999})
			else:
				plat = wgs_84[0:4]
				plon = wgs_84[4:8]
				plat = self.convert_hex_to_signed(plat, 4)
				plon = self.convert_hex_to_signed(plon, 4)
				plat =  plat * (180) / float(2**31)
				plon =  plon * (180) / float(2**31)
				tmp_dict.update({'plat':plat})
				tmp_dict.update({'plon':plon})
				self.log.debug("plat:%f, plon:%f" %(plat, plon))
			# hex: 4byte -> position (x,y) 
			cartesian = hexa_data.get('042', -9999) 
			if cartesian == -9999:
				tmp_dict.update({'p_x':-9999})
				tmp_dict.update({'p_y':-9999})
			else:
				p_x = cartesian[0:2]
				p_y = cartesian[2:4]
				p_x = self.convert_hex_to_signed(p_x, 2)
				p_y = self.convert_hex_to_signed(p_y, 2)
				tmp_dict.update({'px':p_x})
				tmp_dict.update({'py':p_y})
				self.log.debug("p_x:%d, p_y:%d" %(p_x, p_y))

			#alt
			altitude = hexa_data.get('093', -9999) 
			if altitude == -9999:
				tmp_dict.update({'alt':-9999})
			else:
				altitude = int(binascii.hexlify(altitude), 16) & int('0b0111111111111111', 2)
				altitude = altitude / 25
				tmp_dict.update({'alt':altitude})
				self.log.debug("alt:%f" %(altitude))
				

			velocity = hexa_data.get('202', -9999) 
			if velocity == -9999:
				tmp_dict.update({'v_x':-9999})
				tmp_dict.update({'v_y':-9999})
			else:
				v_x = velocity[0:2]
				v_y = velocity[2:4]
				v_x = self.convert_hex_to_signed(v_x, 2)
				v_y = self.convert_hex_to_signed(v_y, 2)
				v_x = float(v_x * 0.25)
				v_y = float(v_y * 0.25)
				tmp_dict.update({'vx':v_x})
				tmp_dict.update({'vy':v_y})
				self.log.debug("v_x:%f, v_y:%f" %(v_x, v_y))
			#toti
			toti = hexa_data.get('140', -9999) 
			if toti == -9999:
				tmp_dict.update({'toti':-9999})
			else:
				toti = int(binascii.hexlify(toti), 16) / 128.0
				tmp_dict.update({'toti':toti})
				self.log.debug("toti:%f" %(toti))
			self.log.debug(tmp_dict)
		elif cat_no == 01:
			pass
		elif cat_no == 21:
			pass
		return tmp_dict

	def parse_records(self, cat_no, fspec, message, offset, index):
		parser = []
		frn_list = []
		frn_list = self.fill_frn_list(fspec, frn_list)
		self.log.debug("\n")
		self.log.debug("asterix_record: {}" .format(index))
		self.log.debug("fspec: {} (len:{})" .format(binascii.b2a_hex(fspec), len(fspec)))

		parser = self.get_parser(cat_no) # Get Parser from category numbers...
		for frn in frn_list:
			self.log.debug("  -> frn(%d) parsing...  => offset(%d)" %(frn, offset)) 
			item_len = self.parse_item(parser, frn, message, offset) # Parsing Items, returns Item Length
			offset += item_len # Parse next Items in same record
		return offset
	
	def fill_frn_list(self, fspec, frn_list):
		for oct_idx, octet in enumerate(fspec):
			if octet & 128:
				frn_list.append(1 + (oct_idx * 7))
			if octet & 64:
				frn_list.append(2 + (oct_idx * 7))
			if octet & 32:
				frn_list.append(3 + (oct_idx * 7))
			if octet & 16:
				frn_list.append(4 + (oct_idx * 7))
			if octet & 8:
				frn_list.append(5 + (oct_idx * 7))
			if octet & 4:
				frn_list.append(6 + (oct_idx * 7))
			if octet & 2:
				frn_list.append(7 + (oct_idx * 7))
		return frn_list
	
	def parse_item(self, parser, frn, message, offset):
		self.log.debug("    -> desc: I%s (%s)" %(parser['Parser'][frn]['id'], parser['Parser'][frn]['desc']))
		self.log.debug("    -> type: %s (%s)" %(parser['Parser'][frn]['type'], parser['Parser'][frn]['len']))

		start_offset = offset

		# Parse Data from types
		item_len = 0
		if parser['Parser'][frn]['type'] == "fix":
			item_len = parser['Parser'][frn]['len']
			key = parser['Parser'][frn]['id']
			val = message[offset:offset+item_len]
			self.hexa_data.update({key:val})
		elif parser['Parser'][frn]['type'] == "extent":
			extent = self.get_extent_octet(message, offset)
			item_len = len(extent)
			key = parser['Parser'][frn]['id']
			val = message[offset:offset+item_len]
			self.hexa_data.update({key:val})
		elif parser['Parser'][frn]['type'] == "rep":
			key = parser['Parser'][frn]['id']
			item_len = message[offset] * parser['Parser'][frn]['len']
			val = message[offset:offset+item_len]
			self.hexa_data.update({key:val})
		elif parser['Parser'][frn]['type'] == "explicit":
			item_len = message[offset]
			key = parser['Parser'][frn]['id']
			val = message[offset:offset+item_len]
			self.hexa_data.update({key:val})
		elif parser['Parser'][frn]['type'] == "compound":
			ext_subfield_list = []
			pri_subfield = self.get_extent_octet(message, offset)
			offset += len(pri_subfield)
			self.log.debug("      -> compound_bit: {} (len:{}) => offset({})" 
							.format(binascii.b2a_hex(pri_subfield), len(pri_subfield), offset))
			ext_subfield_list = self.fill_subfield_list(pri_subfield, ext_subfield_list)
			ext_subfield_list.sort(reverse=True)
			for sub_idx in ext_subfield_list:
				repeat_cnt = 1
				if parser['Parser'][frn]['bit_table'][sub_idx]['type'] == "fix":
					tmp_len = parser['Parser'][frn]['bit_table'][sub_idx]['length']
				elif parser['Parser'][frn]['bit_table'][sub_idx]['type'] == "rep":
					repeat_cnt = message[offset]
					tmp_len = (message[offset] * parser['Parser'][frn]['bit_table'][sub_idx]['length']) + 1
				elif parser['Parser'][frn]['bit_table'][sub_idx]['type'] == "fx":
					continue

				self.log.debug("        -> bit: {}, type:{}({} X {}), value:{}, offset:{}" 
					.format(parser['Parser'][frn]['bit_table'][sub_idx]['bit'],
							parser['Parser'][frn]['bit_table'][sub_idx]['type'],
							parser['Parser'][frn]['bit_table'][sub_idx]['length'], repeat_cnt,
							binascii.b2a_hex(message[offset:offset+tmp_len]), offset))
				key = parser['Parser'][frn]['bit_table'][sub_idx]['id']
				val = message[offset:offset+tmp_len]
				self.hexa_data.update({key:val})
				offset += tmp_len
			item_len = (offset - start_offset)
		
		# Save Datas
		val = message[start_offset:start_offset+item_len]
		self.log.debug("    -> value: {}" .format(binascii.b2a_hex(val)))
		return item_len

	def fill_subfield_list(self, pri_field, subfield_list):
		for oct_idx, octet in enumerate(pri_field):
			if len(pri_field) == 1:
				base_oct = 1
			else:
				base_oct = len(pri_field) - 1 - oct_idx
			if octet & 128:
				subfield_list.append(8 + (base_oct * 8))	
			if octet & 64:
				subfield_list.append(7 + (base_oct * 8))	
			if octet & 32:
				subfield_list.append(6 + (base_oct * 8))	
			if octet & 16:
				subfield_list.append(5 + (base_oct * 8))	
			if octet & 8:
				subfield_list.append(4 + (base_oct * 8))	
			if octet & 4:
				subfield_list.append(3 + (base_oct * 8))	
			if octet & 2:
				subfield_list.append(2 + (base_oct * 8))	
			if octet & 1:
				subfield_list.append(1 + (base_oct * 8))	
		return subfield_list

	def get_parser(self, cat_no):
		if cat_no == 1: return self.cat001_parser
		elif cat_no == 11: return self.cat011_parser
		elif cat_no == 62: return self.cat062_parser

	def get_extent_octet(self, message, start_offset):
		offset = start_offset
		while message[offset] & 1:
			offset += 1
		return message[start_offset:offset+1]
