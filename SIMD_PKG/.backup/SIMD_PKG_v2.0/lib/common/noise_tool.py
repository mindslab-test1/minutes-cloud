#!/usr/bin/python
# -*- coding:utf-8 -*-

import os
import sys

import grpc
import numpy as np
import scipy.io.wavfile as wav
import torch

sys.path.insert(0, '/srv/maum/lib/python')
from maum.brain.dap.cnnoise_pb2 import CnnoiseRequest
from maum.brain.dap.cnnoise_pb2_grpc import DapCnnoiseStub
from maum.brain.dap.cnnoise_pb2_grpc import DapCnnoiseServicer
from maum.brain.dap.cnnoise.utils.utils import read_wav

#exe_path = os.path.realpath(sys.argv[0])
#bin_path = os.path.dirname(exe_path)
#lib_path = os.getenv('MAUM_ROOT') + '/lib/python'
# lib_path = os.path.realpath(bin_path + '/../lib/python')
#sys.path.append(lib_path)

#from common.config import Config
#from maum.brain.stt import stt_pb2
##from maum.brain.stt import stt_pb2_grpc
#from maum.common import lang_pb2
#from maum.common import types_pb2
#import getopt

#def cut_wav_file(input_file, total_sec) :
#    result=list()
#    for idx in range(int(total_sec/30)) :
#        tmp_wav=input_file + str(idx) +'.wav'
#        command='ffmpeg -i ' + input_file  + ' -ss ' + str(idx*30) + ' -t 30' + ' -acodec copy ' +  tmp_wav 
#        print(int(total_sec/30 + 1))
#        print(tmp_wav)
#        print(command)
#        process = os.popen(command)
#        process.read()
#        result.append(tmp_wav)
#	
#    return result

def cut_wav_file(input_file, total_sec) :
	chunk_size = 30
	result=list()
	result_wav=list()

    #input_wav=np.memmap(input_file, dtype=dtype, mode='r')
	#idx=0
	#while True :
	#	with wav.open(input_file) as f :
	#		nframes=f.getnframes()
	#		if nframes :
	#			data=f.readframes(chunk_frames)
	#			tmp_file = input_file +str(idx) + '.wav'
	#			wav.write(tmp_file, 16000, data);
	#			result.append(tmp_file)
	#			idx=idx+1
	#		else : 
	#			break

	split_at_timestamp=30
	rate, input_wav = wav.read(input_file)
	split_at_frame = rate * split_at_timestamp
	if len(input_wav)%split_at_frame == 0 :
		file_count=int(len(input_wav)/split_at_frame)
	else :
		file_count=int(len(input_wav)/split_at_frame) + 1
	print (file_count)

	for i in range(file_count) :
		tmp_file = input_file +str(i) + '.wav'
		wav.write(tmp_file, rate, input_wav[(split_at_frame * i) : (split_at_frame * (i+1))-1])
		#print('wav.write input_wav[({}):({})]' .format(split_at_frame*i, (split_at_frame*(i+1)) -1))
		result.append(tmp_file)
	return result


def cnnoise(input_file, output_file, total_sec) :
    print(" in:[{}], out [{}]" .format(input_file, output_file))
    input_list=cut_wav_file(input_file, total_sec);
    result=list()
    for input_file_ in input_list :
        try :
            with open(input_file_, "rb") as f:
                with grpc.insecure_channel("127.0.0.1:59000") as channel:
                    stub = DapCnnoiseStub(channel)
                    #chunk_size = 224000
                    #print("chunk_size {}" .format(chunk_size))
                    #i=0
                    #while True :
                    #    i=i+1
                    #    input_wav=f.read(chunk_size) 
                    #    if input_wav :
                    #        print("[{}] ~ [{}]" .format(chunk_size*(i-1), chunk_size*(i)))
                    #        a = stub.Cnnoise(CnnoiseRequest(input_wav=input_wav))
                    #        result = list(a.output_wav)
                    #        print("complete part")
                    #        wav.write(output_file, 16000, np.asarray(result, 'float32'));
                    #    else :
                    #        break

                    response= stub.Cnnoise(CnnoiseRequest(input_wav=f.read()))
                    #result = list(response.output_wav)
                    #print(list(response.output_wav))
                    result.extend(list(response.output_wav))
                    print("thread: CNNOISE SUCCESS. <>" )
                    os.remove(input_file_)

        except Exception as e:
            print("thread: CNNOISE FAIL. <{}>" .format(e))
            return False

    wav.write(output_file, 16000, np.asarray(result, 'float32'));
    #wav.write(output_file, 16000, np.asarray(result_sum, 'float32'));
    print("thread: CNNOISE COMPLETE. <>" )
    return True

if __name__ == "__main__":
    cnnoise("./5.wav", "./5_cnnoise.wav", 60);
