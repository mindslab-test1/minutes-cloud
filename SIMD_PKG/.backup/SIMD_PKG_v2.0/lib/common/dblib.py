#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os, logging, time
import pymysql
import cx_Oracle
from common.config import Config

#db.type = mysql ; mysql or oracle
#db.pri_host=192.168.1.113
#db.pri_port=3306
#db.sec_host=192.168.1.114
#db.sec_port=3306
#db.user=root
#db.passwd=Msl1234~
#db.sid=MINDS
#db.encode=utf8

class DBLib:
	# DB 타입에 따라 DB정보를 초기화: 설정파일을 통해 초기화할 경우에만 2중화 기능을 수행한다.
	def __init__(self, log, conf_path='dbconf.conf'):
		conf = Config()
		conf.init(conf_path)
		self.log = log
		self.db_type = conf.get('db.type')
		self.host1 = conf.get('db.pri_host')
		self.port1 = conf.get('db.pri_port')
		self.host2 = conf.get('db.sec_host')
		self.port2 = conf.get('db.sec_port')
		self.user = conf.get('db.user')
		self.passwd = conf.get('db.passwd')
		self.sid = conf.get('db.sid')
		self.encode = conf.get('db.encode')
		self.db_conn = None
		self.cursor = None
		print("Oracle:: db.type       [{}]" .format(self.db_type))
		print("Oracle:: db.pri_host   [{}]" .format(self.host1))
		print("Oracle:: db.pri_post   [{}]" .format(self.port1))
		print("Oracle:: db.sec_host   [{}]" .format(self.host2))
		print("Oracle:: db.sec_post   [{}]" .format(self.port2))
		print("Oracle:: db.user       [{}]" .format(self.user))
		print("Oracle:: db.passwd     [{}]" .format(self.passwd))
		print("Oracle:: db.sid        [{}]" .format(self.sid))
		print("Oracle:: db.encode     [{}]" .format(self.encode))
		return
	
	def mysql_connect(self, host, port):
		db_connect_info = self.user + '@' + host + '/' + self.sid
		self.db_conn = pymysql.connect(host=host, port=port, user=self.user, password=self.passwd, db=self.sid, charset=self.encode)
		self.cursor = self.db_conn.cursor(pymysql.cursors.DictCursor)
		self.log.critical("MySql:: connect() SUCC -> %s", db_connect_info)
		return True

	def oracle_connect(self, host, port):
		db_connect_info = self.user + '/' + self.passwd + '@' + host + ':' + port + '/' + self.sid
		#self.db_conn = cx_Oracle.connect(db_connect_info, charset=self.encode)
		self.db_conn = cx_Oracle.connect(db_connect_info, encoding=self.encode)
		self.cursor = self.db_conn.cursor()
		self.log.critical("Oracle:: connect() SUCC -> %s", db_connect_info)
		return True

	def connect(self):
		#if self.db_type is 'oracle':
		if self.db_type == 'oracle':
			print("Oracle:: db.type       [{}]" .format(self.db_type))
			self.oracle_connect(self.host1, self.port1)
		else:
			print("Mysql:: db.type       [{}]" .format(self.db_type))
			self.mysql_connect(self.host1, int(self.port1))
		return

	def disconnect(self):
		try:
			self.cursur.close()
			self.db_conn.close()
		except Exception as e:
			self.log.error("[DBLIB] disconnect() fail <%s>", e)
			pass
		self.log.critical("[DBLIB] disconnect()")
		return

	def reconnect(self):
		self.disconnect()
		for i in range(6):
			try:
				if self.db_type is 'oracle':
					if i % 2 == 0:
						rc = self.oracle_connect(self.host1, self.port1)
					else:
						rc = self.oracle_connect(self.host2, self.port2)
					if rc:
						return True
				else:
					if i % 2 == 0:
						rc = self.mysql_connect(self.host1, int(self.port1))		
					else:
						rc = self.mysql_connect(self.host2, int(self.port2))		
					if rc:
						return True
			except:
				self.log.error("reconnect fail <%d>" %(i + 1))
			time.sleep(0.1)
		return False

	def execute_query(self, sql, fetch_flag = True):
		try:
			re_flag = self.cursor.execute(sql)
		except cx_Oracle.InterfaceError:
			re_flag = self.reconnect()
		except cx_Oracle.OperationalError:
			re_flag = self.reconnect()
		except pymysql.InterfaceError:
			re_flag = self.reconnect()
		except pymysql.OperationalError:
			re_flag = self.reconnect()
		except Exception as e:
			self.log.error("[DBLIB] execute_query() fail <%s>", e)
			return None
		else:
			if self.db_type == 'oracle' :
				if fetch_flag == True :
					return self.cursor.fetchall()
				else :
					return self.db_conn.commit()		
			else :
				return self.cursor.fetchall()
			#return self.cursor.fetchall()

		if re_flag:	
			self.cursor.execute(sql)
			if self.db_type == 'oracle' :
				if fetch_flag == 'true' :
					return self.cursor.fetchall()
				else :
					return self.db_conn.commit()		
			else :
				return self.cursor.fetchall()

			#return self.cursor.fetchall()
		else:
			return None

	def execute_query2(self, sql):
		try:
			re_flag = self.cursor.execute(sql)
		except cx_Oracle.InterfaceError:
			re_flag = self.reconnect()
		except cx_Oracle.OperationalError:
			re_flag = self.reconnect()
		except pymysql.InterfaceError:
			re_flag = self.reconnect()
		except pymysql.OperationalError:
			re_flag = self.reconnect()
		except Exception as e:
			self.log.error("[DBLIB] execute_query() fail <%s>", e)
			return None
		else:
			self.db_conn.commit()		
			return self.cursor.rowcount, self.cursor.fetchall()

		if re_flag:	
			self.cursor.execute(sql)
			return self.cursor.fetchall()
		else:
			return None

	def prepare_execute_query(self, pstmt, values):
		try:
			re_flag = self.cursor.execute(pstmt, values)
		except cx_Oracle.InterfaceError:
			re_flag = self.reconnect()
		except cx_Oracle.OperationalError:
			re_flag = self.reconnect()
		except pymysql.InterfaceError:
			re_flag = self.reconnect()
		except pymysql.OperationalError:
			re_flag = self.reconnect()
		except Exception as e:
			self.log.error("[DBLIB] prepare_execute_query() fail <%s>", e)
			return None
		else:
			return self.cursor.fetchall()

		if re_flag:	
			self.cursor.execute(pstmt, values)
			return self.cursor.fetchall()
		else:
			return None

	def execute(self, sql, commit=True):
		self.cursor.execute(sql)
		try:
			self.cursor.execute(sql)
		except cx_Oracle.InterfaceError:
			re_flag = self.reconnect()
		except cx_Oracle.OperationalError:
			re_flag = self.reconnect()
		except pymysql.InterfaceError:
			re_flag = self.reconnect()
		except pymysql.OperationalError:
			re_flag = self.reconnect()
		except Exception as e:
			self.log.error("[DBLIB] execute() fail <%s>", e)
			return False
		else:
			if commit:
				self.db_conn.commit()		
			return True

		if re_flag:	
			self.cursor.execute(sql)
			if commit:
				self.db_conn.commit()		
			return True
		else:
			return False

	def prepare_execute(self, pstmt, values, commit=True):
		try:
			self.cursor.execute(pstmt, values)
		except cx_Oracle.InterfaceError:
			re_flag = self.reconnect()
		except cx_Oracle.OperationalError:
			re_flag = self.reconnect()
		except pymysql.InterfaceError:
			re_flag = self.reconnect()
		except pymysql.OperationalError:
			re_flag = self.reconnect()
		except Exception as e:
			self.log.error("[DBLIB] prepare_execute() fail <%s>", e)
			return False
		else:
			if commit:
				self.db_conn.commit()		
			return True

		if re_flag:	
			self.cursor.execute(pstmt, values)
			if commit:
				self.db_conn.commit()		
			return True
		else:
			return False

	def execute_many(self, pstmt, values, commit=True):
		try:
			self.cursor.executemany(pstmt, values)
		except cx_Oracle.InterfaceError:
			re_flag = self.reconnect()
		except cx_Oracle.OperationalError:
			re_flag = self.reconnect()
		except pymysql.InterfaceError:
			re_flag = self.reconnect()
		except pymysql.OperationalError:
			re_flag = self.reconnect()
		except Exception as e:
			self.log.error("[DBLIB] execute_many() fail <%s>", e)
			return False
		else:
			if commit:
				self.db_conn.commit()		
			return True

		if re_flag:	
			self.cursor.executemany(pstmt, values)
			if commit:
				self.db_conn.commit()		
			return True
		else:
			return False

	
	def get_last_auto_increment(self, column, table_name):
		#pstmt = """select AUTO_INCREMENT 
		#           from information_schema.tables 
		#		   where table_name = %s and table_schema = DATABASE()"""
		#value = (table_name)
		sql = "select MAX(" + column + ") as LAST_ID " + "from " + table_name
		rows = self.execute_query(sql)
		print(rows)
		if self.db_type == 'oracle':
			return rows[0][0]
		else :
			return rows[0]['LAST_ID']

