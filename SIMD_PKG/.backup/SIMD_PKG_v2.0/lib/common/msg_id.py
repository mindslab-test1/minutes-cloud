#!/usr/bin/python
# -*- coding: utf-8 -*-

LOC_REPORT_ASDE=1 #Interface: RADAR-004
LOC_REPORT_ADSB=2 #Interface: RADAR-004
LOC_REPORT_ARTS=3 #Interface: RADAR-004
LOC_PUBLISH_NOTI=4 #Interface:RADAR-005

LOC_UPDATE_NOTI=5 #Interface: VOICE-006

VOICE_STT_RESULT_NOTI=6 #Interface: VOICE-004
VOICE_PUBLISH_NOTI=7 #Interface:VOICE-005

def msg_str(msg_id):
	if msg_id == LOC_REPORT_ADSB:
		return "ADSB_NOTI"
	elif msg_id == LOC_REPORT_ARTS:
		return "ARTS_NOTI"
	elif msg_id == LOC_REPORT_ASDE:
		return "ASDE_NOTI"
	elif msg_id == LOC_PUBLISH_NOTI:
		return "LOC_PUBLISH"
	elif msg_id == LOC_UPDATE_NOTI:
		return "LOC_UPDATE"
	elif msg_id == VOICE_STT_RESULT_NOTI:
		return "STT_RESULT"
	elif msg_id == VOICE_PUBLISH_NOTI:
		return "VOICE_PUBLISH"
	else:
		return "UNKNOWN"
