import time
from collections import deque

class TransManager():
	def __init__(self, timeout, max_cnt=0, log=None):
		self.tr_list = deque()
		self.timeout = timeout
		self.max_cnt = max_cnt
		self.log = log
	
	def check_in(self, data):
		tmp_dict = {}
		itime = int(float(time.time()))
		tmp_dict.update({'time':itime})
		tmp_dict.update({'data':data})
		self.tr_list.append(tmp_dict)
	
	def check_out(self):
		result = []
		ctime = int(float(time.time()))
		while True:
			if len(self.tr_list) <= 0:
				break
			if ctime - self.tr_list[0]['time'] > self.timeout:
				result.append(self.tr_list[0]['data'])
				self.tr_list.popleft()
			else:
				break
		return result

if __name__ == '__main__':
	tr = TransManager(3, 3)
	tr.check_in(1)
	tr.check_in(2)
	time.sleep(1)
	tr.check_in(3)
	while True:
		tmp = tr.check_out()
		if tmp:
			print tmp
		time.sleep(1)

