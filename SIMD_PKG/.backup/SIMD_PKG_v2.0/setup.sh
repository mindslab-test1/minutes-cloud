echo '############################################################'
echo '                          install SIMd                      '
echo '############################################################'

echo ' python module installing....'
echo 'y' | sudo yum install python-devel
echo 'y' | sudo pip install --upgrade pip "setuptools<45"
echo 'y' | sudo pip install zmq 
sleep 1
echo 'y' | sudo pip install tornado
sleep 1
echo 'y' | sudo pip install multiprocessing_logging
sleep 1
echo 'y' | sudo pip install websocket_client
sleep 1
echo 'y' | sudo pip install pymysql
sleep 1
echo 'y' | sudo pip install cx_Oracle
sleep 1
echo 'y' | sudo pip install psutil
sleep 1
echo 'y' | sudo pip install gpustat
sleep 1
echo 'y' | sudo pip install bcrypt
sleep 1
echo ' python module installing compelete'
echo ' '
sleep 1

echo ' Config File installing....'
echo 'y' | cp -rp ./etc/dbconf.conf $MAUM_ROOT/etc/
echo 'y' | cp -rp ./etc/process_info.conf $MAUM_ROOT/etc/
echo 'y' | cp -rp ./etc/simc.conf $MAUM_ROOT/etc/
echo 'y' | cp -rp ./etc/simd.conf $MAUM_ROOT/etc/
echo 'y' | cp -rp ./etc/framework.conf $MAUM_ROOT/etc/supervisor/conf.d/
echo ' '

p
sleep 1
echo ' lib File installing....'
echo 'y' | cp -rp ./lib/*.py $MAUM_ROOT/lib/python/common/
sleep 1

echo ' bin File installing....'
echo 'y' | cp -rp ./system-information-management/simd.py $MAUM_ROOT/bin/simd
echo 'y' | cp -rp ./system-information-management/simc.py $MAUM_ROOT/bin/simc
echo 'y' | cp -rp ./system-information-management/simd/ $MAUM_ROOT/lib/
sleep 1
