#! /usr/bin/python
#-*- coding:utf-8 -*-
#######################################################################
## CONFIGPARSER #######################################################
import os, sys, io
import common.simd_config as simd_config
#SIMd_ConfPath = os.getenv('MAUM_ROOT') + '/etc/simd.conf'
proc_name=os.path.basename(sys.argv[0])
SIMd_ConfPath = os.getenv('MAUM_ROOT') + '/etc/' + proc_name + '.conf'
SIMd_config = simd_config.Config_Parser(SIMd_ConfPath)
MMC_PATH = os.getenv('MAUM_ROOT') + '/bin/MMC' 
#######################################################################
sys.path.append(MMC_PATH)
import zmq
import shutil
#import threading
import traceback
import time
import json
import common.resource_monitor as monitor
import common.logger as logger
import common.minds_ipc as IPC
import socket
import uuid
import tornado.web
import tornado.websocket
import tornado.websocket
import websocket
from common.minds_ipc import ZmqPipline
#from multiprocessing import Process, Queue
from multiprocessing import Process
from common.dblib import DBLib
from datetime import datetime
from tornado.websocket import websocket_connect
########################################################################
## PYTHON MODULE #######################################################
import DIS_SYS_INFO 
import CHG_SYS_INFO
import DIS_RM_INFO
import CHG_RM_INFO
import DIS_DBCONF_INFO
import CHG_DBCONF_INFO
import DIS_LOG_LEVEL
import CHG_LOG_LEVEL
import CHG_DB_PWD
########################################################################


def backup_conf(log, ConfPath):
	try :
		file_name = os.path.basename(ConfPath)	
		process_name = os.path.splitext(file_name)[0]	
		save_time = time.strftime('%Y%m%d%H%M%S', time.localtime(time.time()))
		backup_path = os.getenv('MAUM_ROOT') + '/etc/backup/' + process_name
		if not os.path.exists(backup_path):
			os.makedirs(backup_path) 
		shutil.copy(ConfPath, backup_path + '/' + process_name + '_' + save_time)	
			
	except Exception as e :
		log.error("backup error : {}".format(e))
		log.error(traceback.format_exc())
	
	return backup_path

def proc_function(log, mysql, ipc,  MMC, ARG):
	try :
		PROC_ConfPath = os.getenv('MAUM_ROOT') + '/etc/process_info.conf' 
		DB_ConfPath = os.getenv('MAUM_ROOT') + '/etc/dbconf.conf' 

		if (MMC == 'dis-sys-info'):
			return DIS_SYS_INFO.proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath)
	
		elif (MMC == 'chg-sys-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(log, SIMd_ConfPath)	
				log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return CHG_SYS_INFO.proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath)
	
		elif (MMC == 'dis-rm-info'):
			return DIS_RM_INFO.proc_exec(log, mysql, MMC, ARG,SIMd_ConfPath)
	
		elif (MMC == 'chg-rm-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(log, SIMd_ConfPath)	
				log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return CHG_RM_INFO.proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath)
	
		elif (MMC == 'dis-dbconf-info'):
			return DIS_DBCONF_INFO.proc_exec(log, mysql, MMC, ARG, DB_ConfPath)
	
		elif (MMC == 'chg-dbconf-info'):
			if (ARG != 'help') :
				backup_path = backup_conf(log, DB_ConfPath)	
				log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return CHG_DBCONF_INFO.proc_exec(log, mysql, MMC, ARG, DB_ConfPath)
	
		elif (MMC == 'dis-log-level'):
			return DIS_LOG_LEVEL.proc_exec(log, mysql, MMC, ARG, PROC_ConfPath)
	
		elif (MMC == 'chg-log-level'):
			if (ARG != 'help') :
				backup_path = backup_conf(log, PROC_ConfPath)	
				log.info('backup_path : {}'.format(backup_path))
			else :
				pass
			return CHG_LOG_LEVEL.proc_exec(log, mysql, ipc, MMC, ARG, PROC_ConfPath)
	
		elif (MMC == 'chg-db-pwd'):
			return CHG_DB_PWD.proc_exec(log, mysql, MMC, ARG)
	
		else :
			ResultMsg = {}
			ResultMsg['msg_header'] = {}
			ResultMsg['msg_header']['msg_id'] = 'MMC_Response'
			ResultMsg['msg_body'] = {}
			ResultMsg['msg_body']['mmc'] = MMC
			ResultMsg['msg_body']['result'] = 'FAILURE'
			ResultMsg['msg_body']['reason'] = 'MMC is wrong'
			ResultMsg['msg_body']['data'] = 'MMC is wrong'
			return ResultMsg
	
	
	except Exception as e :
		ResultMsg = {}
		ResultMsg['msg_header'] = {}
		ResultMsg['msg_header']['msg_id'] = 'MMC_Response'
		ResultMsg['msg_body'] = {}
		ResultMsg['msg_body']['mmc'] = MMC
		ResultMsg['msg_body']['result'] = 'FAILURE'
		ResultMsg['msg_body']['reason'] = 'PROC_FUNCTION ERROR'
		ResultMsg['msg_body']['data'] = 'PROC_FUNCTION ERROR'
		return ResultMsg

def resource_monitor(res_monitor, log, wsc_loc):
	### resource_monitor log 객체 생성, log를 따로 생성함
	res_log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', 'resource_monitor', 'debug', False)
	before_time = int(time.time())
	off_flag = True
	while True :
		res_config = simd_config.Config_Parser(SIMd_ConfPath)
		if res_config == False :
			log.error("simd.conf read error")
		else :
			if (res_config.DIS_Item_Value('HARDWARE_MONITORING', 'use_flag').lower() == 'on'): 
				current_time = int(time.time())
				if (current_time != before_time):
					before_time = int(time.time())
					monitoring_interval = int(res_config.DIS_Item_Value('HARDWARE_MONITORING', 'monitoring_interval'))
					if ((before_time % monitoring_interval) == 0): 
						cpu_percent = res_monitor.cpu_percent_2()
						mem_total = float(res_monitor.mem_total())
						#mem_free = int(res_monitor.mem_free())
						mem_used = float(res_monitor.mem_used())
						mem_percent = round((mem_used)/(mem_total) * 100,1)
						disk_total = float(res_monitor.disk_total(SIMd_ConfPath))
						#disk_free = int(res_monitor.disk_free(SIMd_ConfPath))
						disk_used = float(res_monitor.disk_used(SIMd_ConfPath))
						disk_percent = round((disk_used/disk_total) * 100,1)
				
						ws_msg = {}
						ws_msg['system']=res_config.DIS_Item_Value('SYS', 'sys_name')
						ws_msg['cpu'] = {}
						ws_msg['cpu']['percent'] = float(cpu_percent)
						ws_msg['mem'] = {} 
						ws_msg['mem']['total'] = int(mem_total)
						ws_msg['mem']['used'] = int(mem_used)
						ws_msg['disk'] = {}
						ws_msg['disk']['total'] = int(disk_total)
						ws_msg['disk']['used'] = int(disk_used)
						ws_msg['gpu'] = {}
						ws_msg['gpu_mem'] = {}
						
						gpu_list = 'GPU' 
						gpu_mem_list = 'GPU_MEM'
						gpus = res_monitor.gpu_stat3()
						
						if (gpus == None):
							gpu_list = gpu_list + '[{}]'.format(gpus)
							gpu_mem_list = gpu_mem_list + '[{}]'.format(gpus)
							ws_msg['gpu'] = None
							ws_msg['gpu_mem'] = None

						else :	
							for i in range(len(gpus)) :
								gpu_mem_per = round((gpus[i]["gpu_total_used"]/gpus[i]["gpu_total_mem"]) * 100,1)
								gpu_row = '[({}){} %] '.format(i, gpus[i]["gpu_percent"])
								gpu_list = gpu_list + gpu_row 
								gpu_mem_row = '[({}){} %] '.format(i, gpu_mem_per)
								gpu_mem_list = gpu_mem_list + gpu_mem_row
					
								ws_msg['gpu'] = {}
								ws_msg['gpu_mem'] = {}
								ws_msg['gpu'][i] = {}
								ws_msg['gpu'][i]['percent'] = gpus[i]["gpu_percent"]
								ws_msg['gpu_mem'][i] = {}
								ws_msg['gpu_mem'][i]['total'] = int(gpus[i]["gpu_total_mem"])
								ws_msg['gpu_mem'][i]['used'] = int(gpus[i]["gpu_total_used"])
							
						res_log.info('CPU[{} %] MEM[{} %] DISK[{} %] {}{}'.format(cpu_percent, mem_percent, disk_percent, gpu_list, gpu_mem_list))
						
						json_ws_msg = json.dumps(ws_msg)	
						wsc_loc.send(json_ws_msg)	
						off_flag = True	
					else :
						time.sleep(0.05)
						continue
				else :
					time.sleep(0.05)
					continue
			else :
				if (off_flag == True):
					res_log.info('Resource_Monitor Off')
					off_flag = False
				else :
					pass
				
				time.sleep(1)
				continue

def ipc_send(ipc, log, JsonMsg):
	try :
		ipc.IPC_Send('MECD', JsonMsg)
		ipc.IPC_Send('MCCD', JsonMsg)
		ipc.IPC_Send('MIPD', JsonMsg)
		ipc.IPC_Send('MIDD', JsonMsg)
		ipc.IPC_Send('CNN_SERVER', JsonMsg)
		return True	
	
	except Exception as e :
		log.error("IPC ERROR : {}".format(e))
		log.error(traceback.format_exc())
		return False

def mmc_list(MMC):
	data = """
====================================================================
 Basic Command
====================================================================
 help        - Display MMC_LIST
 clear       - Clear Screen
 quit, exit  - PROCESS EXIT
====================================================================

====================================================================
 MMC_LIST  
====================================================================
 DIS-SYS-INFO	  CHG-SYS-INFO     DIS-RM-INFO      CHG-RM-INFO 
 DIS-DBCONF-INFO  CHG-DBCONF-INFO  DIS-LOG-LEVEL    CHG-LOG-LEVEL
 CHG-DB-PWD      
====================================================================
** If you need more information MMC. 
   Try -> help [MMC] 
   ex) help DIS-SYS-INFO
"""
	result_msg = {}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC
	result_msg['msg_body']['data'] = data

	return result_msg

def rsp_msg(ReqMsgId, MsgFlag):
	RspMsg = {}
	RspMsg['msg_header'] = {}
	RspMsg['msg_body'] = {}
	if (ReqMsgId == "Connection_Request") :
		if (MsgFlag == True):
			RspMsg['msg_header']['msg_id'] = "Connection_Response"
			RspMsg['msg_body']['result'] = "SUCCESS"
		
		else :
			RspMsg['msg_header']['msg_id'] = "Connection_Response"
			RspMsg['msg_body']['result'] = "RECONNECTION"
			RspMsg['msg_body']['reason'] = "Client_key already exist in the ClientObj"

	elif (ReqMsgId == "MMC_Request") :
		if (MsgFlag == True):
			RspMsg['msg_header']['msg_id'] = "MMC_Response"
		
		else :
			RspMsg['msg_header']['msg_id'] = "MMC_ERROR_Response"
			RspMsg['msg_body']['result'] = "CONNECTION"
			RspMsg['msg_body']['reason'] = "Client_key doesn't exist in the ClientObj"
			RspMsg['msg_body']['data'] = "Please enter MMC again"
	
	elif (ReqMsgId == "Close_MSG") :
		RspMsg['msg_header']['msg_id'] = "Close_MSG"

	else :
		log.error("It is not an ReqMsgId")
		RspMsg['msg_header']['msg_id'] = "msg_id ERROR"
		RspMsg['msg_body']['result'] = "msg_id ERROR"
		RspMsg['msg_body']['reason'] = "msg_id may have changed"
		RspMsg['msg_body']['data'] = "You need to check the MSG_ID"
		

	JsonRspMsg = json.dumps(RspMsg)
	
	return JsonRspMsg

def init_process():
	############# log 객체 생성#######################
	log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', 'simd', 'debug', True, False)

	############# DB 객체 생성 #######################
	mysql = DBLib(log)	
	
	## MySQL DB 접속
	db_conn = mysql.connect()
	
	########### resource_monitor 생성 ##################
	#res_monitor = monitor.ResourceMonitor()

	########### ipc 통신 setting #######################
	#proc_name=os.path.basename(sys.argv[0])
	ipc = IPC.MinsIPCs(log, proc_name)
	
	ret = ipc.IPC_Open()
	if ret == False :
		log.error("IPC OPEN FAILURE")
		sys.exit(1)
	
	## ipc 통신 등록
	ret = ipc.IPC_Regi_Process('MECD')
	if ret == False :
		log.error("IPC_REGI_PROCESS FAIL")
		sys.exit(1)
	ret = ipc.IPC_Regi_Process('MCCD')
	if ret == False :
		log.error("IPC_REGI_PROCESS FAIL")
		sys.exit(1)
	ret = ipc.IPC_Regi_Process('MIPD')
	if ret == False :
		log.error("IPC_REGI_PROCESS FAIL")
		sys.exit(1)
	ret = ipc.IPC_Regi_Process('MIDD')	
	if ret == False :
		log.error("IPC_REGI_PROCESS FAIL")
		sys.exit(1)
	ret = ipc.IPC_Regi_Process('CNN_SERVER')	
	if ret == False :
		log.error("IPC_REGI_PROCESS FAIL")
		sys.exit(1)
	
	log.info('Initial Setting Complete!!')

	return log, mysql, ipc 

def client_manager(ClientObj):
	
	ClientList = ""
	ClientCount = len(ClientObj)

	if (ClientCount == 0):
		ClientList = "No SIMc is connected"
	
	else :
		for SimcKey in ClientObj :
			if not ClientList:
				row = " {} ".format(SimcKey)		
			else :
				row = "\n {} ".format(SimcKey)		
			ClientList = ClientList + row
	ClientListMsg = """
===============================================================
 Connected client_keys (count : {})
===============================================================
{}
===============================================================
""".format(ClientCount, ClientList)

	return ClientListMsg

####### SIMc 측에서 10분동안 MSG가 없으면 종료 메시지를 보내고 커넥트 종료
def garbage_collection(log, CheckTime, ClientObj):
	current_time = int(time.time())
	if (current_time != CheckTime) :
		CheckTime = int(time.time())	
		if ((CheckTime % 1) == 0):
			if (len(ClientObj) == 0):
				pass
			else :
				for SimcKey in ClientObj :
					LstMsgInterval = time.time() - ClientObj[SimcKey]['recv_time']
					if (LstMsgInterval > 600):
						try :
							ClientObj[SimcKey]['zmq'].close()
							######### Client_list 출력
							log.info("[{}] closed".format(SimcKey))
							del ClientObj[SimcKey]
							ClientList = client_manager(ClientObj)
							log.info("{}".format(ClientList))
							break
						
						except Exception as e:
							log.error('ERROR : {}'.format(e))
							log.error(traceback.format_exc())
							log.info("[{}] closed".format(SimcKey))
							del ClientObj[SimcKey]
							ClientList = client_manager(ClientObj)
							log.info("{}".format(ClientList))
							break
					else :
						pass
		else :
			pass
	else :
		pass

	return ClientObj, CheckTime

def main():
	log, mysql, ipc = init_process()
	
	####### WS_Resource_Monitoring ########
	## ws server run
	ws_server_proc(log)
	
	## muti_proc_ws_client
	WsProcess = Process(target=ws_client_proc, args=(log,))
	WsProcess.daemon = True   #### deamon 화??? 왜이렇게 해야 하는가?
	WsProcess.start()

	############# SIMc Communication #################
	### PULL 객체 생성
	mmc_port = SIMd_config.DIS_Item_Value('SYS', 'mmc_port')
	SimcZmqPull = ZmqPipline(log)
	SimcZmqPull.bind(mmc_port)
	
	######## client_list dictionary 
	ClientObj = {}
	CheckTime = int(time.time()) #### garbage_collection 함수를 1초마다 실행하는데 필요	
	####### main_loop
	while True :
		try :
			############# NON_BLOCK
			try :
				ReqMsg = json.loads(SimcZmqPull.recv(flags=zmq.NOBLOCK))
				RecvTime = time.time()
				log.info("REQ_MSG = {}".format(ReqMsg))
				ReqMsgId = ReqMsg['msg_header']['msg_id']
				MsgFlag = True
			
			except zmq.Again as e :
				ClientObj, CheckTime = garbage_collection(log, CheckTime, ClientObj)
				time.sleep(0.1)
				continue

			if (('msg_header' in ReqMsg) and ('msg_body' in ReqMsg)) :
				################ Connect_MSG  
				if (ReqMsgId == "Connection_Request") :
					ClientIp = ReqMsg['msg_body']['client_ip']
					ClientPort = ReqMsg['msg_body']['client_port']
					ClientKey = ClientIp + ':' + ClientPort					
					if (ClientKey not in ClientObj) :
						ClientObj[ClientKey] = {}
						ClientObj[ClientKey]['recv_time'] = RecvTime
						ClientObj[ClientKey]['zmq'] = ZmqPipline(log)
						ClientObj[ClientKey]['zmq'].connect(ClientIp, ClientPort)
						######### Client_list 출력
						ClientList = client_manager(ClientObj)
						log.info("{}".format(ClientList))

						JsonConnectRsp = rsp_msg(ReqMsgId, MsgFlag)
						ClientObj[ClientKey]['zmq'].send(JsonConnectRsp)
						log.info("Connect_RSP : {}".format(JsonConnectRsp))
					
					else :
						log.error("Client_key already exist in the ClientObj")
						ClientObj[ClientKey]['zmq'].close()
						del ClientObj[ClientKey]
						######### Client_list 출력
						ClientList = client_manager(ClientObj)
						log.info("{}".format(ClientList))

						ClientObj[ClientKey] = {}
						ClientObj[ClientKey]['recv_time'] = RecvTime
						ClientObj[ClientKey]['zmq'] = ZmqPipline(log)
						ClientObj[ClientKey]['zmq'].connect(ClientIp, ClientPort)
						######### Client_list 출력
						ClientList = client_manager(ClientObj)
						log.info("{}".format(ClientList))
						
						JsonConnectRsp = rsp_msg(ReqMsgId, MsgFlag)
						ClientObj[ClientKey]['zmq'].send(JsonConnectRsp)
						log.info("Connect_RSP : {}".format(JsonConnectRsp))

				elif (ReqMsgId == "MMC_Request") :
					ClientIp = ReqMsg['msg_body']['client_ip']
					ClientPort = ReqMsg['msg_body']['client_port']
					ClientKey = ClientIp + ':' + ClientPort					
					# simc는 종료 안했는데, 10분 경과했을 경우 reconnect
					if (ClientKey in ClientObj):
						pass
					else :
						ClientObj[ClientKey] = {}
						ClientObj[ClientKey]['recv_time'] = RecvTime
						ClientObj[ClientKey]['zmq'] = ZmqPipline(log)
						ClientObj[ClientKey]['zmq'].connect(ClientIp, ClientPort)

					MMC = ReqMsg['msg_body']['mmc']
					ARG = ReqMsg['msg_body']['arg']
					if (MMC == 'help') :
						MmcRsp = mmc_list(MMC)	
					else :
						MmcRsp = proc_function(log, mysql, ipc, MMC, ARG)
				
					JsonMmcRsp = json.dumps(MmcRsp)
					#log.info('Created Message!!')
					ClientObj[ClientKey]['zmq'].send(JsonMmcRsp)
					log.info("MMC_RSP = {}".format(JsonMmcRsp))
				
				elif (ReqMsgId == "Close_MSG") :
					## 이미 접속이 끊긴 상태에서 Close_MSG 온경우
					try :
						ClientIp = ReqMsg['msg_body']['client_ip']
						ClientPort = ReqMsg['msg_body']['client_port']
						ClientKey = ClientIp + ':' + ClientPort					
						ClientObj[ClientKey]['zmq'].close()
						del ClientObj[ClientKey]
						###client_list 출력
						log.info("[{}] closed".format(ClientKey))
						ClientList = client_manager(ClientObj)
						log.info("{}".format(ClientList))
					
					except Exception as e :
						log.error("Already disconnected")
						###client_list 출력
						ClientList = client_manager(ClientObj)
						log.info("{}".format(ClientList))
		
				else :
					log.error("msg_id [{}] is wrong".format(ReqMsgId))
			else :
				log.error("Message structure is invalid")

		## 새로운 에러가 발생할 경우
		except Exception as e:
			log.error('New Exception Error [{}]'.format(e))
			log.error(traceback.format_exc())

	### 멀티 프로세스 및 통신 종료
	ipc.ipc_Close()
	WsProcess.close()
	for ClientKey in ClientObj :
		ClientObj[ClientKey]['zmq'].close()
	SimcZmqPull.close()


class WS_Handler(tornado.websocket.WebSocketHandler):
	clients = set()
	def open(self):
		if self not in self.clients:
			self.id = uuid.uuid4()
			self.clients.add(self)
			#log.critical("WS_Handler::open(%s) client connected", self.id)
			print("WS_Handler::open(%s) client connected", self.id)
	
	def on_close(self):
		if self in self.clients:
			self.clients.remove(self)
			#log.critical("WS_Handler::on_close(%s) client removed", self.id)
			print("WS_Handler::on_close(%s) client removed", self.id)
	
	def on_message(self, msg):
		#log.info("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		#print("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		self.SendAll(self.id, msg)
	
	def check_origin(self, origin):
		return True
	
	def SendAll(self, send_id, msg):
		for client in self.clients:
			if not client.ws_connection.stream.socket:
				self.clients.remove(client)
				#log.critical("WS_Handler::SendAll(%s) client removed", self.id)
				print("WS_Handler::SendAll(%s) client removed", self.id)
			else:
				if client.id == send_id:
					continue

				#log.info("[TX] {} => {}" .format(msg, client.id))
				#print("[TX] {} => {}" .format(msg, client.id))
				client.write_message(msg)

class WS_Server(Process):
	def __init__(self, __log, ws_port):
		Process.__init__(self)
		self.log=__log
		self.ws_port=ws_port
	
	def run(self):
		self.log.critical("WS_Server::run() START [port:{}]" .format(self.ws_port))
		try:
			app = tornado.web.Application([(r"/websocket", WS_Handler)])
			app.listen(int(self.ws_port))
			tornado.ioloop.IOLoop.instance().start()
		except Exception as e:
			self.log.critical("WS_Server() Exception => %s", e)
			pass

		self.log.critical("WS_Server::run STOP")
class WS_Client():
	def __init__(self, url, log):
		self.log= log
		self.url = url
		self.wsc = None
	
	def connect(self):
		self.log.critical("WS_Client::connect() => %s", self.url)
		try:
			self.wsc = websocket.WebSocket()
			self.wsc.connect(self.url, setdefaulttimeout=0)
		except socket.error as e:
			if e.errno == 111: # [errno 111] Connection refused
				self.log.critical("WS_Client::connect refused fail [%s]", e)
				pass
			else:
				self.log.critical("WS_Client::connect socket fail [%s]", e)
				self.close()
		except Exception as e:
			self.log.critical("WS_Client::connect fail [%s]", e)
			self.log.critical(traceback.format_exc())
			self.close()

	def send(self, msg):
		if self.wsc is None:
			self.connect()

		if self.wsc:
			try:
				self.wsc.send(msg)
			except Exception as e:
				self.log.critical("WS_Client::connect fail [%s]", e)
				self.close()

	def close(self):
		self.log.critical("WS_Client::close()")
		self.wsc.close()
		self.wsc = None

class WS_Sender():
	def __init__(self, log):

		## websocket client Init
		ws_port = SIMd_config.DIS_Item_Value('HARDWARE_MONITORING', 'ws_port')
		self.log=log
		self.wsc_loc = WS_Client("ws://127.0.0.1:%s/websocket" %(ws_port),log)
		self.res_monitor = monitor.ResourceMonitor()
		## queue Init
		#self.ws_sender_q=ws_sender_q
	
	def run(self):
		self.log.critical("WS_Sender::run()")
		while True:
			try:
				resource_monitor(self.res_monitor, self.log, self.wsc_loc)
	
			except KeyboardInterrupt:
				self.log.info("interrupt received, stopping...")
		
			except Exception as e:
				self.log.error("main: exception raise fail. <%s>", e)
				self.log.error(traceback.format_exc())

	def close(self):
		self.log.critical("WS_Sender::close()")
		self.wsc_loc.Close()
		#self.wsc_voice.Close()

def ws_server_proc(log):
	ws_port = SIMd_config.DIS_Item_Value('HARDWARE_MONITORING', 'ws_port')
	wss = WS_Server(log, ws_port)
	wss.start() #Process Spawn: run() method 호출

def ws_client_proc(log):

	#init , IP port
	ipc_receiver = WS_Sender(log)
	#ipc_receiver.init()

	#blocking here
	ipc_receiver.run()
	ipc_receiver.close()
	log.critical("Process stopped...")

	return 0

if __name__ == "__main__" :
	main()
	print("process exit")




