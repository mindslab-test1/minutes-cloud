#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import traceback
import common.simd_config as simd_config
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	============================================================
	 {} = mandatory, () = optional
	============================================================
	 [Usage]
	 	1. DIS-RM-INFO
		  - View all 
	
	 [Section Options Configuration]
	 	use_flag             : Whether to use a resource_monitor
		ws_port              : Web_socket port
		monitoring_interval  : Time interval for resource_monitor

	 [Result]
	 	<SUCCESS>
			Date time
			MMC = DIS-RM-INFO
			Result = SUCCESS
			====================================================
			use_flag            = value
			ws_port             = value
			monitoring_interval = value
			====================================================

		<FAILURE>
			Date time
			MMC = DIS-RM-INFO
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================

"""
	return total_body

def Arg_Parsing(ARG) :
	ARG_CNT=len(ARG)
	
	if (ARG_CNT != 0) :
		return False
	else :
		return True

def proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath):
	total_body=''
	
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			SIMd_config = simd_config.Config_Parser(SIMd_ConfPath)
			if SIMd_config == False :
				log.error("simd.conf read error")
				total_body=''
				result='FAILURE'
				reason='Config file read error'
				return make_result(MMC, ARG, result, reason, total_body)
			else :
				pass

			ret = Arg_Parsing(ARG)
			if ret == False :
				total_body=''
				result='FAILURE'
				reason='Parameter is invalid. Enter only MMC'
				return make_result(MMC, ARG, result, reason, total_body)

			else :
				data = SIMd_config.DIS_Section2('HARDWARE_MONITORING')		
				log.info('conf_data = {}'.format(data))
				row = ''
				for item in data :
					if (row == ''):
						row = '\t{0:20} = {1}'.format(item, data[item])
					else :
						row = '\n\t{0:20} = {1}'.format(item, data[item])
					total_body = total_body + row

			result='SUCCESS'
			reason=''
			log.info('PROC_DIS_RM_INFO() Complete!!')
	
		
	except Exception as e:
		log.error('PROC_DIS_RM_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		if data == '' :
			reason='SYSTEM FAILURE'
		else :
			reason=data

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "\t{}".format(total_body)
	else :
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)

		else :
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg
