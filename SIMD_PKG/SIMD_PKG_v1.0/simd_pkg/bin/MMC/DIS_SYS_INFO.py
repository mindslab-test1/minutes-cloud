#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import common.simd_config as simd_config
from datetime import datetime
from collections import OrderedDict
import os
#from common.dblib import DBLib

def mmc_help():
	total_body = """
	============================================================
 	 {} = mandatory, () = optional
	============================================================
 	 [Usage]
	 	1. DIS-SYS-INFO
		  - View all
		
	 [Section Options Configuration]
		sys_name          : System name
		mmc_port          : SIMc bind port
		UPLOAD_DIR        : Where to upload voice files from server
		RESULT_DIR        : Where the result informations are stored
		ADMIN_EMAIL       : Administrator email address

 	 [Result]
 		<SUCCESS>
			Date time
			MMC = DIS-SYS-INFO
			Result = SUCCESS
			====================================================
			[SYS_CONF]
				sys_name     = value
				mmc_port     = value
			[DB_INFO]
				UPLOAD_DIR   = value
				RESULT_DIR   = value
				ADMIN_EMAIL  = value
			====================================================

 		<FAILURE>
			Date time
			MMC = DIS-SYS-INFO
			Result = FAILURE
			======================================================
			Reason = Reason for error
			======================================================
"""
	return total_body 

def Arg_Parsing(ARG) :
	ARG_CNT=len(ARG)
		
	if (ARG_CNT != 0) :
		return False 
	else :	
		return True 

def proc_exec(log, mysql, MMC, ARG, SIMd_ConfPath):
	total_body=''
	
	try :
		if (ARG == 'help'):
			total_body = mmc_help()	
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
	
		else :
			SIMd_config = simd_config.Config_Parser(SIMd_ConfPath)
			if SIMd_config == False :
				log.error("simd.conf read ERROR")
				total_body=''
				result='FAILURE'
				reason='CONFIG_FILE READ ERROR'
				return make_result(MMC, ARG, result, reason, total_body)
			else :
				pass

			ret = Arg_Parsing(ARG)
			if ret == False :
				total_body=''
				result='FAILURE'
				reason='Parameter is invalid. Enter only MMC'
				return make_result(MMC, ARG, result, reason, total_body)
			
			else :
				data = SIMd_config.DIS_Section2('SYS')		
				DB_data = DIS_Query(mysql, 'MINUTES_COMMON', '*', ';')
				log.info('conf_data = {}'.format(data))
				log.info('db_data = {}'.format(DB_data))
				if not DB_data :
					result='FAILURE'
					reason='DB_DATA does not exist'
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					#### DB에서 조회해서 가져오는 값을 순서를 있게 하는 법 알아보기
					db_list = ["UPLOAD_DIR", "RESULT_DIR", "ADMIN_EMAIL"]
					row = ''
					for item in data :
						if (row == ''):
							row = '\t[SYS_CONF]\n\t\t{0:13} = {1}'.format(item, data[item])
						else :
							row = '\n\t\t{0:13} = {1}'.format(item, data[item])
						total_body = total_body + row
					row = ''
					for item in db_list :
						if (row == ''):
							row = '\n\t[DB_INFO]\n\t\t{0:13} = {1}'.format(item , DB_data[0][item])
						else :
							row = '\n\t\t{0:13} = {1}'.format(item , DB_data[0][item])
						total_body = total_body + row

				result='SUCCESS'
				reason=''
				log.info('PROC_DIS_SYS_INFO() Complete!!')
				return make_result(MMC, ARG, result, reason, total_body)

	except Exception as e:
		log.error('PROC_DIS_SYS_INFO(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'

		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "\t{}".format(total_body)
	else :
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
	
		else :
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)

	if (ARG == 'help') :
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body
	
	result_msg['msg_body']['data'] = data 
	
	return result_msg

def DIS_Query(mysql, table, column, where):
	DIS_All_Query = """
		select {}
		from {}
		""".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		return rows

	except Exception as e :
		log.error('DB DIS_Query ERROR : {}' .format(e))
		return ''

