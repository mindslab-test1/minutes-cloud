#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys, os, logging
import zmq
import common.simd_config as simd_conf

# ZMP PUSH / PULL
# PIPELINE PATTERN
# producer -> consumer : only downstream

class ZmqPipline:
	def __init__(self, log):
		self.log = log
		self.context = None
		self.socket = None
	
	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		#self.context = zmq.Context()
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" %(collector_ip, collector_port))
		self.log.critical("ZmqPipline:: connect() SUCC -> <%s:%s>", collector_ip, collector_port)
	
	def send(self, msg, flags=0):
		self.socket.send(msg, flags=flags)
			
	
	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" %bind_port)
		self.log.critical("ZmqPipline:: bind() SUCC -> <*:%s>", bind_port)
	
	def recv(self, flags=0):
		return self.socket.recv(flags=flags)
	
	def close(self):
		self.socket.close()
		self.context.term()

class MinsIPCs:
	def __init__(self, log, my_proc_name) :
		self.log = log
		self.context = None
		self.socket= None
		self.my_ipc_key=None
		self.my_proc_name=my_proc_name
		self.conf=simd_conf.Proc_Conf(my_proc_name)
		self.ipc_keys={} 
	def IPC_Open(self) :
		try :
			my_zmq_port = self.conf.get_my_zmq_port()
			self.my_ipc_key=ZmqPipline(self.log)
			self.my_ipc_key.bind(my_zmq_port)
			self.log.critical("[{}] IPC OPEN SUCCESS ".format(self.my_proc_name))
			return True
		except Exception as e:
			self.log.critical("[{}] IPC OPEN FAILURE".format(self.my_proc_name))
			self.log.critical("{}".format(e))
			return False
	def IPC_RE_OPEN(self) :
		try :
			self.my_ipc_key.close()
			self.log.critical("IPC Close Complete".format(proc_name))
			my_zmq_port = self.conf.get_my_zmq_port()
			self.my_ipc_key=ZmqPipline(self.log)
			self.my_ipc_key.bind(my_zmq_port)
			self.log.critical("[{}] IPC OPEN SUCCESS ".format(self.my_proc_name))

			for proc_name in self.ipc_keys :
				try :
					self.ipc_keys[proc_name].close()
					self.log.critical("[{}] IPC Close".format(proc_name))

					zmq_port = self.conf.get_zmq_port(proc_name)
					self.ipc_keys[proc_name]=ZmqPipline(self.log)
					self.ipc_keys[proc_name].connect('127.0.0.1', zmq_port)
				except Exception as e:
					self.log.critical("[{}] REGI IPC FAILURE".format(proc_name))
					self.log.critical("{}".format(e))
			
			return True
		except Exception as e:
			self.log.critical("[{}] IPC OPEN FAILURE".format(self.my_proc_name))
			self.log.critical("{}".format(e))
			return False
	def IPC_Regi_Process(self, proc_name, ip='127.0.0.1') :
		try :
			zmq_port = self.conf.get_zmq_port(proc_name)
			self.ipc_keys[proc_name]=ZmqPipline(self.log)
			self.ipc_keys[proc_name].connect(ip, zmq_port)
			return True
		except Exception as e:
			self.log.critical("[{}] REGI IPC FAILURE".format(proc_name))
			self.log.critical("{}".format(e))
			return False
	def IPC_Close(self) :
		for proc_name in self.ipc_keys :
			self.ipc_keys[proc_name].close()
			self.log.critical("[{}] IPC Close".format(proc_name))

		self.my_ipc_key.close()
		self.log.critical("IPC Close Complete".format(proc_name))
	def IPC_Send(self, proc_name, msg) :
		if proc_name not in self.ipc_keys :
			self.log.critical("[{}] IPC Not Registed".format(proc_name))
			return False
		#return await self.ipc_keys[proc_name].send(msg)
		return self.ipc_keys[proc_name].send(msg)
	def IPC_Recv(self) :
		try :
			msg=self.my_ipc_key.recv(flags=zmq.NOBLOCK)
			return msg
		except zmq.Again as e:
			return ''
		except Exception as e:
			self.log.critical("[{}] REGI IPC FAILURE".format(proc_name))
			self.log.critical("{}".format(e))
			return ''
