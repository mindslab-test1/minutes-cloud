#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *

DIS_LOG_LEVEL_MIN_PARAMETER = 0
DIS_LOG_LEVEL_MAX_PARAMETER = 1

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  DIS-LOG-LEVEL (PROCESS_NAME=a)

 [Parameter]
  a = STRING (1:100)

 [Usage]
  DIS-LOG-LEVEL
  DIS-LOG-LEVEL (PROCESS_NAME=a)
   ex) DIS-LOG-LEVEL PROCESS_NAME=SIMD

 [Config_File Organization]
  [Section]
    MECD       : minutes-event-collector
    MCCD       : minutse-center-control
    MIPD       : minutes-information-publisher
    MIDD       : minutes-information-distributor
    SIMD       : system-information-management
    CNN_SERVER : cnn_server

  [Option]
    log_level  : Log level 
 
 [Result]
  <SUCCESS>
    Date time
    MMC = DIS-LOG-LEVEL
    Result = SUCCESS
    ============================================
            Process       |      Log Level
    --------------------------------------------
         Process Name     |        value
         Process Name     |        value
	....
    ============================================

  <FAILURE>
    Date time
    MMC = DIS-LOG-LEVEL
    Result = FAILURE
    ====================================================
    Reason = Reason for error
    ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data) :

	# Mandatory Parameter Check
	if arg_data['PROCESS_NAME'] == None : 
		return False, "'PROCESS_NAME' is Mandatory Parameter"

	##### Mandatory Parameter #####
	if arg_data['PROCESS_NAME'] != None :
		if len(arg_data['PROCESS_NAME']) > 100 :
			return False, "'PROCESS_NAME's Max length is 100"
	
	return True, ''
	
def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	arg_data = {}
	for item in org_list :
		arg_data[item] = None

	# ARG Count Check
	ARG_CNT=len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"

	if ('=' in ARG[0]):
		for item in ARG :
			if '=' not in item :
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not')"
			else:
				name, value = item.split('=', 1)
				if name not in arg_data :
					#check each parameter's name validation
					return False, ARG_CNT, None, "PARAMETER Type is Invalid. '{}'".format(name)
				else:
					arg_data[name] = value
	# input all parameter without '='
	else:
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]]=ARG[idx]
	
	# check parameter's values
	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
	    # if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/process_info.conf)
			proc_config = ConfigParser.RawConfigParser()
			proc_config.read(G_proc_cfg_path)
			section_list = proc_config.sections()

			# make argument list (parsing and check validation)
			if len(ARG) is not 0:
				org_list = ['PROCESS_NAME']
				ret, ARG_CNT, Parsing_Dic, reason = Arg_Parsing(ARG, org_list, DIS_LOG_LEVEL_MAX_PARAMETER, DIS_LOG_LEVEL_MIN_PARAMETER)
				if ret == False :
					result = 'FAILURE'
					return make_result(MMC, ARG, result, reason, total_body)
				# get specific section information
				else:
					# Check section name
					section = Parsing_Dic['PROCESS_NAME']
					if section not in section_list :
						result = 'FAILURE'
						reason = 'Section Name [{}] is not exist.'.format(section)
						return make_result(MMC, ARG, result, reason, total_body)

					row = ''
					total_body=""" {:^20} | {:^20} \n""".format('PROCESS', 'LOG LEVEL')
					total_body = total_body + '--------------------------------------------'
					item_list = proc_config.options(section)
					for item in item_list :
						G_log.debug(item)
						if 'log_level' in item:
							value = proc_config.get(section, item)
							row = '\n {:^20} | {:^20}'.format(section, value)
							total_body = total_body + row
			# get all section information
			else:
				row = ''
				count = 0
				total_body=""" {:^20} | {:^20} \n""".format('PROCESS', 'LOG LEVEL')
				total_body = total_body + '--------------------------------------------'
				for section in section_list : 	
					G_log.debug(section)
					item_list = proc_config.options(section)
					for item in item_list :
						if 'log_level' in item:
							G_log.debug(item)
							value = proc_config.get(section, item)
							row = '\n {:^20} | {:^20}'.format(section, value)
						total_body = total_body + row
						row = ''

			result='SUCCESS'
			reason=''
			G_log.info('DIS-LOG-LEVEL() Complete!!')
			return make_result(MMC, ARG, result, reason, total_body)
	
	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-LOG-LEVEL(), NoSectionError : [{}]".format(e))
		reason='DIS-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('DIS-LOG-LEVEL(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File Read error [{}]'.format(G_proc_cfg_path)
	except Exception as e:
		G_log.error('DIS-LOG-LEVEL(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "{}".format(total_body)
	
	else :	
		if (result == 'FAILURE') :
			msg_body = """
============================================
 {}
============================================
""".format(reason)
		else :	
			msg_body = """
============================================
{}
============================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg

