#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	===========================================================
	 {} = mandatory, () = optional
	===========================================================
	 [Usage]
		1. DIS_RM_THR 

	 [Options Configuration]
		SYS_RSC_THR_SER    = Serial Number
		CREATE_USER        = Create user ID
		CREATE_TIME        = Create time
		UPDATE_USER        = Update user ID 
		UPDATE_TIME        = Update time
		SYSTEM_NAME        = Server information 
		SORTATION          = equipment separator 
		THRESHOLD_MINOR    = Caution steps
		THRESHOLD_MAJOR    = Warning steps
		THRESHOLD_CRITICAL = Critical steps
	 	
	 [Result]
	 	<SUCCESS>
			Date time
			MMC    = DIS-RM-THR
			Result = SUCCESS
			====================================================
			SYS_RSC_THR_SER    = value
			CREATE_USER        = value
			CREATE_TIME        = value
			UPDATE_USER        = value 
			UPDATE_TIME        = value
			SYSTEM_NAME        = value 
			SORTATION          = value 
			THRESHOLD_MINOR    = value 
			THRESHOLD_MAJOR    = value 
			THRESHOLD_CRITICAL = value 
				                   ...
			====================================================

	 	<FAILURE>
			Date time
			MMC    = DIS-RM-THR
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, log) :
	#org_list = ["SYSTEM_NAME", "SORTATION", "THRESHOLD_MINOR", "THRESHOLD_MAJOR", "THRESHOLD_CRITICAL"]
	ARG_CNT = len(ARG)
	if (ARG_CNT != 0):
		return validation_check_result(False, 0, {})
	else :	
		return validation_check_result(True, 0, {})

def proc_exec(log, mysql, MMC, ARG):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				db_data = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", ';')
				log.info("DB_data = {}".format(db_data))
				if not db_data :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					first_flag = True
					total_body="\t [{}] {} : {}  {}  {}  {}\n".format("SER", "SYS_NAME", "SORTATION", "MINOR", "MAJOR", "CRITICAL")
					for thr_num in range(len(db_data)) :
						thr_ser=db_data[thr_num]["SYS_RSC_THR_SER"]
						sys_nm=db_data[thr_num]["SYSTEM_NAME"]
						sortation=db_data[thr_num]["SORTATION"]
						minor=db_data[thr_num]["THRESHOLD_MINOR"]
						major=db_data[thr_num]["THRESHOLD_MAJOR"]
						critical=db_data[thr_num]["THRESHOLD_CRITICAL"]
						row_buf="\t [{:3}] {:8} : {:10} {:5} {:5} {:5}\n".format(thr_ser, sys_nm, sortation, minor, major, critical)
						total_body=total_body + row_buf
						#row = "\tSYS_RSC_THR_SER    = {}".format(db_data[thr_num]["SYS_RSC_THR_SER"])
						#row2 = "\tCREATE_USER        = {}".format(db_data[thr_num]["CREATE_USER"])
						#row3 = "\tCREATE_TIME        = {}".format(db_data[thr_num]["CREATE_TIME"])
						#row4 = "\tUPDATE_USER        = {}".format(db_data[thr_num]["UPDATE_USER"])
						#row5 = "\tUPDATE_TIME        = {}".format(db_data[thr_num]["UPDATE_TIME"])
						#row6 = "\tSYSTEM_NAME        = {}".format(db_data[thr_num]["SYSTEM_NAME"])
						#row7 = "\tSORTATION          = {}".format(db_data[thr_num]["SORTATION"])
						#row8 = "\tTHRESHOLD_MINOR    = {}".format(db_data[thr_num]["THRESHOLD_MINOR"])
						#row9 = "\tTHRESHOLD_MAJOR    = {}".format(db_data[thr_num]["THRESHOLD_MAJOR"])
						#row10 = "\tTHRESHOLD_CRITICAL = {}".format(db_data[thr_num]["THRESHOLD_CRITICAL"])
						
						#if (first_flag == True) :
						#	total_body = total_body + row + '\n' + row2 + '\n' + row3 + '\n' + row4 + '\n' + row5 + '\n' + row6 + '\n' + row7 + '\n' + row8 + '\n' + row9 + '\n' + row10 
					#		first_flag = False
					#	else :
					#		total_body = total_body + '\n\n' + row + '\n' + row2 + '\n' + row3 + '\n' + row4 + '\n' + row5 + '\n' + row6 + '\n' + row7 + '\n' + row8 + '\n' + row9 + '\n' + row10 
						
					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('ADD_SYS_NAME(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="\t{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else:	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {}".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		for row in rows :
			for a in row :
				try :
					print('{} : {}' .format(a, row[a]))
				except Exception as e :
					pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
		return ''

def Insert_Query(table, Parsing_Dict, mysql, log):
	try :
		sql = "insert into {}(CREATE_USER, SYSTEM_NAME, SORTATION, THRESHOLD_MINOR, THRESHOLD_MAJOR, THRESHOLD_CRITICAL) values('admin', '{}', '{}', {}, {}, {});".format(table, Parsing_Dict["SYSTEM_NAME"].upper(), Parsing_Dict["SORTATION"].upper(), Parsing_Dict["THRESHOLD_MINOR"].upper(), Parsing_Dict["THRESHOLD_MINOR"].upper(), Parsing_Dict["THRESHOLD_MAJOR"].upper(), Parsing_Dict["THRESHOLD_CRITICAL"].upper()) 
		mysql.execute(sql, True)
		return True

	except Exception as e :
		log.error('DB INSERT ERROR : {}'.format(e))
		log.error(traceback.format_exc())
		return False
