#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import re
import json
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *

CHG_LOG_LEVEL_MIN_PARAMETER = 2
CHG_LOG_LEVEL_MAX_PARAMETER = 2


def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-LOG-LEVEL [PROCESS_NAME=a] [LOG_LEVEL=b]

 [Parameter]
  a = STRING 	(1:100)
  b = ENUM      (debug|info|warning|error|critical)

 [Usage]
  CHG-LOG-LEVEL {Section}, {value}
   ex) CHG-LOG-LEVEL SIMD debug
  CHG-LOG-LEVEL {Section}, (option=value) 
   ex) CHG-LOG-LEVEL SIMD log_level=debug
 
 [Config_File Organization]
  [Section]
   MECD       : minutes-event-collector
   MCCD       : minutes-center-control
   MIPD       : minutes-information-publisher 
   MIDD       : minutes-information-distributor
   SIMD       : system-information-management 
   CNN_SERVER : cnn-server
  [Option]
   log_level  : log_level

 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-LOG-LEVEL
  Result = SUCCESS
  ============================================
        Process       |       Log Level
  --------------------------------------------
      Process Name    |    before -> after
  ============================================


  <FAILURE>
  Date time
  MMC = CHG-PROC-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body


def Check_Arg_Validation(arg_data) :

	# Mandatory Parameter Check
	if arg_data['PROCESS_NAME'] == None : 
		return False, "'PROCESS_NAME' is Mandatory Prameter"
	
	if arg_data['LOG_LEVEL'] == None :
		return False, "'LOG_LEVEL' is Mandatory Prameter"
		
	##### Mandatory Parameter #####
	if arg_data['LOG_LEVEL'] != None :
	    if len(arg_data['LOG_LEVEL']) > 100 :
			return False, "'LOG_LEVEL's Max length is 100"

	if arg_data['LOG_LEVEL'] != None :
		log_level_list = ['debug', 'info', 'warning', 'error', 'critical']
		if len(arg_data['PROCESS_NAME']) > 100 :
			return False, "'PROCESS_NAME's Max length is 100"
		elif arg_data['LOG_LEVEL'] not in log_level_list :
			return False, "'LOG_LEVEL' set only \n   'debug', 'info', 'warning', 'error', 'critical'."

	return True, ''


def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	arg_data={}
	for item in org_list :
		arg_data[item] = None

	# ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt:
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"

	if ('=' in ARG[0]):
		for item in ARG :
			if '=' not in item :
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not')"
			else:
				name, value = item.split('=', 1)
				if name not in arg_data :
					return False, ARG_CNT, None, "PARAMETER Type is Invalid. '{}'".format(name)
				else:
					arg_data[name] = value
	# input all parameter without '='
	else:
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]] = ARG[idx]

	# check parameter's values
	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason

def proc_exec(MMC, ARG, ipc):

	total_body=''

	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = "SUCCESS"
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
	
		else :
		    # load simd config file (/srv/maum/etc/process_info.conf)
			proc_config = ConfigParser.RawConfigParser() 
			proc_config.read(G_proc_cfg_path)	
			section_list = proc_config.sections()

			org_list=['PROCESS_NAME', 'LOG_LEVEL']
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, org_list, CHG_LOG_LEVEL_MAX_PARAMETER, CHG_LOG_LEVEL_MIN_PARAMETER)
			if (ret == False):
				result = 'FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
			else :
				# Check section name
				section = Parsing_Dict['PROCESS_NAME']
				if section not in section_list :
					result = 'FAILURE'
					reason = 'Section Name [{}] is not exist.'.format(section)
					return make_result(MMC, ARG, result, reason, total_body)

				data = OrderedDict()		
				org_item_value = proc_config.items(Parsing_Dict['PROCESS_NAME'])
				for i in range(len(org_item_value)):
					data[org_item_value[i][0]] = org_item_value[i][1]
				G_log.info('conf_data = {}'.format(data))

				# change config file data
				G_log.debug(Parsing_Dict['PROCESS_NAME'])
				G_log.debug(Parsing_Dict['LOG_LEVEL'])
				if Parsing_Dict['LOG_LEVEL'] is not None :
					value = proc_config.set(Parsing_Dict['PROCESS_NAME'], 'log_level', Parsing_Dict['LOG_LEVEL'])
					if value == False:
						result = 'FAILURE'
						return make_result(MMC, ARV, result, 'Change Config File Failure [log_level]')
					#else:
						#with open(G_proc_cfg_path, 'w') as configfile :
							#proc_config.write(configfile)

				# make response message
				total_body=""" {:^20} | {:^20} \n""".format('PROCESS', 'LOG LEVEL')
				total_body = total_body + '--------------------------------------------'
				total_body = total_body + '\n {:^20} | {:^8} -> {:^8}'.format(section, data['log_level'], Parsing_Dict['LOG_LEVEL'])
				G_log.info('CHG-LOG-LEVEL Complete!!')
				
				##### ipc 
				ipc_msg = {}
				ipc_msg['msg_header'] = {}
				ipc_msg['msg_header']['msg_id'] = "CHG_LOG_LEVEL"
				ipc_msg['msg_body'] = "" 
				
				for proc in section_list :
					if proc is section:
						ipc.IPC_Send(proc, json.dumps(ipc_msg))	
						G_log.info("IPC Message Send to [{}]".format(proc)) 

				return make_result(MMC, ARG, 'SUCCESS', '', total_body)
	
	except ConfigParser.NoSectionError as e :
		G_log.error("{}, NoSectionError : [{}]".format(MMC,e))
		reason='{}, NoSectionError'.format(MMC)
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('{}, ERROR Occured [{}]'.format(MMC,e))
		G_log.error(traceback.format_exc())
		reason='Config_File read error [{}]'.format(MMC)
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('{}, ERROR Occured [{}]'.format(MMC,e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return make_result(MMC, ARG, "FAILURE", reason, total_body)


def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)
	else : 
		if (result == 'FAILURE'):
			msg_body = """
============================================
 {}
============================================
""".format(reason)
	
		else :
			msg_body = """
============================================
{}
============================================
""".format(total_body)

	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	
	if (ARG == 'help'):
		data = msg_header +  msg_body
	else :
		data = msg_header +  msg_body
	
	result_msg['msg_body']['data'] = data
	return result_msg
