#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	===========================================================
	 {} = mandatory, () = optional
	===========================================================
	 [Usage]
		1. DEL-SYS-NAME {value}

		* value : Serial nember to delete

	 [Result]
	 	<SUCCESS>
			Date time
			MMC    = DEL-RM-THR
			Result = SUCCESS
			====================================================
			[Delete Serial Number]"
				SYS_RSC_THR_SER    = deleted serial number
				SYS_TEM_NAME       = deleted system_name
				SORTATION          = deleted sortation
				THRESHOLD_MINOR    = deleted threshold_minor
				THRESHOLD_MAJOR    = deleted threshold_major
				THRESHOLD_CRITICAL = deleted threshold_critical
			====================================================

	 	<FAILURE>
			Date time
			MMC    = DEL-RM-THR
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 2)):
		if (Parsing_Dict == "SYS_RSC_THR_SER"):
			reason = "Value error[{}] (The value is a number positive and type 'int')".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, log) :
	ARG_CNT = len(ARG)
	if (ARG_CNT != 1):
		return validation_check_result(False, 0, {})
	else :	
		Parsing_Dict = OrderedDict()
		Parsing_Dict["SYS_RSC_THR_SER"] = ARG[0] 
		
	for ItemName in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
		if (Parsing_Dict[ItemName] == ''):
			return validation_check_result(False, 1, ItemName)
		elif (ItemName == "SYS_RSC_THR_SER"): 
			int_value = re.findall("\d+", Parsing_Dict[ItemName])
			if ((len(int_value) != 1) or (Parsing_Dict[ItemName] != int_value[0])):	
				return validation_check_result(False, 2, ItemName)
			else :
				pass
		else :
			pass

	return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				select_query = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", " where SYS_RSC_THR_SER={}".format(Parsing_Dict["SYS_RSC_THR_SER"]))
				print("[{}](type:{})!!!!!!!!!!!".format(select_query, type(select_query)))
				if select_query == None :
					result="FAILURE"
					reason="User Not Found [{}]" .format(Parsing_Dict["SYS_RSC_THR_SER"])
					return make_result(MMC, ARG, result, reason, total_body)
				elif len(select_query) == 0 :
					result="FAILURE"
					reason="User Not Found [{}]" .format(Parsing_Dict["SYS_RSC_THR_SER"])
					return make_result(MMC, ARG, result, reason, total_body)
				elif select_query == '' :
					result="FAILURE"
					reason="Select Failure [{}]" .format(Parsing_Dict["SYS_RSC_THR_SER"])
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					pass

				delete_query = Delete_Query("SYSTEM_RESOURCE_THRESHOLD", Parsing_Dict["SYS_RSC_THR_SER"], mysql, log)
				if (delete_query == False) :
					result="FAILURE"
					reason="Insert_Query Error"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					row = "\t[Delete Serial Number]"
					total_body = total_body + '\n' + row 
					for line in select_query :
						line_buf=  "\t\tSYS_RSC_THR_SER    = {}\n".format(line['SYS_RSC_THR_SER'])
						line_buf+= "\t\tSYSTEM_NAME        = {}\n".format(line['SYSTEM_NAME'])
						line_buf+= "\t\tSORTATION          = {}\n".format(line['SORTATION'])
						line_buf+= "\t\tTHRESHOLD_MINOR    = {}\n".format(line['THRESHOLD_MINOR'])
						line_buf+= "\t\tTHRESHOLD_MAJOR    = {}\n".format(line['THRESHOLD_MAJOR'])
						line_buf+= "\t\tTHRESHOLD_CRITICAL = {}\n".format(line['THRESHOLD_CRITICAL'])
						total_body = total_body + '\n' + line_buf

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('ADD_SYS_NAME(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="\t{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else:	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {}".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		#for row in rows :
		#	for a in row :
		#		try :
		#			print('{} : {}' .format(a, row[a]))
		#		except Exception as e :
		#			pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
		return ''

def Delete_Query(table, serial_num, mysql, log):
	try :
		sql = "delete from {} where SYS_RSC_THR_SER={};".format(table, serial_num) 

		mysql.execute(sql, True)
		return True

	except Exception as e :
		log.error('DB INSERT ERROR : {}'.format(e))
		log.error(traceback.format_exc())
		return False
