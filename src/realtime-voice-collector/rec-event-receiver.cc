#include "rec-event-receiver.h"
#include "stt-client.h"
#include "zhelpers.h"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"

#include <arpa/inet.h>

#define QUOTE(...) #__VA_ARGS__

#if 0
class EventTask : public Task {
 public:
  EventTask() {
  }
  virtual ~EventTask() {
    g_var.console->debug("dtor EventTask");
  }

  rapidjson::Document document;
};
#endif

// record event receiver
RecEventReceiver::RecEventReceiver(RecSessionManager *manager) {
  session_manager_ = manager;
}

RecEventReceiver::~RecEventReceiver() {
}

void RecEventReceiver::Start() {
  thrd_ = std::thread(&RecEventReceiver::run, this);
}

void RecEventReceiver::run() {
  char buf[8192];
  int size;
  int cur_call = 0;

  void *context = zmq_ctx_new();
  void *receiver = zmq_socket(context, ZMQ_PULL);
  zmq_bind(receiver, g_var.event_recv_addr.c_str());

  while (true) {
    size = zmq_recv(receiver, buf, 8192, 0);
    buf[size] = 0x00;
    g_var.console->info("got message: {}", buf);

    int index;
    int event = SessionEvent::EV_UNKNOWN;
    std::string key;
    std::unique_ptr<rapidjson::Document> document(new rapidjson::Document);
    document->Parse(buf);

    auto it = document->FindMember("CALL-ID");
    if (it != document->MemberEnd() && it->value.IsString()) {
      key = it->value.GetString();
      g_var.console->debug("SESS_DEBUG: {} / {}", cur_call, g_var.max_proc_call);

      it = document->FindMember("TYPE");
      if (it != document->MemberEnd() && it->value.IsString()) {
        std::string stage = it->value.GetString();
        if (stage == "START") {
	      if (cur_call >= g_var.max_proc_call) {
            g_var.console->error("SESS_DEBUG: max proc count over");
		    continue;
	      }
          event = SessionEvent::EV_START;
          auto session = session_manager_->GetSession(key, index);
          if (session == NULL) {
            // session is max
            g_var.console->error("session is full");
			continue;
          }
		  cur_call++;
      	  g_var.console->debug("SESS_DEBUG++: {} / {}", cur_call, g_var.max_proc_call);
        } else if (stage == "STOP") {
          event = SessionEvent::EV_STOP;
          auto session = session_manager_->FindSession(key, index);
          if (session == NULL) {
            // session is max
            g_var.console->error("session is none");

	    // added by KHY
            char buf[4096] = {0, };
            if (g_var.publisher_) 
            {
                char *fmt = QUOTE( { "TYPE":"ALREADY_STOPED", "CALL-ID":"%s", "TALKER":"", "STOP-TIME":"" });
		    snprintf(buf, sizeof(buf), fmt, key.c_str());

                g_var.push_lock.lock();
                zmq_send(g_var.publisher_, buf, strlen(buf), ZMQ_NOBLOCK);
                g_var.push_lock.unlock();
            }
            //added end
		continue;
          }
		  cur_call--;
      	  g_var.console->debug("SESS_DEBUG--: {} / {}", cur_call, g_var.max_proc_call);
        } else {
          // unknown event
          g_var.console->error("unknown event");
          continue;
        }
        Task *task = new Task { this, event, std::string(buf, size) };
    	document->Parse(buf);
        task->user_data = (void *)document.release();
        session_manager_->Enqueue(index, task);
      }
    } else {
      g_var.console->error("not found CALL-ID");
    }
  }

  zmq_close(receiver);
  zmq_ctx_destroy(context);
}

void RecEventReceiver::handle_task(int index, Task *task)
{
	if (task->data_type == SessionEvent::EV_START) {  // start
		RecSession *session = session_manager_->GetSession(index);
		g_var.console->info("SESSION START : index = {}", index);
		if (!session->is_stopped) {
			// 같은 Start 이벤트 반복해서 온 경우 (ex:RTP based event)
			g_var.console->debug("RTP based event 2");
			session_manager_->ReleaseSession(index);
			return;
		}
		// do something long work
		std::unique_ptr<rapidjson::Document> document((rapidjson::Document *)task->user_data);
		rapidjson::StringBuffer buffer;
		rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
		document->Accept(writer);
		const char* json = buffer.GetString();
		LOGGER()->info("Message is {}", json);
		g_var.console->info("Message is {}", json);

		g_var.console->info("session start {}", index);
		g_var.console->info("key is {}", (*document)["CALL-ID"].GetString());
		std::string key = (*document)["CALL-ID"].GetString();
#if 0
		auto it = document->FindMember("AGENT-IP");
		if (it != document->MemberEnd() && it->value.IsString()) {
			//yhlee
			//string dst_ip = it->value.GetString();
			//second_key = inet_addr(dst_ip.c_str());
			//g_var.console->debug("inet is {}", second_key);
			string local_ip = it->value.GetString();
			second_key = inet_addr(local_ip.c_str());
			g_var.console->debug("inet is {}", second_key);
		}

		if (second_key == INADDR_NONE) {
			// invalid address
			session_manager_->ReleaseSession(index);
			LOGGER()->error("Invalid AGENT-IP");
			return;
		}
#else
		std::string second_key="";
		auto it = document->FindMember("MIC_INFO");
		if (it != document->MemberEnd() && it->value.IsString()) {
			//yhlee
			//string dst_ip = it->value.GetString();
			//second_key = inet_addr(dst_ip.c_str());
			//g_var.console->debug("inet is {}", second_key);
			second_key = it->value.GetString();
			g_var.console->debug("MIC_INFO is {}", second_key);
		}
#endif

		g_var.console->info("key         : {}", key.c_str());
		g_var.console->info("second_key  : {}", second_key);
		session->key = key;
		session->second_key = second_key;
		// 전화는 Tx, Rx 2개의 채널을 기본으로 한다
		//session->channels.emplace_back(new RecChannel);
		//session->channels.emplace_back(new RecChannel);
		//session->channels.emplace_back(new RecChannel(key, (*document)["AGENT-IP"].GetString(), (*document)["CUSTOMER-NUMBER"].GetString(), "A"));
		stt_info stt_info;
		//stt_info.stt_model_ser=0;
		stt_info.stt_model_ser=(*document)["STT_MODEL_SER"].GetString();
		stt_info.lean_type="";
		stt_info.server_addr=(*document)["STT_ADDR"].GetString();
		stt_info.model_name=(*document)["STT_NAME"].GetString();
		stt_info.model_lang=(*document)["STT_LANG"].GetString();
		//stt_info.model_rate=8000;
		stt_info.model_rate=(*document)["STT_RATE"].GetString();
		session->channels.emplace_back(new RecChannel(key, second_key, (*document)["CUSTOMER-NUMBER"].GetString(), (*document)["MIC_NAME"].GetString(), stt_info));

		session_manager_->SetSession(key, second_key, index);
		gettimeofday(&session->LastEventTime, NULL);
		session->is_stopped = false;

		/* Added by KHY for Not Recv Packet */
		//Send Start MSG
		//session->channels[key].ConnectSTT();
		RecChannel *channel = (RecChannel *)session->channels[0].get();
		channel->DoStart();


	} else if (task->data_type == SessionEvent::EV_STOP) {  // stop
		g_var.console->debug("receive stop event {}", index);
		RecSession *session = session_manager_->GetSession(index);
		session->Stop();
		session_manager_->ReleaseSession(index);
		session->key.clear();
		session->second_key = "";
		g_var.console->info("release session {}", index);
	} else if (task->data_type == SessionEvent::IS_EXPIRED) {  // timeout
		g_var.console->info("session expired {}", index);
		RecSession *session = session_manager_->GetSession(index);
		//g_var.console->info("release session cid:{}", session->channels.unique_id_);
		session->Stop();
		session_manager_->ReleaseSession(index);
		session->key.clear();
		session->second_key = "";
		g_var.console->info("release session {}", index);
	}
}

void RecEventReceiver::StartCall(char* msg, int len) {
#if 0
  int index;
  PDS_REC1 *rec1 = (PDS_REC1 *)msg;
  std::string session_key(rec1->Common.KEY, 14);
  g_var.console->debug("key is {}", session_key);

  auto session = session_manager_->GetSession(session_key, index);
  if (session == NULL) {
    // session is max
    g_var.console->error("session is full");
  } else {
    if (session->is_stopped) {
      session->is_stopped = false;
    } else {
      // previouse session exist
      // update session?
    }

    session->is_stopped = false;
    // session->channel[0].stt = StartSTT(session_key, id);
    // session->channel[1].stt = StartSTT(session_key, id);
    session->key = session_key;
    session->second_key = session_key;
    // 전화는 Tx, Rx 2개의 채널을 기본으로 한다
    session->channels.emplace_back(new PdsChannel);
    session->channels.emplace_back(new PdsChannel);

    session_manager_->SetSession(session_key, session_key, index);

    Task *task = new Task { this, SessionEvent::EV_START, std::string(msg, len) };
    session_manager_->Enqueue(index, task);
  }
#endif
}

void RecEventReceiver::StopCall(char* msg, int len) {
#if 0
  int index;
  PDS_REC3* rec3 = (PDS_REC3*)msg;
  std::string key(rec3->Common.KEY, 14);
  auto session = session_manager_->FindSession(key, index /* output */);

  if (session == NULL) {
    // there is no session
    g_var.console->error("there is no session");
  } else {
    Task *task = new Task { this, SessionEvent::EV_STOP, std::string(msg, len) };
    session_manager_->Enqueue(index, task);
  }

  // g_var.lock.lock();
  // auto it = g_var.call_record_map.find(key);
  // if (it != g_var.call_record_map.end()) {
  //   CALL_RECORD record = it->second;
  //   g_var.call_record_map.erase(rec3->Common.KEY);
  //   timeval tv = {0, 0};
  //   record.SttRx->Stop(tv);
  //   record.SttTx->Stop(tv);
  //   g_var.lock.unlock();
  //   delete record.SttRx;
  //   delete record.SttTx;
  // } else {
  //   g_var.lock.unlock();
  //   LOGGER()->error("Invalid key: {}", key);
  // }
#endif
}

void RecEventReceiver::ProcessCallInfo(char* msg, int len) {
}

