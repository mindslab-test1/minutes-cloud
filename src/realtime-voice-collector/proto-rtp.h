#ifndef MSL_PROTO_RPT_H
#define MSL_PROTO_RPT_H

#include <string>

#include "rtp.h"
#include <memory>

using std::unique_ptr;

using Payload = std::pair<const char *, uint32_t>;

class RtpPacket {
 public:
  RtpPacket() {
  }
  virtual ~RtpPacket() {}

  static bool IsRtpPacket(unique_ptr<RtpPacket>& rtpPacket,
                          const char *data, uint32_t len, int *rtp_type);
  bool IsMark() {
    return header_->m_pt & 0x80;
  }
  uint32_t GetSSRC() {
    return header_->ssrc;
  }

  int GetPayloadType() {
    return header_->m_pt & 0x7F;
  }

#if 0
  void SetStreamValues(shared_ptr<Call> call, CallDir dir,
                       bool &ssrc_changed) {
    if (dir == CallDir ::DIR_FROM) {
      ssrc_changed = (call->rtp_from_ssrc != header_->ssrc);

      if (ssrc_changed)
        LOGGER()->debug("dir from ssrc changed: {:x} -> {:x}, seq: {}",
                        call->rtp_from_ssrc, ntohl(header_->ssrc), ntohs(header_->seq_num));

      call->rtp_from_ssrc = header_->ssrc;
      call->rtp_from_seq = ntohs(header_->seq_num);
      call->rtp_from_payload_type = GetPayloadType();
      // FIXME: timestamp
    } else {
      ssrc_changed = (call->rtp_to_ssrc != header_->ssrc);
      if (ssrc_changed)
        LOGGER()->debug("dir from ssrc changed: {:x} -> {:x}, seq: {}",
                        call->rtp_to_ssrc, ntohl(header_->ssrc), ntohs(header_->seq_num));
      call->rtp_to_ssrc = header_->ssrc;
      call->rtp_to_seq = ntohs(header_->seq_num);
      call->rtp_to_payload_type = GetPayloadType();
      // FIXME: timestamp
    }
  }
#endif

  Payload GetPayload() {
    const char *first = data_ + sizeof(*header_);
    uint32_t len = len_ - sizeof(*header_);
    return Payload(first, len);
  }

 private:
  const rtp_header * header_ = nullptr;
  const char * data_;
  size_t len_;
  int codec = 0;
 public:
  static const char kG729 = 18;
  static const char kG711 = 0;
  static const char kG711a = 8;
  static const char kDynamic101 = 101;
};

#endif // MSL_PROTO_RPT_H
