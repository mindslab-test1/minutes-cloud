cmake_minimum_required(VERSION 2.8.12)

project(biz-rvcd)

set(BINOUT biz-rvcd)
#set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

include_directories(
  "$ENV{MAUM_ROOT}/include"
  "rvc-session"
  ${CMAKE_CURRENT_SOURCE_DIR}
)

link_directories("$ENV{MAUM_ROOT}/lib")

set(CMAKE_CXX_STANDARD 11)

if (CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-flto)
  add_definitions(-Wl,--no-as-needed)
  add_definitions(-ggdb)
endif ()

set(SOURCE_FILES
  app-variable.cc
  stt-client.cc
  rec-event-receiver.cc
  pcap-collector.cc
  rec-session.cc
  rvc-session/worker-pool.cc
  udp-packet.cc
  proto-rtp.cc
  g711.c
  main.cc
)

add_executable(${BINOUT} ${SOURCE_FILES})

add_custom_command(TARGET ${BINOUT}
  POST_BUILD
  COMMAND sudo ARGS setcap cap_net_raw,cap_net_admin=eip ${BINOUT}
)

set_target_properties(${BINOUT} PROPERTIES LINK_SEARCH_START_STATIC 1)
set_target_properties(${BINOUT} PROPERTIES LINK_SEARCH_END_STATIC 1)
set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
#set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")

target_link_libraries(${BINOUT}
  brain-stt-pb
  grpc++ grpc protobuf
  maum-common
  maum-json
  pcap
  pthread
#  rt
  zmq
)

set(SOURCE_TEST
 heart-beat.cc
 test-hb.cc
)
add_executable(test-hb ${SOURCE_TEST})
target_link_libraries(test-hb
 pthread
 zmq
)

install(TARGETS ${BINOUT} RUNTIME DESTINATION bin)
install(CODE "MESSAGE(\"setcap biz-capd.\")")
install(CODE "execute_process(COMMAND sudo setcap cap_net_raw,cap_net_admin=eip ${CMAKE_INSTALL_PREFIX}/bin/${BINOUT})")
install(FILES biz-rvcd.conf.sample DESTINATION etc COMPONENT config)
