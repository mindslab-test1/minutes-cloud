#include <string.h>
#include <sys/time.h>
#include "session-manager.h"
#include "app-variable.h"

#define CHECK_INTERVAL 3

template <typename SESS, typename FirstKey, typename SecondKey>
SessionManager<SESS, FirstKey, SecondKey>::SessionManager(
    int max_session_num, WorkerPool *pool, int session_timeout) {
  for (int i = 0; i < max_session_num; i++) {
    SESS *session = new SESS;
    session->ref_count = 0;
    session->is_stopped = true;
    standby_session_q_.push(i);
    session_list_.push_back(session);
  }

  max_session_num_ = max_session_num;
  worker_pool_ = pool;
  session_timeout_ = session_timeout;
  is_done_ = false;
}

template <typename SESS, typename FirstKey, typename SecondKey>
SessionManager<SESS, FirstKey, SecondKey>::~SessionManager() {
  for (auto session : session_list_) {
    delete session;
  }
  is_done_ = true;
  if (monitor_thrd_.joinable()) {
    monitor_thrd_.join();
  }
}

template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::StartMonitoring(TaskHandler* handler) {
  monitor_thrd_ = std::thread([&, handler](){
    while (!is_done_) {
      timeval expire_time;
      gettimeofday(&expire_time, NULL);
      expire_time.tv_sec -= session_timeout_;

      for (int i = 0; i < max_session_num_; i++) {
        SESS* session = session_list_[i];
        if (session->is_stopped) {
          // empty session...
          continue;
        } else if (timercmp(&expire_time, &session->LastEventTime, >) == true) {
          // timeout
          Task *task = new Task { handler, SessionEvent::IS_EXPIRED, ""};
          worker_pool_->Enqueue(i, task);
        }
      }
      usleep(CHECK_INTERVAL * 1000 * 1000);
      g_var.console->info("Session count is {}", session_map.size());
    }
  });
}

template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::ProcessData(int index, int data_type, std::string data) {
#if 0
  if (index == max_session_num_) {
    Dispatch(data_type, data);
  } else {
    if (data_type == SessionEvent::SIGNALLING) {
      ProcessSession(index, data);
    } else if (data_type == SessionEvent::VOICE_DATA) {
      ProcessChannel(index, data);
    } else if (data_type == SessionEvent::IS_EXPIRED) {
      auto session = session_list_[index];
      timeval expire_time;
      gettimeofday(&expire_time, NULL);
      expire_time.tv_sec -= session_timeout_;
      if (!session->is_stopped && timercmp(&expire_time, &session->LastEventTime, >) == true) {
        session->Stop();
        ReleaseSession(index);
      }
    }
  }
#endif
}

template <typename SESS, typename FirstKey, typename SecondKey>
int SessionManager<SESS, FirstKey, SecondKey>::WorkerIndex() {
  return max_session_num_;
}

template <typename SESS, typename FirstKey, typename SecondKey>
int SessionManager<SESS, FirstKey, SecondKey>::GetSessionIndex(FirstKey key)
{
  if (standby_session_q_.empty()) {
    return -1;
  }

  int index = standby_session_q_.front();
  standby_session_q_.pop();
  session_map[key] = index;
  return index;
}

template <typename SESS, typename FirstKey, typename SecondKey>
SESS *SessionManager<SESS, FirstKey, SecondKey>::GetSession(int index)
{
  return session_list_[index];
}

template <typename SESS, typename FirstKey, typename SecondKey>
SESS *SessionManager<SESS, FirstKey, SecondKey>::FindSession(FirstKey key, int &index)
{
  std::unique_lock<std::mutex> lock(session_lock_);
  auto it = session_map.find(key);
  if (it == session_map.end()) {
    return NULL;
  }
  index = it->second;
  return session_list_[index];
}

template <typename SESS, typename FirstKey, typename SecondKey>
int SessionManager<SESS, FirstKey, SecondKey>::FindSessionIndexFromChannel(SecondKey key)
{
  std::unique_lock<std::mutex> lock(session_lock_);
  auto it = session_map2.find(key);
  if (it == session_map2.end()) {
    return -1;
  }
  return it->second;
}

template <typename SESS, typename FirstKey, typename SecondKey>
SESS *SessionManager<SESS, FirstKey, SecondKey>::GetSession(FirstKey key, int &index)
{
  std::unique_lock<std::mutex> lock(session_lock_);
  auto it = session_map.find(key);
  if (it == session_map.end()) {
    if (standby_session_q_.empty()) {
      return NULL;
    }
    index = standby_session_q_.front();
    standby_session_q_.pop();
    g_var.console->info("allocate new session: {}", index);
    // 같은 키의 start 이벤트에 해당되는 메시지 (예:RTP)가 연속으로 왔을 때를 대비하여 미리 등록
    session_map[key] = index;

  } else {
    index = it->second;
    g_var.console->info("already exist session: {}, {}", index, key);
  }

  // 같은 키로 stop, start 이벤트가 동시에 왔을 때 release하는 과정에서
  // 새로 start되는 session을 standby 큐에 넣는 것을 방지
  session_list_[index]->ref_count++;

  return session_list_[index];
}

template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::ReleaseSession(int index)
{
  auto session = session_list_[index];
  std::unique_lock<std::mutex> lock(session_lock_);
  session->ref_count--;
  if (session->ref_count == 0) {
    session_map.erase(session->key);
    session_map2.erase(session->second_key);
    standby_session_q_.push(index);
    g_var.console->info("erase session: {}, {}", index, session->key);
  }
}

template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::
SetSession(FirstKey session_key, SecondKey second_key, int index)
{
  std::unique_lock<std::mutex> lock(session_lock_);
  session_map[session_key] = index;
  session_map2[second_key] = index;
}

template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::Enqueue(int index, Task *task)
{
  worker_pool_->Enqueue(index, task);
}

// SessionManager worker
template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::Dispatch(int data_type, std::string data)
{
#if 0
  if (data_type == SessionEvent::SIGNALLING) {
    PDS_COMMON *msg_common = (PDS_COMMON *)data.c_str();
    std::string session_key(msg_common->KEY, 14);
    int index;

    auto it = session_map.find(session_key);
    if (it == session_map.end()) {
      index = GetSessionIndex(session_key);
      if (index == -1) {
        // TODO: session full
        return;
      }
    } else {
      index = it->second;
    }
    Task *task = new Task { this, data_type, data };
    worker_pool_->Enqueue(index, task);

  } else if (data_type == SessionEvent::VOICE_DATA) {
    PDS_COMMON *msg_common = (PDS_COMMON *)data.c_str();
    std::string key(msg_common->KEY, 14);

    std::unique_lock<std::mutex> lock(channel_lock_);
    auto it = session_map2.find(key);
    if (it == session_map2.end()) {
      return;
    }
    lock.unlock();
    // Put task to worker
    Task *task = new Task { this, data_type, data };
    worker_pool_->Enqueue(it->second, task);

  } else if (data_type == SessionEvent::DELETE_SESSION) {
    // data is session_key
    auto it = session_map.find(data);
    if (it != session_map.end()) {
      int index = it->second;
      session_map2.erase(data);
      session_map.erase(data);
      standby_session_q_.push(index);
    }
  }
#endif
}

// From other worker
template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::ProcessSession(int index, std::string data) {
#if 0
  SESS *session = session_list_[index];
  // SessionEvent(data);
  PDS_COMMON *msg_common = (PDS_COMMON *)data.c_str();
  // channel의 특정 값으로 session을 찾을 수 있게 한다. DB에서 찾는다면 오래 걸릴 수도 있다.
  std::string key = session->KeyFromChannel();
  // std::string key(msg_common->KEY, 14);
  if (strncmp(msg_common->Code, "CREC1", PDS_CODE_SIZE) == 0) {
    // START
    if (!session->is_stopped) {
      // TODO: error
      g_var.console->error("already started");
    } else {
      session->is_stopped = false;
      g_var.console->error("started");
    }
    channel_lock_.lock();
    session_map2[key] = index;
    channel_lock_.unlock();
    //StartCall(msg, len);
  } else if (strncmp(msg_common->Code, "CREC3", PDS_CODE_SIZE) == 0) {
    channel_lock_.lock();
    session_map2.erase(key);
    channel_lock_.unlock();
    // TODO
    // session->Stop();
    //StopCall(msg, len);
    Task *task = new Task { this, DELETE_SESSION, key };
    worker_pool_->Enqueue(max_session_num_, task);
  } else if (strncmp(msg_common->Code, "CREC5", PDS_CODE_SIZE) == 0) {
    // TODO:
    // session->ProcessCallInfo(msg, len);
  } else {
    // Invalid Message
    g_var.console->error("invalie msg: [{}]", data);
  }
#endif
}

template <typename SESS, typename FirstKey, typename SecondKey>
void SessionManager<SESS, FirstKey, SecondKey>::ProcessChannel(int index, std::string data)
{
  // SESS *session = session_list_[index];
  // VoiceChannel *channel = session->FindChannel(data);
  // channel->Process(data);
}
