#ifndef VOICE_SESSION_H
#define VOICE_SESSION_H

#include <vector>
#include <memory>

class VoiceChannel {
 public:
  VoiceChannel() {}
  virtual ~VoiceChannel() {}

  virtual void Stop() {}
};

template <typename FIRSTKEY, typename SECONDKEY>
class VoiceSession {
 public:
  VoiceSession() {}
  virtual ~VoiceSession() {}

  virtual void Stop() {}

  FIRSTKEY key;
  SECONDKEY second_key;
  std::vector<std::unique_ptr<VoiceChannel> > channels;

  timeval LastEventTime;
  bool is_stopped;
  int ref_count;
};

#endif /* VOICE_SESSION_H */
