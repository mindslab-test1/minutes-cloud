#ifndef PCAP_COLLECTOR_H
#define PCAP_COLLECTOR_H

#include <string>
#include <pcap.h>
#include <memory>
#include <thread>
#include <libmaum/common/config.h>
#include "proto-rtp.h"
#include "rvc-session/session-manager.h"
#include "rec-session.h"

#define BIZ_SNAPLEN 1024
//#define BIZ_SNAPLEN 256
#define NUM_WORKER 1

struct PcapData {
  pcap_pkthdr header;
  u_char pkt_data[BIZ_SNAPLEN];
};

//using RecSessionManager = SessionManager<RecSession, std::string, uint32_t>;
using RecSessionManager = SessionManager<RecSession, std::string, std::string>;

class PcapCollector : public TaskHandler {
 public:
  PcapCollector(std::string filter, std::string device, RecSessionManager *manager);
  virtual ~PcapCollector();

  void SetFilter(const string &filter);
  void SetBufferSize(int buffer_size);
  void Dispatch(const pcap_pkthdr *header, const u_char *pkt_data);
  void Handler(const pcap_pkthdr *header, const u_char *pkt_data);
  void Start();
  int  Run();

 private:
  void InitPcapWithDebugFile();
  void InitPcapLive();
  void CompileFilter();

  // Worker Thread
  void Consume(int index);

  // util
  void dump_pcap(const pcap_pkthdr *header, const u_char *pkt_data);

  // override
  // void handle_task(int index, int event, std::string data);
  void handle_task(int index, Task *task);

  void CreateSession(RecSession *session, std::string session_key,
                     std::string dst_info, int index);

  std::shared_ptr<spdlog::logger> logger_;
  pcap_t *pcap_ = nullptr;
  string filter_ = "ip and udp";
  string device_;
  bpf_program bpf_code_;
  bool capture_all_numbers_;
  bool remove_upstream_;
  std::thread thrd_;

  void *producer_ctx_[NUM_WORKER];
  void *producer_[NUM_WORKER];

  void *consumer_ctx_[NUM_WORKER];
  void *consumer_[NUM_WORKER];

  std::thread worker_thrd_[NUM_WORKER];

  RecSessionManager *session_manager_;
};

#endif /* PCAP_COLLECTOR_H */
