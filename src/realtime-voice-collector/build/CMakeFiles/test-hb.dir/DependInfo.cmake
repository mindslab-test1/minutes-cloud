# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/minds/git/minutes/src/realtime-voice-collector/heart-beat.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/test-hb.dir/heart-beat.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/test-hb.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/test-hb.dir/test-hb.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/srv/maum/include"
  "../rvc-session"
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
