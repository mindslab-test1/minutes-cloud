#include <net/ethernet.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#include "udp-packet.h"

UdpPacket::UdpPacket(char *pkt_data, int pkt_len) {
  // caculate ethernet header length
  ether_header *eh = (ether_header *)pkt_data;
  u_int16_t ether_type  = ntohs(eh->ether_type);
  // 14바이트 or vlan일 경우 18바이트
  u_int eh_len = ETHER_HDR_LEN + (ether_type == ETHERTYPE_VLAN ? 4 : 0);

  /* retireve the position of the ip header */
  ip *ih = (ip *) (pkt_data + eh_len); //length of ethernet header
  /* retireve the position of the udp header */
  u_int ip_len = (ih->ip_hl & 0xf) * 4;
  udphdr *uh = (udphdr *) ((u_char *) ih + ip_len);
  /* convert from network byte order to host byte order */
  src_port = ntohs(uh->source);
  dst_port = ntohs(uh->dest);

  src_ip = ih->ip_src.s_addr;
  dst_ip = ih->ip_dst.s_addr;

  payload = ((char *)uh + sizeof(*uh));
  payload_len = pkt_len - (payload - pkt_data);
}
