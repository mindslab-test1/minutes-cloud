#ifndef REPLACE_KEYWORD_H
#define REPLACE_KEYWORD_H

#include <string>
#include <unordered_map>
#include "aho_corasick.hpp"

class StringSubtitutor {
public:
    StringSubtitutor() {
        trie_map_.remove_overlaps();
    }

    void AddKeyword(std::string keyword, std::string replacement) {
        replace_map_.insert(std::make_pair(keyword, replacement));
        trie_map_.insert(keyword);
    }

    bool ReplaceKeywords(std::string &origin_text) {
        auto result = trie_map_.parse_text(origin_text);
        for (auto rit = result.rbegin(); rit != result.rend(); ++rit) {
            origin_text.replace(rit->get_start(),
                                rit->size(),
                                replace_map_[rit->get_keyword()]);
        }
        return result.empty() ? false : true;
    }

private:
    std::unordered_map<std::string, std::string> replace_map_;
    aho_corasick::trie trie_map_;
};

// example.cpp
// int main() {
//     StringSubtitutor subtitutor;
//     subtitutor.AddKeyword("one", "1");
//     subtitutor.AddKeyword("two", "2");
//     subtitutor.AddKeyword("three", "3");
//     subtitutor.AddKeyword("one two", "12");
//     subtitutor.AddKeyword("four", "4");
//     subtitutor.AddKeyword("가나다", "한글");
//     subtitutor.AddKeyword("여야", "하이");
//
//     std::string test1(" one two 123 three 가나다");
//     if (subtitutor.ReplaceKeywords(test1) == true) {
//       cout << test1 << endl;
//     }
//     return 0;
// }

#endif /* REPLACE_KEYWORD_H */
