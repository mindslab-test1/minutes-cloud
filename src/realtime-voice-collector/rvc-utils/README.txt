* aho_corasick
- 다량의 키워드가 하나의 문자열에 존재하는지 검색하는 알고리즘
- trie 자료구조를 이용
- URL: https://github.com/cjgdev/aho_corasick.git
- REVISION: b4fb360
