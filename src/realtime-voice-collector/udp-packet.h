#ifndef UDP_PACKET_H
#define UDP_PACKET_H

struct UdpPacket {
  UdpPacket(char *pkt_data, int pkt_len);

  uint32_t src_ip;
  uint32_t dst_ip;
  unsigned short src_port;
  unsigned short dst_port;

  char    *payload;
  uint32_t payload_len;
};

#endif /* UDP_PACKET_H */
