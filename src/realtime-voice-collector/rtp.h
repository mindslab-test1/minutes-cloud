#ifndef _RTP_H_
#define _RTP_H_

#include <string>

using std::string;

struct rtp_header {
  unsigned char v_p_x_cc;
  unsigned char m_pt;
  uint16_t seq_num;
  uint32_t timestamp;
  uint32_t ssrc;
  uint32_t csrc[];
} __attribute__ ((packed));

#endif
