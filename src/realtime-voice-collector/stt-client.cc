#include "app-variable.h"
#include "stt-client.h"
//#include <boost/algorithm/string.hpp>
#include <sys/time.h>
#include <algorithm>
#include <chrono>
#include <zmq.h>

#define QUOTE(...) #__VA_ARGS__

std::thread SttClient::monitor_thrd_;

SttClient::SttClient(std::shared_ptr<Channel> channel)
    : stub_(SpeechToTextService::NewStub(channel)) {
  pcm_fd_ = 0;
  result_fd_ = 0;
  last_result_time_ = 0;
}

SttClient::~SttClient() {
  if (result_thrd_.joinable()) {
    result_thrd_.join();
  }

  if (pcm_fd_ > 0) close(pcm_fd_);
  if (result_fd_ > 0) close(result_fd_);
}

void SttClient::ReadStream(ClientReaderWriter<Speech, Segment>* stream) {
  Segment segment;
  std::string buf;
  while (stream->Read(&segment)) {
    g_var.console->debug("result[{}_{}] ({}~{}): {}", call_id_.c_str(), talker_.c_str(), segment.start()/100, segment.end()/100, segment.txt());
    PublishText(segment.start(), segment.end(), segment.txt());
    last_result_time_ = segment.end();
    buf += segment.txt();

    // io 성능을 위해 데이터가 모이면 쓰자
    if (g_var.record_stt && result_fd_ > 0 && buf.length() >= 1024) {
      write(result_fd_, buf.c_str(), buf.length());
      buf.clear();
    }
  }

  if (g_var.record_stt && result_fd_ > 0 && !buf.empty()) {
    write(result_fd_, buf.c_str(), buf.length());
  }

  int end;
  timeval tv_diff;
  timersub(&stop_time_, &start_time_, &tv_diff);
  end = tv_diff.tv_sec * 100 + tv_diff.tv_usec / 10000;
  PublishStop();
}

std::string get_subdir(timeval time)
{
	time_t ltime;
	struct tm info;
	char subdir[256] = {0, };

	ltime = time.tv_sec;
	localtime_r(&ltime, &info);
	snprintf(subdir, sizeof(subdir), "%d%02d%02d/%02d", info.tm_year + 1900, info.tm_mon + 1, info.tm_mday, info.tm_hour);

	return std::string(subdir);
}

void SttClient::PublishStart()
{
  char buf[4096] = {0, };
  if (g_var.publisher_) {
	char *fmt = QUOTE(
		{
			"TYPE":"START",
			"CALL-ID":"%s",
			"START-TIME":"%d",
			"AGENT-IP":"%s",
			"CUSTOMER-NUMBER":"%s",
			"REC-PATH":"%s",
			"TALKER":"%s"
		}
	);
	std::string subdir = get_subdir(start_time_);
	//std::string rec_path = g_var.call_final_path + "/" + subdir + "/" + call_id_ + talker_ + ".pcm";
	std::string rec_path = g_var.call_final_path + "/" + subdir + "/" + call_id_ + ".pcm";
	snprintf(buf, sizeof(buf), fmt, call_id_.c_str(), int(start_time_.tv_sec), device_ip_.c_str(), tel_no_.c_str(), rec_path.c_str(), talker_.c_str());
    g_var.push_lock.lock();
    zmq_send(g_var.publisher_, buf, strlen(buf), ZMQ_NOBLOCK);
    g_var.push_lock.unlock();
  }
	return;
}

void SttClient::PublishStop()
{
  char buf[4096] = {0, };
  if (g_var.publisher_) {
	char *fmt = QUOTE(
		{
			"TYPE":"STOP",
			"CALL-ID":"%s",
			"STOP-TIME":"%d",
			"TALKER":"%s"
		}
	);
	snprintf(buf, sizeof(buf), fmt, call_id_.c_str(), int(stop_time_.tv_sec), talker_.c_str());
    g_var.push_lock.lock();
    zmq_send(g_var.publisher_, buf, strlen(buf), ZMQ_NOBLOCK);
    g_var.push_lock.unlock();
  }
	return;
}

void SttClient::PublishText(int start, int end, std::string txt) {
  char buf[4096] = {0, };
  if (g_var.publisher_) {
	char *fmt = QUOTE(
		{
			"TYPE":"TEXT",
			"CALL-ID":"%s",
			"TALKER":"%s",
			"TEXT":"%s",
			"TEXT-START":"%.2f",
			"TEXT-END":"%.2f"
		}
	);
	snprintf(buf, sizeof(buf), fmt, call_id_.c_str(), talker_.c_str(), txt.c_str(), start / 100.0, end / 100.0);
    g_var.push_lock.lock();
    zmq_send(g_var.publisher_, buf, strlen(buf), ZMQ_NOBLOCK);
    g_var.push_lock.unlock();
  }

  // call_id, start_time, end_time, text
  LOGGER()->debug("result: {}", txt);
  LOGGER()->debug("result[{}:{}]: {}", tel_no_.c_str(), call_id_.c_str(), txt);
}

bool SttClient::Start(std::string model, std::string lang, std::string rate, std::string unique_id, std::string device_ip, std::string tel_no, std::string talker, timeval start_time) {
  call_id_ = unique_id;
  unique_id_ = unique_id;
  device_ip_ = device_ip;
  tel_no_ = tel_no;
  talker_ = talker;

  gettimeofday(&start_time_, NULL);

  if (g_var.record_call) {
    pcm_tempf_ = g_var.call_temp_path + "/" + call_id_ + "_" + talker_ + ".tmp";
    pcm_fd_ = open(pcm_tempf_.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644);
    LOGGER()->debug("PCM temp file_name is {}", pcm_tempf_);
  }
  if (g_var.record_stt) {
    result_tempf_ = g_var.call_temp_path + "/" + call_id_ + ".dat";
    result_fd_ = open(result_tempf_.c_str(), O_WRONLY | O_CREAT | O_APPEND, 0644);
    LOGGER()->debug("STT Result temp file_name is {}", result_tempf_);
  }

  resp.Clear();
  ctx_.reset(new ClientContext);
  ctx_->AddMetadata("in.lang", lang);
  ctx_->AddMetadata("in.samplerate", rate);
  ctx_->AddMetadata("in.model", model);
  LOGGER()->info("SttClient::Start() MODEL[{}], MODEL[{}], RATE[{}]", model, lang, rate);
  g_var.console->info("SttClient::Start() MODEL[{}], MODEL[{}], RATE[{}]", model, lang, rate);

  stream_ = stub_->StreamRecognize(ctx_.get());
  result_thrd_ = std::thread(&SttClient::ReadStream, this, stream_.get());

  g_var.console->info("SttClient started... ");
  PublishStart();

  return true;
}

std::string to_date(timeval tv) {
  struct tm *info;
  char tmp[80];
  char buffer[80];
  info = localtime(&tv.tv_sec);

  strftime(tmp, sizeof(tmp) , "%H:%M:%S", info);
  snprintf(buffer, sizeof(buffer), "%s.%03ld", tmp, tv.tv_usec / 1000);
  return std::string(buffer);
}



static void _mkdir(const char *dir) {
	char tmp[256];
	char *p = NULL;
	size_t len;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	len = strlen(tmp);
	if(tmp[len - 1] == '/') {
		tmp[len - 1] = 0;
	}
	for(p = tmp + 1; *p; p++) {
		if(*p == '/') {
			*p = 0;
			mkdir(tmp, S_IRWXU);
			*p = '/';
		}
	}
	mkdir(tmp, S_IRWXU);
}


void make_finaldir(std::string subdir)
{
	char dirpath[256] = {0, };
	snprintf(dirpath, sizeof(dirpath), "%s/%s", g_var.call_final_path.c_str(), subdir.c_str());
	if (access(dirpath, 00) != 0) {
		_mkdir(dirpath);
	}
}

void SttClient::Stop(timeval end_time) {
  g_var.console->info("SttClient stopped... ");
  gettimeofday(&stop_time_, NULL);

  if (sndbuf_.size() > 0) {
    Speech speech;
    speech.set_bin(sndbuf_);
    if (g_var.record_call) {
      WritePCM(sndbuf_, true);
    }
    sndbuf_.clear();
	if (g_var.is_active) {
      if (stream_->Write(speech) == false) {
        LOGGER()->error("stt client write fail {}", phone_number_);
      }
	}
  }
  stream_->WritesDone();
  if (result_thrd_.joinable()) {
    result_thrd_.join();
  }
  auto status = stream_->Finish();
  if (status.ok()) {
  } else {
    LOGGER()->info("[SttClient]failed: {}", status.error_message());
  }
  g_var.console->info("SttClient stream finished... ");

  // 녹취 파일 정리
  std::string subdir = get_subdir(start_time_);
  make_finaldir(subdir);
  if (g_var.record_call && pcm_fd_ > 0) {
    close(pcm_fd_);
    pcm_fd_ = 0;
	#if 0
	std::string subdir = get_subdir(start_time_);
	make_finaldir(subdir);
	#endif
	std::string pcm_final = g_var.call_final_path + "/" + subdir + "/" + call_id_ + ".pcm";
    //std::string pcm_final = g_var.call_final_path + "/" + call_id_ + ".pcm";
    if (rename(pcm_tempf_.c_str(), pcm_final.c_str()) != 0) {
      LOGGER()->error("Move PCM file error : {} -> {}, {}",
                      pcm_tempf_, pcm_final, strerror(errno));
    }
  }

  // STT Result 파일 정리
  if (g_var.record_stt && result_fd_ > 0) {
    close(result_fd_);
    result_fd_ = 0;
    std::string result_final = g_var.call_final_path + "/" + subdir + "/" + call_id_ + ".result";
    if (rename(result_tempf_.c_str(), result_final.c_str()) != 0) {
      LOGGER()->error("Move Result file error : {} -> {}, {}",
                      result_tempf_, result_final, strerror(errno));
    }
  }
}

void SttClient::WritePCM(std::string &buf, bool save_all) {
  audio_buf_.append(buf);
  if (audio_buf_.size() >= 1024 || save_all) {
    if (write(pcm_fd_, audio_buf_.data(), audio_buf_.size()) == -1) {
      LOGGER()->error("File Writing is failed, {}", strerror(errno));
    }
    audio_buf_.clear();
  }
}

bool SttClient::Write(const char* buf, size_t size, timeval timestamp) {
  sndbuf_.append(buf, size);
  if (sndbuf_.size() < 160 * 20) {
    return true;
  }

  Speech speech;
  //speech.set_bin(buf, size);
  speech.set_bin(sndbuf_);

  if (g_var.record_call) {
    WritePCM(sndbuf_);
  }
  sndbuf_.clear();

  if (g_var.is_active) {
    if (stream_->Write(speech) == false) {
      LOGGER()->error("stt client write fail {}", call_id_);
      return false;
    }
  }

  return true;
}

bool SttClient::Ping(std::string addr) {
  // Find STT Child Process
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<SttModelResolver::Stub> stub(SttModelResolver::NewStub(channel));

  ClientContext ctx;
  ServerStatus server_status;

  Model model;
  model.set_model(g_var.sttd_model);
  model.set_sample_rate(8000);
  model.set_lang(LangCode::kor);
  LOGGER()->debug("SttClient::Ping() MODEL[{}], MODEL[{}], MODEL[8000]", g_var.sttd_model, g_var.sttd_lang);

  grpc::Status status = stub->Find(&ctx, model, &server_status);
  if (status.error_code() != grpc::StatusCode::OK) {
    g_var.console->error("GRPC Find({}) error is {}", addr, status.error_message());
    return false;
  }

  // Ping to STT Child Process
  auto real_channel =
    grpc::CreateChannel(server_status.server_address(), grpc::InsecureChannelCredentials());
  auto real_stub = SttRealService::NewStub(real_channel);

  ClientContext real_ctx;
  ServerStatus real_server_status;
  grpc::Status real_status = real_stub->Ping(&real_ctx, model, &real_server_status);
  if (real_server_status.running() != true) {
    g_var.console->error("GRPC Ping({}) status is {}:{}",
                        addr, real_server_status.running(), real_server_status.invoked_by());
    return false;
  }

  return true;
}

void SttClient::StartMonitor() {
  monitor_thrd_ = std::thread(SttClient::CheckAlive);
}

void SttClient::StopMonitor() {
  if (monitor_thrd_.joinable()) {
    monitor_thrd_.join();
  }
}

void SttClient::CheckAlive() {
}

bool SttClient::Ping(std::string addr, std::string &real_addr) {
  // Find STT Child Process
  auto channel = grpc::CreateChannel(addr, grpc::InsecureChannelCredentials());
  std::unique_ptr<SttModelResolver::Stub> stub(SttModelResolver::NewStub(channel));

  ClientContext ctx;
  ServerStatus server_status;

  Model model;
  model.set_model(g_var.sttd_model);
  model.set_sample_rate(8000);
  model.set_lang(LangCode::kor);
  LOGGER()->debug("SttClient::Ping() MODEL[{}], MODEL[{}], MODEL[8000]", g_var.sttd_model, g_var.sttd_lang);

  grpc::Status status = stub->Find(&ctx, model, &server_status);
  if (status.error_code() != grpc::StatusCode::OK) {
    g_var.console->error("GRPC Find({}) error is {}", addr, status.error_message());
    return false;
  }

  // Ping to STT Child Process
  auto real_channel =
    grpc::CreateChannel(server_status.server_address(), grpc::InsecureChannelCredentials());
  auto real_stub = SttRealService::NewStub(real_channel);

  ClientContext real_ctx;
  ServerStatus real_server_status;
  grpc::Status real_status = real_stub->Ping(&real_ctx, model, &real_server_status);
  if (real_server_status.running() != true) {
    g_var.console->error("GRPC Ping({}) status is {}:{}",
                        addr, real_server_status.running(), real_server_status.invoked_by());
    return false;
  }
  real_addr = server_status.server_address();
  //g_var.console->info("real server is {}, {}", real_addr, addr);

  return true;
}
