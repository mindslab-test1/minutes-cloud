#! /usr/local/bin/python
# -*- coding: utf-8 -*-
import os, copy, sys, time, json
import multiprocessing
import ConfigParser
import zmq
import common.minds_ipc as IPC
#import common.logger as logger


'''
[프로그램 설명]
1. rtp send simulator
2. ffmpeg을 이용 rtp 메시지 전송
3. 화자분리
4. STT 분석
4. MIPD로 메시지 전달.
'''
g_sim_info={}

class ZmqPipline:
	def __init__(self, log=None):
		self.log = log
		self.context = None
		self.socket = None
	
	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		#self.context = zmq.Context()
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" %(collector_ip, collector_port))
		if self.log :
			self.log.critical("ZmqPipline:: connect() SUCC -> <%s:%s>", collector_ip, collector_port)
	
	def send(self, msg, flags=0):
		self.socket.send(msg, flags=flags)
			
	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" %bind_port)
		if self.log :
			self.log.critical("ZmqPipline:: bind() SUCC -> <*:%s>", bind_port)
	
	def recv(self, flags=0):
		return self.socket.recv(flags=flags)
	
	def close(self):
		#self.socket.close()
		self.socket.close(linger=1)
		self.context.term()


def send_rtp(file_path, target_addr) :
	#command="echo 'Y' | ffmpeg -i " + file_path + ' -ar ' + stt['stt_rate']+ ' ' + output_wav + ' > /dev/null  2>&1'
	#command="ffmpeg -re -i {} -vn -acodec pcm_alaw -f rtp rtp://{}?pkt_size=172 > /dev/null  2>&1" .format(file_path, target_addr)
	command="ffmpeg -re -i {} -vn -acodec pcm_alaw -f rtp rtp://{}?pkt_size=172 > /dev/null  2>&1" .format(file_path, target_addr)
	process = os.popen(command)
	result=process.read()

def send_event_start(zq) :
	session_cnt=int(g_sim_info['session_cnt'])
	local_ip=g_sim_info['local_ip']
	target_ip=g_sim_info['rtp_target_ip']
	target_port=g_sim_info['rtp_target_port']

	push_msg={}

	if True :
		push_msg['msg_header']={}
		push_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
		push_msg['msg_body']={}
		push_msg['msg_body']['type']='REALTIME'
		push_msg['msg_body']['status']='#START#'
		push_msg['msg_body']['channel_key']='sim'
		push_msg['msg_body']['session']=[]
		for i in range(session_cnt) :
			session={}
			session['session_key']=str(i)
			session['src_ip']=local_ip
			session['dst_port']=str(int(target_port) + int(i))
			push_msg['msg_body']['session'].append(session)

		push_msg['msg_body']['stt_meta']={}
		push_msg['msg_body']['container']={}

	elif True :
		push_msg['CALL-ID']='sim'+str(idx)
		push_msg['TYPE']='START'
		push_msg['MIC_INFO']='{}:{}'.format(local_ip, target_port)
		push_msg['MIC_NAME']='{}:{}'.format(local_ip, target_port)
		push_msg['CUSTOMER-NUMBER']='{}:{}'.format(local_ip, target_port)
		push_msg['STT_MODEL_SER']='0'
		#push_msg['STT_ADDR']='10.122.64.184'
		push_msg['STT_ADDR']='127.0.0.1:9802'
		push_msg['STT_NAME']='aaaa'
		push_msg['STT_LANG']='kor'
		push_msg['STT_RATE']='8000'

	json_string = json.dumps(push_msg)
	zq.send(json_string)

	print('[#START#] [{}]'.format('sim'))
	for session in push_msg['msg_body']['session'] :
		print('[REGI MIC] [{} ({}/{})]'.format(session['session_key'], session['src_ip'], session['dst_port']))


def send_event_stop(zq) :
	target_ip, target_port = target_addr.split(':')

	push_msg={}
	if True :
		push_msg['msg_header']={}
		push_msg['msg_header']['msg_id']='EVENT_COLLECTION_PUSH'
		push_msg['msg_body']={}
		push_msg['msg_body']['type']='REALTIME'
		push_msg['msg_body']['status']='#END#'
		push_msg['msg_body']['channel_key']='sim'
		push_msg['msg_body']['stt_meta']={}
		push_msg['msg_body']['container']={}
	elif True :
		push_msg['CALL-ID']='sim'+str(idx)
		push_msg['TYPE']='STOP'
		push_msg['MIC_INFO']='{}:{}'.format(local_ip, target_port)
		push_msg['STT_MODEL_SER']='0'
		push_msg['STT_ADDR']='127.0.0.1:9802'
		push_msg['STT_NAME']='aaaa'
		push_msg['STT_LANG']='kor'
		push_msg['STT_RATE']='8000'

	json_string = json.dumps(push_msg)
	zq.send(json_string)
	print('Send Event [#END#] [{}/{} ({}/{})]'.format('sim', str(idx), target_ip, target_port))


def working_function(idx, target_addr, react_cnt) :

	unlimit_flag=False
	if react_cnt== 0 :
		unlimit_flag==True

	print("Start Working Thread[{}] :: Target[{}]" .format(idx, target_addr))
	if int(g_sim_info['none_wav_session_interval']) == 0 :
		wav_path=g_sim_info['wav_path']
	else :
		if idx % int(g_sim_info['none_wav_session_interval']) == 0 :
			wav_path=g_sim_info['none_wav_path']
			#none_wav session is unlimit !!!!!!!!!!!!!!
			unlimit_flag=True
		else :
			wav_path=g_sim_info['wav_path']

	print("Path : {}".format(wav_path))

	while (unlimit_flag == True) or react_cnt> 0:
		if react_cnt> 0 :
			react_cnt= react_cnt - 1

		print("[{}] 1 list Complete".format(target_addr))
		for path, directory, files in os.walk(wav_path):
			for filename in files:
				ext = os.path.splitext(filename)[-1]
				if ext == ".pcm" or ext == ".wav" or ext ==".m4a" :
					full_path = os.path.join(path, filename)
					print("Working Thread[{}] :: Send[{}]" .format(idx, full_path))
					send_rtp(full_path, target_addr) 

	print("[{}] Terminate".format(target_addr))


def main() :

	#conf=simd_conf.Config_Parser("./rtp_sim.conf")
	print("Start RTP SIM")

	#conf=simd_conf.Config_Parser("/home/minds/git/minutes/simul/rtp_sim/rtp_sim.conf")
	#g_sim_info['wav_path']=conf.DIS_Item_Value("sim_info", "wav_path")

	g_sim_info['proc_name']='rtp_sim'

	conf=ConfigParser.ConfigParser()
	conf.read("./rtp_sim.conf")

	items=conf.items('sim_info')
	for name, value in items :
		g_sim_info[name]=value
	if 'wav_path' not in g_sim_info :
		print("wav_path is Not Found [check 'rtp_sim.conf']")
	elif 'react_cnt' not in g_sim_info :
		print("react_cnt is Not Found [check 'rtp_sim.conf']")
	elif 'session_cnt' not in g_sim_info :
		print("session_cntis Not Found [check 'rtp_sim.conf']")
	elif 'rtp_target_ip' not in g_sim_info :
		print("target_ip is Not Found [check 'rtp_sim.conf']")
	elif 'rtp_target_port' not in g_sim_info :
		print("target_port is Not Found [check 'rtp_sim.conf']")
	elif 'event_target_ip' not in g_sim_info :
		print("target_ip is Not Found [check 'rtp_sim.conf']")
	elif 'event_target_port' not in g_sim_info :
		print("target_port is Not Found [check 'rtp_sim.conf']")


	#react_cnt=int(conf.DIS_Item_Value("sim_info", "react_cnt"))
	react_cnt=int(g_sim_info['react_cnt'])

	#items=conf.items('event_msg')
	#for name, value in items :
	#	g_sim_info[name]=value
#	zq=ZmqPipline()
#	zq.connect(g_sim_info['event_target_ip'],g_sim_info['event_target_port']) 


	worker_list = list()

	#target_list_cnt=conf.DIS_Item_Value("auto_increase_list", "target_list_cnt")
	#target_ip=conf.DIS_Item_Value("auto_increase_list", "target_ip")
	#target_port=conf.DIS_Item_Value("auto_increase_list", "target_port")

	session_cnt=int(g_sim_info['session_cnt'])
	target_ip=g_sim_info['rtp_target_ip']
	target_port=g_sim_info['rtp_target_port']

	#global log
	#log = logger.create_logger(os.getenv('MAUM_ROOT') + '/logs', g_sim_info['proc_name'], g_sim_info['log_level'], True)
	zq=ZmqPipline()
	zq.connect(g_sim_info['event_target_ip'],g_sim_info['event_target_port']) 


	send_event_start(zq)

	for i in range(int(session_cnt)) :
		target_addr="{}:{}" .format(target_ip, str(int(target_port)+i))
		p = multiprocessing.Process(target=working_function, args=(i, target_addr, react_cnt))
		p.daemon = True
		p.start()
		worker_list.append(p)

	for p in worker_list:
		p.join()

	send_event_stop(zq)


	return 0


main()
