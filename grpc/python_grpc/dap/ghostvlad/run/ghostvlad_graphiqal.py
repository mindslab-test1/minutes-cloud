import os
import sys
import torch
import librosa
import numpy as np

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/ghostvlad')
from core.model import VGGSR
from core.utils import read_wav
from core.hparams import load_hparam_str


class GhostVLAD:
    def __init__(self, checkpoint_path, device):
        chkpt = torch.load(checkpoint_path, map_location='cpu')
        hp =load_hparam_str(chkpt['hp_str'])
        self.hp = hp

        #torch.cuda.set_device(device)
        os.environ["CUDA_VISIBLE_DEVICES"] = str(device)
        self.device = device
        self.model = VGGSR(hp).cuda()
        self.model.load_state_dict(chkpt['model'])
        self.model.eval()

        del chkpt
        torch.cuda.empty_cache()

    def GetDvectorFromWav(self, wav):
        # wav: list of float values
        with torch.no_grad():
            wav = torch.tensor(wav)
            wav = wav.cuda()
            wav = wav.unsqueeze(0)
            dvec = self.model.inference(wav).squeeze(0)
            dvec = dvec.cpu().detach().tolist()
            return dvec

def compare_list(x, y):
    x = x.cpu().numpy().tolist()
    assert np.max(np.abs(np.array(x) - np.array(y))) < 1e-6


if __name__ == '__main__':
    sr, ref_input1 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "moon1.wav"))
    sr, ref_input2 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "moon2.wav"))
    sr, ref_input3 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "park1.wav"))
    sr, ref_input4 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "park2.wav"))

    ref_output1 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "moon1.gvlad"))
    ref_output2 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "moon2.gvlad"))
    ref_output3 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "park1.gvlad"))
    ref_output4 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'ghostvlad', "park2.gvlad"))

    model = GhostVLAD(
        checkpoint_path=os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'ghostvlad', 'nolrdecay_c07b57c_103.pt'),
        device=0)

    result_output1 = model.GetDvectorFromWav(ref_input1)
    result_output2 = model.GetDvectorFromWav(ref_input2)
    result_output3 = model.GetDvectorFromWav(ref_input3)
    result_output4 = model.GetDvectorFromWav(ref_input4)

    compare_list(ref_output1, result_output1)
    compare_list(ref_output2, result_output2)
    compare_list(ref_output3, result_output3)
    compare_list(ref_output4, result_output4)

