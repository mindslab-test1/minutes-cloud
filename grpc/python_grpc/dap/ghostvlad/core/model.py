import torch
import torch.nn as nn
import torch.nn.functional as F

from .blocks import Block
from .vlad import VladPooling
from .initializer import init


class VGGSR(nn.Module):
    def __init__(self, hp):
        super(VGGSR, self).__init__()
        self.hp = hp
        self.window = torch.hann_window(window_length=hp.audio.win_length)

        self.thinResNet = nn.Sequential(
            # Convolution Block 1
            init(nn.Conv2d(1, 64, kernel_size=7, padding=3, bias=False)),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            # [B, 64, 257, T]

            nn.MaxPool2d(kernel_size=2, stride=2),
            # [B, 64, 128, T/2]

            # Convolution Section 2
            Block(3, [64, 48, 48, 96], identity=False),
            Block(3, [96, 48, 48, 96], identity=True),
            # [B, 96, 64, T/2]

            # Convolution Section 3
            Block(3, [96, 96, 96, 128], stride=2, identity=False),
            Block(3, [128, 96, 96, 128], identity=True),
            Block(3, [128, 96, 96, 128], identity=True),
            # [B, 128, 32, T/4]

            # Convolution Section 4
            Block(3, [128, 128, 128, 256], stride=2, identity=False),
            Block(3, [256, 128, 128, 256], identity=True),
            Block(3, [256, 128, 128, 256], identity=True),
            # [B, 256, 16, T/8]

            # Convolution Section 5
            Block(3, [256, 256, 256, 512], stride=2, identity=False),
            Block(3, [512, 256, 256, 512], identity=True),
            Block(3, [512, 256, 256, 512], identity=True),
            # [B, 512, 16, T/16]

            nn.MaxPool2d(kernel_size=(3, 1), stride=(2, 1)), # inconsistent with paper!
            # [B, 512, 7, T/16]
        )
        self.fc = init(nn.Conv2d(512, hp.model.dim, kernel_size=(7, 1), bias=True))
        # [B, 512, 1, T/16]
        self.center = init(nn.Conv2d(512, hp.model.cluster.K + hp.model.cluster.G, kernel_size=(7, 1), bias=True))
        self.vlad = VladPooling(hp)

        self.fc2 = init(nn.Linear(hp.model.cluster.K * hp.model.dim, hp.model.dim))

        self.glu1 = nn.Linear(hp.model.dim, hp.model.dim)
        self.glu2 = nn.Linear(hp.model.dim, hp.model.dim)

        self.fc3 = init(nn.Linear(hp.model.dim, hp.data.num_classes))

    def inference(self, x):
        x = self.make_spectrogram(x) # [B, 257, T]
        x = x.unsqueeze(1) # x: [B, 1, 257, T]
        x = self.thinResNet(x) # [B, 512, 7, T/16]
        x_fc = F.relu(self.fc(x)) # [B, 512, 1, T/16]
        x_k_center = self.center(x) # [B, K+G, 1, T/16]
        x = self.vlad(x_fc, x_k_center) # [B, K*D]
        x = self.fc2(x) # [B, D]
        x = self.glu1(x) * torch.sigmoid(self.glu2(x)) # [B, D]
        # x = F.relu(x)
        x = x / torch.norm(x, p=2, dim=-1, keepdim=True) # [B, D]
        return x

    def make_spectrogram(self, x, train=True):
        x = torch.stft(x, n_fft=self.hp.audio.n_fft,
                       hop_length=self.hp.audio.hop_length,
                       win_length=self.hp.audio.win_length,
                       window=self.window)
        x = torch.norm(x, p=2, dim=-1)

        mu = torch.mean(x, 1, keepdim=True)
        std = torch.std(x, 1, keepdim=True)
        return (x - mu) / (std + 1e-5)

    def forward(self, x):
        x = self.inference(x) # [B, D]
        x = self.fc3(x) # [B, num_classes]
        return x

    def get_loss(self, y_pred, target):
        # make one-hot from `target`
        # example: torch.zeros(2, 10).scatter_(1, torch.tensor([[2], [3]]), 1)
        y_true = torch.zeros(target.size(0), self.hp.data.num_classes).cuda()
        if len(target.shape) == 1:
            target = target.unsqueeze(1)
        y_true = y_true.scatter_(1, target, 1)

        y = y_true * (y_pred - self.hp.model.loss.margin) + (1 - y_true) * y_pred
        y *= self.hp.model.loss.scale
        y = F.log_softmax(y, dim=1)
        return F.nll_loss(y, target.squeeze(1))
