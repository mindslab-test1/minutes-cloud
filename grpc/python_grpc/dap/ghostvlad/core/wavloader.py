import os
import glob
import torch
import random
import librosa
import numpy as np
from scipy.io.wavfile import read
from torch.utils.data import Dataset, DataLoader

from ..core.utils import wav_formatter


def create_dataloader(hp, args, train):
    if train:
        return DataLoader(dataset=VoxDataset(hp, args, True),
                          batch_size=hp.train.batch_size,
                          shuffle=True,
                          num_workers=hp.train.num_workers,
                          pin_memory=True,
                          drop_last=True)
    else:
        return DataLoader(dataset=VoxDataset(hp, args, False),
                          batch_size=1,
                          shuffle=False,
                          num_workers=hp.test.num_workers,
                          pin_memory=True,
                          drop_last=False)


class VoxDataset(Dataset):
    def __init__(self, hp, args, train):
        self.hp = hp
        self.args = args
        self.train = train
        self.data_dir = hp.data.vox2_train if train else hp.data.vox1_test

        with open(os.path.join('datasets', 'blacklist.txt'), 'r') as f:
            blacklist = [x.strip() for x in f.readlines()]

        if train:
            temp = np.loadtxt(os.path.join('datasets', 'meta', 'voxlb2_train.txt'),
                dtype={'names': ('wav', 'label'), 'formats': ('U100', np.int)})
            self.pairs = [x for x in temp if x[0].split('/')[0] not in blacklist]
        else:
            self.pairs = np.loadtxt(os.path.join('datasets', 'meta', 'voxceleb1_veri_test.txt'),
                dtype={'names': ('label', 'wav1', 'wav2'), 'formats': (np.int, 'U100', 'U100')})

    def __len__(self):
        return len(self.pairs)

    def __getitem__(self, idx):
        if self.train:
            wavpath, label = self.pairs[idx]
            wav = self.load_wav(wavpath)
            return wav, label
        else:
            label, wav1, wav2 = self.pairs[idx]
            wav1 = self.load_wav(wav1)
            wav2 = self.load_wav(wav2)
            return label, wav1, wav2

    def load_wav(self, path):
        path = path.replace('.wav', self.hp.data.file_format[1:])
        sr, wav = read(os.path.join(self.data_dir, path))
        assert sr == self.hp.audio.sr

        wav = wav_formatter(wav)

        # when training, we need to match temporal dimension to stack.
        if self.train:
            cut = self.hp.audio.hop_length * self.hp.model.temporal - 1 # 39999
            start = random.randint(0, wav.shape[0] - cut)
            wav = wav[start:start+cut]
        return wav
