import os
import math
import tqdm
import torch
import torch.nn as nn
import itertools
import traceback

from ..train.validation import validate
from ..core.utils import get_commit_hash
from ..core.model import VGGSR

def train(args, pt_dir, chkpt_path, trainloader, testloader, writer, logger, hp, hp_str):
    model = VGGSR(hp).cuda()

    if hp.train.optimizer == 'adam':
        optimizer = torch.optim.Adam(
            model.parameters(), lr=hp.train.adam.initlr)
    else:
        raise Exception("%s optimizer not supported" % hp.train.optimizer)

    githash = get_commit_hash()

    init_epoch = -1
    step = 0

    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        model.load_state_dict(checkpoint['model'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        step = checkpoint['step']
        init_epoch = checkpoint['epoch']
        githash = checkpoint['githash']
        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint. Will use new.")
    else:
        logger.info("Starting new training run.")

    try:
        model.train()
        for epoch in itertools.count(init_epoch+1):
            loader = tqdm.tqdm(trainloader, desc='Train data loader')
            for spec, target in loader:
                spec = spec.cuda()
                target = target.cuda()
                output = model(spec)
                loss = model.get_loss(output, target)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                step += 1

                loss = loss.item()
                if loss > 1e8 or math.isnan(loss):
                    logger.error("Loss exploded to %.02f at step %d!" % (loss, step))
                    raise Exception("Loss exploded")

                if step % hp.log.summary_interval == 0:
                    writer.log_training(loss, step)
                    loader.set_description('Loss %.02f at step %d' % (loss, step))

            save_path = os.path.join(pt_dir, '%s_%s_%03d.pt' \
                % (args.name, githash, epoch))
            torch.save({
                'model': model.state_dict(),
                'optimizer': optimizer.state_dict(),
                'step': step,
                'epoch': epoch,
                'hp_str': hp_str,
                'githash': githash,
            }, save_path)
            logger.info("Saved checkpoint to: %s" % save_path)

            validate(model, testloader, writer, step)

    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
