import os
import sys
import torch
import librosa
import numpy as np

sys.path.insert(0, '/home/msl/gom/brain-dap/pysrc/maum/brain/dap/dvectorize')
from core.model import GE2ELoss
from core.utils import read_wav
from core.hparams import load_hparam_str


class Dvectorize:
    def __init__(self, checkpoint_path, device):
        chkpt = torch.load(checkpoint_path, map_location='cpu')
        hp = load_hparam_str(chkpt['hp_str'])
        self.hp = hp

        #torch.cuda.set_device(device)
        os.environ["CUDA_VISIBLE_DEVICES"] = str(device)
        self.device = device
        self.model = GE2ELoss(hp).cuda()
        self.model.load_state_dict(chkpt['model'])
        self.model.eval()

        del chkpt
        torch.cuda.empty_cache()

    def GetDvectorFromWav(self, wav, avg):
        with torch.no_grad():
            wav = torch.tensor(wav)
            wav = wav.cuda()
            mel = self.model.embedder.wav2mel(wav)
            dveclist = self.model.embedder.inference(mel, avg=avg)
            dveclist = dveclist.float().view(-1).cpu().detach().tolist()
            return dveclist


def compare_dvector_list(x, y):
    assert np.max(np.abs(np.array(x) - np.array(y))) < 1e-6


if __name__ == '__main__':
    sr, ref_input1 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "moon1.wav"))
    sr, ref_input2 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "moon2.wav"))
    sr, ref_input3 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "park1.wav"))
    sr, ref_input4 = read_wav(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "park2.wav"))

    ref_output1 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "moon1.dvec"))
    ref_output2 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "moon2.dvec"))
    ref_output3 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "park1.dvec"))
    ref_output4 = torch.load(os.path.join(os.environ['MAUM_ROOT'], 'samples', 'dap', 'dvectorize', "park2.dvec"))

    model = Dvectorize(
        checkpoint_path=os.path.join(os.environ['MAUM_ROOT'], 'trained', 'dap', 'dvectorize', 'vf_adam_80_271000_v2.pt'),
        device=0)

    result_output1 = model.GetDvectorFromWav(ref_input1, True)
    result_output2 = model.GetDvectorFromWav(ref_input2, True)
    result_output3 = model.GetDvectorFromWav(ref_input3, True)
    result_output4 = model.GetDvectorFromWav(ref_input4, True)

    compare_dvector_list(ref_output1, result_output1)
    compare_dvector_list(ref_output2, result_output2)
    compare_dvector_list(ref_output3, result_output3)
    compare_dvector_list(ref_output4, result_output4)
