import torch
import numpy as np
from spectralcluster import SpectralClusterer

from ..core.utils import log_error
from ...coreutils.hparams import load_hparam_str

from ...dap_pb2_grpc import DiarizeServicer
from ...dap_pb2 import DiarizeResult, EmbConfig, DiarizeConfig

from .dvectorize_client import DvectorizeClient

def load_checkpoint(chkpt_path):
    checkpoint = torch.load(chkpt_path, map_location='cpu')
    model = checkpoint['model'] # None
    hp_str = checkpoint['hp_str']
    hp = load_hparam_str(hp_str)

    return hp, model

class DiarizeInference(DiarizeServicer):
    def __init__(self, chkpt_path, args, logger, conf):
        self.args = args
        self.logger = logger
        hp, _ = load_checkpoint(chkpt_path)

        self.clusterer = SpectralClusterer(
            min_clusters=hp.inf.spectral.min_clusters,
            max_clusters=hp.inf.spectral.max_clusters,
            p_percentile=hp.inf.spectral.p_percentile,
            gaussian_blur_sigma=hp.inf.spectral.gaussian_blur_sigma,
        )

        self.emb_config = EmbConfig(
            emb_dim=hp.model.emb_dim,
        )

        self.diarize_config = DiarizeConfig(
            stft_hop=hp.audio.hop_length,
            stft_win=hp.audio.win_length,
            dvec_hop=hp.inf.stride,
            dvec_win=hp.inf.window,
            emb_dim=hp.model.emb_dim,
            sampling_rate=hp.audio.sample_rate,
            min_clusters=hp.inf.spectral.min_clusters,
            max_clusters=hp.inf.spectral.max_clusters,
            p_percentile=hp.inf.spectral.p_percentile,
            gaussian_blur_sigma=hp.inf.spectral.gaussian_blur_sigma,
        )

        self.emb_dim = self.emb_config.emb_dim

        self.dvec_client = DvectorizeClient(
            remote=conf.get('brain-dap.dvec.remote'))

    @log_error
    def GetDiarization(self, emblist, context):
        emblist = emblist.data
        assert len(emblist) % self.emb_dim == 0
        emblist = np.array(emblist)
        emblist = emblist.reshape(-1, self.emb_dim)
        labels = self.clusterer.predict(emblist)
        return DiarizeResult(data=list(labels))

    @log_error
    def GetDiarizationFromWav(self, wav_binary, context):
        emblist = self.dvec_client.get_dvec_from_wav(wav_binary).data

        assert len(emblist) % self.emb_dim == 0
        emblist = np.array(emblist)
        emblist = emblist.reshape(-1, self.emb_dim)
        labels = self.clusterer.predict(emblist)
        return DiarizeResult(data=list(labels))

    @log_error
    def GetEmbConfig(self, empty, context):
        return self.emb_config

    def GetDiarizeConfig(self, empty, context):
        return self.diarize_config
