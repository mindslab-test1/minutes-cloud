#!/usr/bin/env bash

# Check OS
OS=
if [ -f /usr/lib/os-release ]; then
  OS=ubuntu
elif [ -f /etc/centos-release ]; then
  OS=centos
elif [ -f /etc/redhat-release ]; then
  OS=centos
elif [ -f /etc/system-release ]; then
  OS=AMI
else
  echo "Illegal OS, use ubuntu, centos or AMI"
  exit 1
fi

#install lsb_release
if [ ! -f /usr/bin/lsb_release ]; then
  if [ "${OS}" == "centos" ]; then
    sudo yum install -y redhat-lsb-core-4.1-27.el7.centos.1.x86_64
  else [ "${OS}" == "AMI" ]
    sudo yum install -y redhat-lsb-core-4.0-7.14.amzn1.x86_64
  fi
fi

# ubuntu Or centos Or AMI
[[ "${OS}" == "ubuntu" || "${OS}" == "centos" || "${OS}" == "AMI" ]] && {
  version="$(lsb_release -r | awk '{print $2=$2}')" # trim
  case "${version}" in
    16.04|7.*) # systemd
      systemctl --user enable maumai-sv.service
      echo "[+] please run 'systemctl --user start maumai-sv.service' to start"
      echo "[+] please run 'systemctl --user stop maumai-sv.service' to stop"
      echo "[+] please run 'sudo loginctl enable-linger maum' to enable"
      echo "[+] please run 'sudo loginctl disable-linger maum' to disable"
      echo "[+] IF ABOVE NOT WORK, USE systemctl with root account."
      echo "[+] Use unit: ${MAUM_ROOT}/etc/supervisor/export/maumai-sv-root.service"
      echo "[+]  sudo cp ${MAUM_ROOT}/etc/supervisor/export/maumai-sv-root.service to /etc/systemd/system"
      echo "[+]  sudo systemctl daemon-reload"
      echo "[+]  sudo systemctl enable maumai-sv-root.service"
      echo "[+]  sudo systemctl start maumai-sv-root.service"
      ;;
    14.04|6.*|20*) # upstart
      sudo cp ${MAUM_ROOT}/etc/supervisor/export/maumai-sv.conf /etc/init
      sudo initctl start maumai-sv
      ;;
    *)
      echo "'${OS}:${version}' not supported." ;;
  esac
}
