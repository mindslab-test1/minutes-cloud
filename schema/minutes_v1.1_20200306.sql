-- --------------------------------------------------------
-- 호스트:                          10.122.64.90
-- 서버 버전:                        5.7.28 - MySQL Community Server (GPL)
-- 서버 OS:                        Linux
-- HeidiSQL 버전:                  10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 테이블 minutes.COMMON_CODE 구조 내보내기
CREATE TABLE IF NOT EXISTS `COMMON_CODE` (
  `code_one` varchar(100) DEFAULT NULL COMMENT '1단계 코드값',
  `code_two` varchar(100) DEFAULT NULL COMMENT '2단계 코드값',
  `code_three` varchar(100) DEFAULT NULL COMMENT '3단계 코드값',
  `code_sort` int(11) DEFAULT NULL COMMENT '코드값 정렬 순서',
  `CODE_VALUE` varchar(100) NOT NULL COMMENT '코드값에 대한 값',
  `CODE_DESC` varchar(500) DEFAULT NULL COMMENT '코드 설명'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='공통코드 테이블';

-- 테이블 데이터 minutes.COMMON_CODE:~12 rows (대략적) 내보내기
DELETE FROM `COMMON_CODE`;
/*!40000 ALTER TABLE `COMMON_CODE` DISABLE KEYS */;
INSERT INTO `COMMON_CODE` (`code_one`, `code_two`, `code_three`, `code_sort`, `CODE_VALUE`, `CODE_DESC`) VALUES
	('CD001', 'A', NULL, 1, '운영관리자', '사용자 권한 코드'),
	('CD001', 'S', NULL, 2, '사용관리자', '사용자 권한 코드'),
	('CD001', 'U', NULL, 3, '사용자', '사용자 권한 코드'),
	('CD002', '1', NULL, 1, '부장', '직급 코드'),
	('CD002', '2', NULL, 2, '과장', '직급 코드'),
	('CD002', '3', NULL, 3, '대리', '직급 코드'),
	('CD002', '4', NULL, 4, '사원', '직급 코드'),
	('CD003', '1', NULL, 1, '영업', '부서 코드'),
	('CD003', '2', NULL, 2, '관리', '부서 코드'),
	('CD003', '3', NULL, 3, '기획', '부서 코드'),
	('CD003', '4', NULL, 4, 'R&D', '부서 코드'),
	('CD003', '5', NULL, 5, '자재', '부서 코드');
/*!40000 ALTER TABLE `COMMON_CODE` ENABLE KEYS */;

-- 테이블 minutes.MGMT_SERVER 구조 내보내기
CREATE TABLE IF NOT EXISTS `MGMT_SERVER` (
  `MGMT_SERVER_SER` int(10) NOT NULL AUTO_INCREMENT,
  `SERVER_NAME` varchar(200) NOT NULL,
  `URL` varchar(200) NOT NULL,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`MGMT_SERVER_SER`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MGMT_SERVER:~1 rows (대략적) 내보내기
DELETE FROM `MGMT_SERVER`;
/*!40000 ALTER TABLE `MGMT_SERVER` DISABLE KEYS */;
INSERT INTO `MGMT_SERVER` (`MGMT_SERVER_SER`, `SERVER_NAME`, `URL`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(19, 'CLOUD_MINUTES', 'ws://10.122.64.198:28081/websocket', 'admin', '2020-02-19 14:36:41', NULL, NULL);
/*!40000 ALTER TABLE `MGMT_SERVER` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_COMMON 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_COMMON` (
  `UPLOAD_DIR` varchar(200) NOT NULL,
  `RESULT_DIR` varchar(200) NOT NULL,
  `ADMIN_EMAIL` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MINUTES_COMMON:~1 rows (대략적) 내보내기
DELETE FROM `MINUTES_COMMON`;
/*!40000 ALTER TABLE `MINUTES_COMMON` DISABLE KEYS */;
INSERT INTO `MINUTES_COMMON` (`UPLOAD_DIR`, `RESULT_DIR`, `ADMIN_EMAIL`) VALUES
	('/DATA/record/upload', '/DATA/record/result', 'greshaper@mindslab.ai');
/*!40000 ALTER TABLE `MINUTES_COMMON` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_EMPLOYEE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_EMPLOYEE` (
  `minutes_employee_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT '사원 일련번호',
  `name` varchar(20) DEFAULT NULL COMMENT '이름',
  `part` varchar(20) DEFAULT NULL COMMENT '부서',
  `position` varchar(10) DEFAULT NULL COMMENT '직급',
  `useYn` char(1) DEFAULT 'Y' COMMENT '사용여부. default ''Y'':사용, ''N'':사용안함',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호. MINUTES_SITE Table의 MINUTES_SITE_SER와 동일',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_employee_ser`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트 사원 정보';

-- 테이블 데이터 minutes.MINUTES_EMPLOYEE:~14 rows (대략적) 내보내기
DELETE FROM `MINUTES_EMPLOYEE`;
/*!40000 ALTER TABLE `MINUTES_EMPLOYEE` DISABLE KEYS */;
INSERT INTO `MINUTES_EMPLOYEE` (`minutes_employee_ser`, `name`, `part`, `position`, `useYn`, `minutes_site_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, '홍길일', '1', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(2, '홍길이', '1', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(3, '홍길삼', '2', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(4, '홍길사', '2', '3', 'Y', 2, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(5, '홍길오', '2', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(6, '홍길육', '4', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(7, '홍길칠', '4', '2', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(8, '홍길팔', '4', '2', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(9, '홍길영', '1', '4', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(10, '홍길일', '1', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(11, '홍길이', '2', '3', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(12, '홍길삼', '5', '2', 'Y', 1, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(13, '홍길사', '5', '2', 'Y', 2, '123', '2019-07-17 15:55:31', NULL, '2019-12-03 14:51:10'),
	(14, '신재희', '4', '3', 'Y', 1, '123', '2019-12-03 16:08:19', '123', '2019-12-03 17:02:45');
/*!40000 ALTER TABLE `MINUTES_EMPLOYEE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_MACHINE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_MACHINE` (
  `minutes_machine_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT '회의장비 일련번호',
  `ipconvt_ipaddr` varchar(15) DEFAULT NULL COMMENT 'IP컨버터 IP주소',
  `userpc_ipaddr` varchar(15) DEFAULT NULL COMMENT '고객정보관리 PC IP주소',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호',
  `minutes_meetingroom_ser` int(11) DEFAULT NULL COMMENT '회의실 일련번호',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_machine_ser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트별 고객 납품 장비 정보';

-- 테이블 데이터 minutes.MINUTES_MACHINE:~2 rows (대략적) 내보내기
DELETE FROM `MINUTES_MACHINE`;
/*!40000 ALTER TABLE `MINUTES_MACHINE` DISABLE KEYS */;
INSERT INTO `MINUTES_MACHINE` (`minutes_machine_ser`, `ipconvt_ipaddr`, `userpc_ipaddr`, `minutes_site_ser`, `minutes_meetingroom_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, '10.122.66.73', NULL, 1, 1, NULL, '2019-08-19 10:16:53', NULL, NULL),
	(2, '10.122.66.73', NULL, 1, 2, NULL, '2019-08-19 10:25:25', NULL, NULL);
/*!40000 ALTER TABLE `MINUTES_MACHINE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_MIC 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_MIC` (
  `minutes_mic_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT '마이크 일련번호',
  `mic_name` varchar(100) DEFAULT NULL COMMENT '마이크명',
  `mic_ipaddr` varchar(15) DEFAULT NULL COMMENT '마이크 IP',
  `mic_port` varchar(15) DEFAULT NULL COMMENT '마이크 PORT',
  `mic_id` varchar(15) DEFAULT NULL COMMENT '마이크 ID',
  `used_flag` varchar(1) DEFAULT NULL COMMENT '사용 유/무',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호\n''MINUTES_SITE Table의 ''MINUTES_SITE_SER''와 동일',
  `minutes_meetingroom_ser` int(11) DEFAULT NULL COMMENT '회의실 일련번호(룸 일련번호)\n''MINUTES_MEETINGROOM Table의 ''MINUTES_MEETINGROOM_SER''와 동일',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_mic_ser`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트별 마이크장비 정보';

-- 테이블 데이터 minutes.MINUTES_MIC:~17 rows (대략적) 내보내기
DELETE FROM `MINUTES_MIC`;
/*!40000 ALTER TABLE `MINUTES_MIC` DISABLE KEYS */;
INSERT INTO `MINUTES_MIC` (`minutes_mic_ser`, `mic_name`, `mic_ipaddr`, `mic_port`, `mic_id`, `used_flag`, `minutes_site_ser`, `minutes_meetingroom_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, '1번 마이크', '10.122.66.73', '50011', '1', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:04:24'),
	(2, '2번 마이크', '10.122.66.73', '50012', '2', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(3, '3번 마이크', '10.122.66.73', '50013', '3', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(4, '4번 마이크', '10.122.66.73', '50014', '4', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(5, '5번 마이크', '10.122.66.73', '50015', '5', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(6, '6번 마이크', '10.122.66.73', '50016', '6', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(7, '7번 마이크', '10.122.66.73', '50017', '7', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(8, '8번 마이크', '10.122.66.73', '50018', '8', '1', 1, 1, '123', '2019-07-16 15:12:28', NULL, '2019-08-16 17:54:13'),
	(10, '1번 마이크', '10.122.66.73', '50011', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(11, '2번 마이크', '10.122.66.73', '50012', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(12, '3번 마이크', '10.122.66.73', '50013', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(13, '4번 마이크', '10.122.66.73', '50014', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(14, '5번 마이크', '10.122.66.73', '50015', '1', '1', 1, 2, '123', '2019-07-16 17:14:52', NULL, '2019-08-16 17:54:13'),
	(15, '1번 마이크', '10.122.66.73', '50011', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13'),
	(16, '2번 마이크', '10.122.66.73', '50012', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13'),
	(17, '3번 마이크', '10.122.66.73', '50013', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13'),
	(18, '4번 마이크', '10.122.66.73', '50014', '1', '1', 1, 3, '123', '2019-07-16 17:15:59', NULL, '2019-08-16 17:54:13');
/*!40000 ALTER TABLE `MINUTES_MIC` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_MIC_HIST 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_MIC_HIST` (
  `MINUTES_SITE_SER` int(11) DEFAULT NULL COMMENT '사이트 일련번호',
  `MINUTES_MEETINGROOM_SER` int(11) DEFAULT NULL COMMENT '회의실 일련번호(룸 일련번호)',
  `STT_META_SER` int(11) DEFAULT NULL COMMENT 'STT META 일련번호',
  `MINUTES_MIC_SER` int(11) DEFAULT NULL COMMENT '마이크 일련번호',
  `MINUTES_JOINED_NAME` varchar(30) DEFAULT NULL COMMENT '참석자 이름',
  `MIC_USE_YN` char(1) DEFAULT 'Y' COMMENT '마이크 사용여부.',
  `CREATE_USER` varchar(100) DEFAULT NULL COMMENT '생성자',
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `UPDATE_USER` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `UPDATE_TIME` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  UNIQUE KEY `MINUTES_MIC_HIST_pk` (`MINUTES_SITE_SER`,`MINUTES_MEETINGROOM_SER`,`STT_META_SER`,`MINUTES_MIC_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='마이크 설정 이력 관리';

-- 테이블 데이터 minutes.MINUTES_MIC_HIST:~79 rows (대략적) 내보내기
DELETE FROM `MINUTES_MIC_HIST`;
/*!40000 ALTER TABLE `MINUTES_MIC_HIST` DISABLE KEYS */;
INSERT INTO `MINUTES_MIC_HIST` (`MINUTES_SITE_SER`, `MINUTES_MEETINGROOM_SER`, `STT_META_SER`, `MINUTES_MIC_SER`, `MINUTES_JOINED_NAME`, `MIC_USE_YN`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(1, 1, 36, 4, '홍길삼', 'Y', '123', '2019-07-23 12:01:21', NULL, NULL),
	(1, 1, 39, 1, '홍길일', 'Y', '123', '2019-07-23 13:57:02', NULL, '2019-08-20 13:24:48'),
	(1, 1, 40, 1, '홍길사', 'Y', '123', '2019-07-23 14:03:22', NULL, NULL),
	(1, 1, 41, 1, '홍길일', 'Y', '123', '2019-07-23 14:04:25', NULL, '2019-08-20 20:55:32'),
	(1, 1, 42, 1, '홍길삼', 'Y', '123', '2019-07-23 14:06:03', NULL, '2019-08-22 14:06:54'),
	(1, 1, 43, 1, '홍길삼', 'Y', '123', '2019-07-23 14:09:16', NULL, '2019-08-26 16:37:32'),
	(1, 1, 45, 1, '홍길일', 'Y', '123', '2019-07-23 14:12:00', NULL, '2019-11-18 10:50:14'),
	(1, 1, 46, 1, '홍길일', 'Y', '123', '2019-07-23 15:17:36', NULL, '2019-11-18 10:58:26'),
	(1, 1, 46, 3, '홍길삼', 'Y', '123', '2019-07-23 15:23:46', NULL, '2019-11-18 10:58:41'),
	(1, 1, 46, 6, '홍길육', 'Y', '123', '2019-07-23 15:24:33', NULL, NULL),
	(1, 1, 23, 1, '홍길사', 'Y', '123', '2019-07-23 18:04:00', NULL, NULL),
	(1, 1, 23, 2, '홍길삼', 'Y', '123', '2019-07-23 18:04:07', NULL, NULL),
	(1, 1, 23, 3, '홍길영', 'Y', '123', '2019-07-23 18:04:15', NULL, NULL),
	(1, 1, 23, 4, '홍길오', 'Y', '123', '2019-07-23 18:04:23', NULL, NULL),
	(1, 1, 23, 5, '홍길육', 'Y', '123', '2019-07-23 18:04:31', NULL, NULL),
	(1, 1, 23, 6, '홍길일', 'Y', '123', '2019-07-23 18:04:40', NULL, NULL),
	(1, 1, 24, 1, '홍길사', 'Y', '123', '2019-07-24 11:15:28', NULL, NULL),
	(1, 1, 25, 1, '홍길오', 'Y', '123', '2019-08-12 10:46:38', NULL, NULL),
	(1, 1, 26, 1, '홍길삼', 'Y', '123', '2019-08-16 17:08:55', NULL, NULL),
	(1, 1, 26, 2, '홍길영', 'Y', '123', '2019-08-16 17:09:08', NULL, NULL),
	(1, 1, 26, 3, '홍길오', 'Y', '123', '2019-08-16 17:09:16', NULL, NULL),
	(1, 2, 27, 10, '홍길오', 'Y', '123', '2019-08-16 17:45:03', NULL, NULL),
	(1, 2, 27, 11, '홍길육', 'Y', '123', '2019-08-16 17:45:12', NULL, NULL),
	(1, 3, 28, 15, '홍길삼', 'Y', '123', '2019-08-16 17:55:20', NULL, NULL),
	(1, 3, 28, 16, '홍길영', 'Y', '123', '2019-08-16 17:55:28', NULL, '2019-08-16 17:55:45'),
	(1, 3, 28, 17, '홍길오', 'Y', '123', '2019-08-16 17:55:38', NULL, NULL),
	(1, 1, 29, 1, '홍길육', 'Y', '123', '2019-08-16 17:59:48', NULL, NULL),
	(1, 1, 29, 2, '홍길이', 'Y', '123', '2019-08-16 17:59:57', NULL, NULL),
	(1, 1, 29, 3, '홍길일', 'Y', '123', '2019-08-16 18:00:04', NULL, NULL),
	(1, 1, 30, 1, '홍길삼', 'Y', '123', '2019-08-16 18:19:24', NULL, NULL),
	(1, 1, 30, 2, '홍길영', 'Y', '123', '2019-08-16 18:19:33', NULL, NULL),
	(1, 1, 30, 3, '홍길오', 'Y', '123', '2019-08-16 18:19:40', NULL, NULL),
	(1, 1, 3, 10, '테스트1', 'Y', '123', '2019-08-19 11:06:06', NULL, NULL),
	(1, 1, 3, 11, '테스트2', 'Y', '123', '2019-08-19 11:06:29', NULL, NULL),
	(1, 1, 3, 12, '테스트3', 'Y', '123', '2019-08-19 11:06:37', NULL, NULL),
	(1, 1, 3, 13, '테스트4', 'Y', '123', '2019-08-19 11:06:51', NULL, NULL),
	(1, 1, 31, 1, '홍길삼', 'Y', '123', '2019-08-19 13:41:56', NULL, NULL),
	(1, 1, 31, 2, '홍길영', 'Y', '123', '2019-08-19 13:42:03', NULL, NULL),
	(1, 1, 31, 3, '홍길오', 'Y', '123', '2019-08-19 13:42:10', NULL, NULL),
	(1, 1, 31, 4, '홍길육', 'Y', '123', '2019-08-19 13:42:18', NULL, NULL),
	(1, 1, 32, 1, '홍길일', 'Y', '123', '2019-08-19 16:22:47', NULL, NULL),
	(1, 1, 32, 2, '홍길이', 'Y', '123', '2019-08-19 16:22:55', NULL, NULL),
	(1, 1, 32, 3, '홍길삼', 'Y', '123', '2019-08-19 16:23:06', NULL, NULL),
	(1, 1, 33, 4, '홍길오', 'Y', '123', '2019-08-19 16:23:21', NULL, NULL),
	(1, 1, 34, 1, '홍길일', 'Y', '123', '2019-08-20 09:57:54', NULL, NULL),
	(1, 1, 34, 2, '홍길이', 'Y', '123', '2019-08-20 09:58:02', NULL, NULL),
	(1, 1, 34, 3, '홍길삼', 'Y', '123', '2019-08-20 09:58:09', NULL, NULL),
	(1, 1, 34, 4, '홍길오', 'Y', '123', '2019-08-20 09:58:16', NULL, NULL),
	(1, 1, 35, 1, '홍길일', 'Y', '123', '2019-08-20 11:34:22', NULL, NULL),
	(1, 1, 35, 2, '홍길이', 'Y', '123', '2019-08-20 11:34:30', NULL, NULL),
	(1, 1, 35, 3, '홍길삼', 'Y', '123', '2019-08-20 11:34:37', NULL, NULL),
	(1, 1, 35, 4, '홍길오', 'Y', '123', '2019-08-20 11:34:44', NULL, NULL),
	(1, 1, 37, 1, '홍길일', 'Y', '123', '2019-08-20 11:50:57', NULL, NULL),
	(1, 1, 37, 2, '홍길이', 'Y', '123', '2019-08-20 11:51:04', NULL, NULL),
	(1, 1, 37, 3, '홍길삼', 'Y', '123', '2019-08-20 11:51:12', NULL, NULL),
	(1, 1, 37, 4, '홍길오', 'Y', '123', '2019-08-20 11:51:19', NULL, NULL),
	(1, 1, 38, 1, '홍길일', 'Y', '123', '2019-08-20 13:16:30', NULL, NULL),
	(1, 1, 38, 2, '홍길이', 'Y', '123', '2019-08-20 13:16:38', NULL, NULL),
	(1, 1, 38, 3, '홍길삼', 'Y', '123', '2019-08-20 13:16:46', NULL, NULL),
	(1, 1, 38, 4, '홍길오', 'Y', '123', '2019-08-20 13:16:53', NULL, NULL),
	(1, 1, 39, 2, '홍길이', 'Y', '123', '2019-08-20 13:24:54', NULL, NULL),
	(1, 1, 39, 3, '홍길삼', 'Y', '123', '2019-08-20 13:25:01', NULL, NULL),
	(1, 1, 39, 4, '홍길오', 'Y', '123', '2019-08-20 13:25:07', NULL, NULL),
	(1, 3, 40, 15, '홍길일', 'Y', '123', '2019-08-20 20:03:13', NULL, NULL),
	(1, 3, 40, 16, '홍길이', 'Y', '123', '2019-08-20 20:03:20', NULL, NULL),
	(1, 3, 40, 17, '홍길삼', 'Y', '123', '2019-08-20 20:03:30', NULL, NULL),
	(1, 3, 40, 18, '홍길오', 'Y', '123', '2019-08-20 20:03:38', NULL, NULL),
	(1, 1, 41, 2, '홍길이', 'Y', '123', '2019-08-20 20:55:40', NULL, NULL),
	(1, 1, 41, 3, '홍길삼', 'Y', '123', '2019-08-20 20:55:47', NULL, NULL),
	(1, 1, 41, 4, '홍길오', 'Y', '123', '2019-08-20 20:55:54', NULL, NULL),
	(1, 1, 45, 2, '홍길이', 'Y', '123', '2019-11-18 10:50:21', NULL, NULL),
	(1, 1, 45, 3, '홍길삼', 'Y', '123', '2019-11-18 10:50:33', NULL, NULL),
	(1, 1, 46, 2, '홍길이', 'Y', '123', '2019-11-18 10:58:33', NULL, NULL),
	(1, 1, 47, 1, '홍길일', 'Y', '123', '2019-11-18 11:11:27', NULL, NULL),
	(1, 1, 47, 2, '홍길이', 'Y', '123', '2019-11-18 11:11:35', NULL, NULL),
	(1, 1, 47, 3, '홍길삼', 'Y', '123', '2019-11-18 11:11:42', NULL, NULL),
	(1, 1, 48, 1, '홍길일', 'Y', '123', '2019-11-18 11:22:48', NULL, NULL),
	(1, 1, 48, 2, '홍길이', 'Y', '123', '2019-11-18 11:22:55', NULL, NULL),
	(1, 1, 48, 3, '홍길삼', 'Y', '123', '2019-11-18 11:23:01', NULL, NULL);
/*!40000 ALTER TABLE `MINUTES_MIC_HIST` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_REPLACE_WORD 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_REPLACE_WORD` (
  `minutes_replace_word_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'STT치환사전 일련번호',
  `minutes_type` varchar(100) DEFAULT 'COM' COMMENT '회의 타입 코드',
  `before_word` varchar(100) DEFAULT NULL COMMENT '치환전단어',
  `after_word` varchar(100) DEFAULT NULL COMMENT '치환후단어',
  `minutes_site_ser` int(11) DEFAULT NULL COMMENT '사이트 일련번호',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일시',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`minutes_replace_word_ser`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트별 치환사전';

-- 테이블 데이터 minutes.MINUTES_REPLACE_WORD:~23 rows (대략적) 내보내기
DELETE FROM `MINUTES_REPLACE_WORD`;
/*!40000 ALTER TABLE `MINUTES_REPLACE_WORD` DISABLE KEYS */;
INSERT INTO `MINUTES_REPLACE_WORD` (`minutes_replace_word_ser`, `minutes_type`, `before_word`, `after_word`, `minutes_site_ser`, `create_user`, `create_time`, `update_user`, `update_time`) VALUES
	(1, 'COM', '에이_1', 'A1', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(2, 'COM', '에이_2', 'A2', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(3, 'COM', '에이_3', 'A3', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(4, 'COM', '에이_4', 'A4', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(5, 'COM', '에이_5', 'A5', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(6, 'COM', '에이_6', 'A6', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(7, 'COM', '에이_7', 'A7', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(8, 'COM', '에이_8', 'A8', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(9, 'COM', '에이_9', 'A9', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(10, 'COM', '에이_10', 'A10', NULL, NULL, '2019-07-30 15:19:53', NULL, '2019-07-30 15:21:43'),
	(11, 'COM', '비_1', 'B1', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(12, 'COM', '비_2', 'B2', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(13, 'COM', '비_3', 'B3', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(14, 'COM', '비_4', 'B4', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(15, 'COM', '비_5', 'B5', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(16, 'COM', '비_6', 'B6', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(17, 'COM', '비_7', 'B7', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(18, 'COM', '비_8', 'B8', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(19, 'COM', '비_9', 'B9', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(20, 'COM', '비_10', 'B10', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(21, 'COM', '시_1', 'C1', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(22, 'COM', '시_2', 'C2', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL),
	(23, 'COM', '시_3', 'C3', NULL, NULL, '2019-07-30 15:19:53', NULL, NULL);
/*!40000 ALTER TABLE `MINUTES_REPLACE_WORD` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_ROLE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_ROLE` (
  `ROLE_SER` int(10) NOT NULL AUTO_INCREMENT,
  `ROLE_TYPE` varchar(10) NOT NULL,
  `ROLE_DESC` varchar(255) NOT NULL,
  `ROLE_ORDER` int(10) NOT NULL,
  `USE_YN` varchar(1) NOT NULL,
  `CREATE_USER` varchar(100) DEFAULT NULL,
  `CREATE_TIME` datetime DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ROLE_SER`),
  UNIQUE KEY `MINUTES_ROLE` (`ROLE_TYPE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MINUTES_ROLE:~2 rows (대략적) 내보내기
DELETE FROM `MINUTES_ROLE`;
/*!40000 ALTER TABLE `MINUTES_ROLE` DISABLE KEYS */;
INSERT INTO `MINUTES_ROLE` (`ROLE_SER`, `ROLE_TYPE`, `ROLE_DESC`, `ROLE_ORDER`, `USE_YN`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(1, 'A', '운영관리자', 1, 'N', NULL, '2019-12-19 15:50:48', NULL, '2019-12-19 15:50:48'),
	(2, 'S', '사용관리자', 2, 'Y', NULL, '2019-12-19 15:50:48', NULL, '2019-12-19 15:50:48'),
	(3, 'U', '사용자', 3, 'Y', NULL, '2019-12-19 15:50:48', NULL, '2019-12-19 15:50:48');
/*!40000 ALTER TABLE `MINUTES_ROLE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_SITE 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_SITE` (
  `minutes_site_ser` int(11) NOT NULL AUTO_INCREMENT COMMENT 'SITE 일련번호',
  `stt_model_ser` int(10) NOT NULL,
  `create_user` varchar(100) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  `site_name` varchar(100) DEFAULT NULL COMMENT '사이트 명',
  `max_meeting_room_cnt` int(11) DEFAULT NULL COMMENT '사이트별 미팅룸 최대 개수',
  `max_mic_cnt` int(11) DEFAULT NULL COMMENT '사이트별 마이크 최대 개수',
  PRIMARY KEY (`minutes_site_ser`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='회의시스템 사이트 관리 정보';

-- 테이블 데이터 minutes.MINUTES_SITE:~5 rows (대략적) 내보내기
DELETE FROM `MINUTES_SITE`;
/*!40000 ALTER TABLE `MINUTES_SITE` DISABLE KEYS */;
INSERT INTO `MINUTES_SITE` (`minutes_site_ser`, `stt_model_ser`, `create_user`, `create_time`, `update_user`, `update_time`, `site_name`, `max_meeting_room_cnt`, `max_mic_cnt`) VALUES
	(0, 0, 'admin', '2019-12-31 14:16:34', 'admin', '2019-12-31 14:16:54', 'all', 0, 0),
	(6, 1, 'admin', '2019-12-31 13:21:59', NULL, '2020-02-07 12:58:17', 'MINDSLAB', 10, 10),
	(8, 1, 'admin', '2020-01-03 11:07:39', NULL, '2020-02-07 12:58:20', 'KHY_TEST1', 10, 10),
	(9, 2, 'admin', '2020-01-03 11:08:01', NULL, '2020-02-07 12:58:22', 'KHY_TEST2', 10, 10),
	(18, 1, 'admin', '2020-01-07 12:27:54', NULL, '2020-02-07 12:58:24', 'MINDSLAB_FREE', 1, 1);
/*!40000 ALTER TABLE `MINUTES_SITE` ENABLE KEYS */;

-- 테이블 minutes.MINUTES_USER 구조 내보내기
CREATE TABLE IF NOT EXISTS `MINUTES_USER` (
  `MINUTES_USER_SER` int(10) NOT NULL AUTO_INCREMENT,
  `MINUTES_SITE_SER` int(10) NOT NULL,
  `USER_ID` varchar(100) NOT NULL,
  `USER_PW` varchar(100) NOT NULL,
  `USER_NAME` varchar(100) NOT NULL,
  `CELLPHONE_NUM` varchar(100) NOT NULL,
  `USER_TYPE` varchar(1) NOT NULL DEFAULT 'U',
  `USER_USE_YN` varchar(1) NOT NULL DEFAULT 'Y',
  `USER_LAST_LOGIN_DT` datetime NOT NULL,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`MINUTES_USER_SER`),
  UNIQUE KEY `MINUTES_USER_UNIQUE` (`MINUTES_USER_SER`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.MINUTES_USER:~171 rows (대략적) 내보내기
DELETE FROM `MINUTES_USER`;
/*!40000 ALTER TABLE `MINUTES_USER` DISABLE KEYS */;
INSERT INTO `MINUTES_USER` (`MINUTES_USER_SER`, `MINUTES_SITE_SER`, `USER_ID`, `USER_PW`, `USER_NAME`, `CELLPHONE_NUM`, `USER_TYPE`, `USER_USE_YN`, `USER_LAST_LOGIN_DT`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`) VALUES
	(4, 0, 'admin', '$2a$10$cVZdCBGLRf6G2t8zNc.hZ.Lvo3HVA/DTKtv4LRcQF74VYqDHKD/Qy', '관리자', '010-1111-2222', '1', 'Y', '2019-12-23 15:34:17', 'admin', '2019-12-23 15:34:17', 'admin', '2019-12-23 21:46:58'),
	(12, 18, 'minds', '$2a$10$PYXAd4cwsKFDycHobZd5ietwQrkztU0HfOVdrQMkgoimkv5O5KmY6', 'MINDSLAB', '010-9485-2222', '2', 'Y', '2019-12-31 13:23:42', 'minds', '2019-12-31 13:23:42', 'minds', '2020-02-03 23:05:21'),
	(14, 8, 'khy', '$2a$10$8IVgn51ar1zg7oq9qS7Rg.YItBSNdpAtqOY4ZzdwR3RFxFb9127vm', '김희연', '011-1111-1111', '3', 'Y', '2020-01-03 11:09:33', 'khy', '2020-01-03 11:09:33', 'khy', '2020-01-21 14:59:37'),
	(16, 6, 'wontop', '$2a$10$hNg6cvQt.oylXdstbx2EyeevalXyo5YkWHdPBoy/usJeYFoFbr4dm', '원정상', '010-9485-2222', '3', 'Y', '2020-01-06 17:53:57', 'wontop', '2020-01-06 17:53:57', 'wontop', '2020-01-21 14:59:20'),
	(18, 9, 'min', '$2a$10$/g6MaEtsSo41J59zt25ZZedYJ.iVfulJ0TdcWuj5aBkE/FsFYQY8y', '남승민', '010-1111-2222', '2', 'Y', '2020-01-07 17:23:10', 'min', '2020-01-07 17:23:10', 'min', '2020-01-07 17:23:10'),
	(19, 18, 'fortis342@gmail.com', '$2a$10$fR.B88gIWrjhe.g7rW9vtu/EDS1Dc/x6lFM1DokDW0hsGHS9UWLt2', '김성훈', '010-2634-4300', '3', 'Y', '2020-01-16 10:16:43', 'fortis342@gmail.com', '2020-01-16 10:16:43', 'fortis342@gmail.com', '2020-01-16 11:44:30'),
	(20, 18, 'papermint@gmail.com', '$2a$10$m57kbRfYEVlpjk3.9IqNVemQEKeIYkAMoZXtbTd.JcTbzsGXcSfdW', '석재니', '010-4424-4422', '3', 'Y', '2020-01-21 14:56:36', 'papermint@gmail.com', '2020-01-21 14:56:36', 'papermint@gmail.com', '2020-01-21 14:56:36'),
	(22, 18, 'pearlismylove@gmail.com', '$2a$10$3Z5Cr2M0Rgt6X.VAIiFcFOu4og7sAioOWTY80T/NJuvFVBLm0/4.2', 'pearlismylove@gmail.com', '', '3', 'Y', '2020-01-29 16:01:25', 'pearlismylove@gmail.com', '2020-01-29 16:01:25', 'pearlismylove@gmail.com', '2020-01-29 16:01:25'),
	(23, 18, 'wontop@mindslab.ai', '$2a$10$1u7wYMTTOKo2wj5oetaGgOuirx5civ9I0ntW.qm5mGb/FgOX0ZBDi', 'wontop@mindslab.ai', '', '3', 'Y', '2020-01-29 16:06:04', 'wontop@mindslab.ai', '2020-01-29 16:06:04', 'wontop@mindslab.ai', '2020-01-29 16:06:04'),
	(25, 18, 'greshaper@mindslab.ai', '$2a$10$F0NgFlByV3UZQcenvS/eIuihrZafFgejNywMHsWhaVKwnyRXoU/Am', 'greshaper@mindslab.ai', '', '3', 'Y', '2020-01-29 16:11:06', 'greshaper@mindslab.ai', '2020-01-29 16:11:06', 'greshaper@mindslab.ai', '2020-01-29 16:11:06'),
	(26, 18, 'belsnake@mindslab.ai', '$2a$10$kNaxqoc9Zqhym1U0wwHqKOhLbTM3LanRG.RPDPUde/9SFTTFHBn9i', 'belsnake@mindslab.ai', '', '3', 'Y', '2020-01-29 16:35:35', 'belsnake@mindslab.ai', '2020-01-29 16:35:35', 'belsnake@mindslab.ai', '2020-01-29 16:35:35'),
	(27, 18, 'sj.hwang@mindslab.ai', '$2a$10$/ElYe/6YtsuPozaSKgIF9OruX4ZA0mH.qeqYzlzJDXfXWi1bkFFZO', 'sj.hwang@mindslab.ai', '', '3', 'Y', '2020-01-30 10:18:46', 'sj.hwang@mindslab.ai', '2020-01-30 10:18:46', 'sj.hwang@mindslab.ai', '2020-01-30 10:18:46'),
	(28, 18, 'bommie.lee@mindslab.ai', '$2a$10$sDwj7gAoO53VOkmaLIydeuyzp9Lkn4s0HTrQC.gDofz2qlgiEnweW', 'bommie.lee@mindslab.ai', '', '3', 'Y', '2020-01-30 14:40:11', 'bommie.lee@mindslab.ai', '2020-01-30 14:40:11', 'bommie.lee@mindslab.ai', '2020-01-30 14:40:11'),
	(29, 18, 'kihun.kim21@gmail.com', '$2a$10$oCOe58rE1F.HNWDorsEhXOM0bizc60yGu3QVtnsjOpWWwwybhX0oS', 'kihun.kim21@gmail.com', '', '3', 'Y', '2020-01-30 15:43:52', 'kihun.kim21@gmail.com', '2020-01-30 15:43:52', 'kihun.kim21@gmail.com', '2020-01-30 15:43:52'),
	(30, 18, 'maro3244@mindslab.ai', '$2a$10$h2foRktt5r3hHvXcNAF.yufSidm3dwf3IrmuzdPijX8GlWDkTBKLC', 'maro3244@mindslab.ai', '', '3', 'Y', '2020-01-30 16:18:13', 'maro3244@mindslab.ai', '2020-01-30 16:18:13', 'maro3244@mindslab.ai', '2020-01-30 16:18:13'),
	(31, 18, 'blissmimi@mindslab.ai', '$2a$10$NGj.7oz.2.luu.3k.wAoHOfkeA4s6pO0guby0THpI/9Eg0FXf0nOi', 'blissmimi@mindslab.ai', '', '3', 'Y', '2020-01-31 09:47:29', 'blissmimi@mindslab.ai', '2020-01-31 09:47:29', 'blissmimi@mindslab.ai', '2020-01-31 09:47:29'),
	(32, 18, 'ieehyunwoo@mindslab.ai', '$2a$10$VwyLwXq/7Nuj.tZ1fAJ0n.I72lUrUYIxQpijJ5K3pymWN2wWXCU1.', 'ieehyunwoo@mindslab.ai', '', '3', 'Y', '2020-01-31 11:07:30', 'ieehyunwoo@mindslab.ai', '2020-01-31 11:07:30', 'ieehyunwoo@mindslab.ai', '2020-01-31 11:07:30'),
	(33, 18, 'seongmo.yim@mindslab.ai', '$2a$10$suKNbqoXPN2KiPU6APLev.dMbQCMoeqVOkkd99XKZgh8JsklSRgCO', 'seongmo.yim@mindslab.ai', '', '3', 'Y', '2020-01-31 11:22:14', 'seongmo.yim@mindslab.ai', '2020-01-31 11:22:14', 'seongmo.yim@mindslab.ai', '2020-01-31 11:22:14'),
	(34, 18, 'kichul.park@bespinglobal.com', '$2a$10$FtkizF/qdYLa0DdrerWNtu52F9tVuS09Z12jDqYJAyi9oJUJGJPF.', 'kichul.park@bespinglobal.com', '', '3', 'Y', '2020-01-31 11:29:19', 'kichul.park@bespinglobal.com', '2020-01-31 11:29:19', 'kichul.park@bespinglobal.com', '2020-01-31 11:29:19'),
	(35, 18, 'ray@mindslab.ai', '$2a$10$J2zk8rVEhX7CmNYVqrKDi.mx8VInge36aIbAG3BVbUdRjDwJ94f0C', 'ray@mindslab.ai', '', '3', 'Y', '2020-01-31 13:33:53', 'ray@mindslab.ai', '2020-01-31 13:33:53', 'ray@mindslab.ai', '2020-01-31 13:33:53'),
	(36, 18, 'focskh@mindslab.ai', '$2a$10$RAgMGxRbmLNDRcKXusN.GeYH8q5Pv4k.5uwv3cg/yiK6EJKW/116O', 'focskh@mindslab.ai', '', '3', 'Y', '2020-01-31 14:43:50', 'focskh@mindslab.ai', '2020-01-31 14:43:50', 'focskh@mindslab.ai', '2020-01-31 14:43:50'),
	(37, 18, 'oym3329@mindslab.ai', '$2a$10$wGv/1qT2liulfXrVi3CYAODQQVwYPyIq9Gv02Hqp5MjUr0/Ed20Fy', 'oym3329@mindslab.ai', '', '3', 'Y', '2020-01-31 17:02:02', 'oym3329@mindslab.ai', '2020-01-31 17:02:02', 'oym3329@mindslab.ai', '2020-01-31 17:02:02'),
	(38, 18, 'soostring@soundandcreative.com', '$2a$10$tyGjQZ2PEIAGC9vehURCOeDIyzRUN76RHlI0t062aHL1dB/Zuuj9y', 'soostring@soundandcreative.com', '', '3', 'Y', '2020-02-01 01:29:47', 'soostring@soundandcreative.com', '2020-02-01 01:29:47', 'soostring@soundandcreative.com', '2020-02-01 01:29:47'),
	(39, 18, 'djmedia73@gmail.com', '$2a$10$OpLP2sUTxFWnuz/.zjTbkuWRreG/VDG3c2hCP4Au2Znf7/DDVTgji', 'djmedia73@gmail.com', '', '3', 'Y', '2020-02-02 23:10:20', 'djmedia73@gmail.com', '2020-02-02 23:10:20', 'djmedia73@gmail.com', '2020-02-02 23:10:20'),
	(40, 18, 'royce.kim@mindslab.ai', '$2a$10$Vc08spvXTFfAszfWFAQH/OSz8iBw0taNgpWo4L4oMMgx3cFOL/veW', 'royce.kim@mindslab.ai', '', '3', 'Y', '2020-02-03 07:23:23', 'royce.kim@mindslab.ai', '2020-02-03 07:23:23', 'royce.kim@mindslab.ai', '2020-02-03 07:23:23'),
	(41, 18, 'dw.lee@processware.co.kr', '$2a$10$xTOxkoxkMgsc3ZrL9FUJbOa9vXlm19NfpHiH.H6W5lx1HjNcHAn4e', 'dw.lee@processware.co.kr', '', '3', 'Y', '2020-02-03 09:19:58', 'dw.lee@processware.co.kr', '2020-02-03 09:19:58', 'dw.lee@processware.co.kr', '2020-02-03 09:19:58'),
	(42, 18, 'biz.processware@gmail.com', '$2a$10$A5tH8w2zM19kArC.ttyUiOl6fJAY9fKCQjAUYu1CsQDRekb1Y.myC', 'biz.processware@gmail.com', '', '3', 'Y', '2020-02-03 09:30:21', 'biz.processware@gmail.com', '2020-02-03 09:30:21', 'biz.processware@gmail.com', '2020-02-03 09:30:21'),
	(43, 18, 'koogeehun@gmail.com', '$2a$10$M3qnhS0GsbjXqGGXji2pmeS8omC0MNkAy5O/fHaU.I4fl7fRP/I1G', 'koogeehun@gmail.com', '', '3', 'Y', '2020-02-03 12:42:49', 'koogeehun@gmail.com', '2020-02-03 12:42:49', 'koogeehun@gmail.com', '2020-02-03 12:42:49'),
	(44, 18, 'fromalice965@gmail.com', '$2a$10$/sAnKpTCTE.wYLYE7EyR1utrn1JwYt9sqLQ2SC1ylHz4KidhHpcwS', 'fromalice965@gmail.com', '', '3', 'Y', '2020-02-04 10:08:10', 'fromalice965@gmail.com', '2020-02-04 10:08:10', 'fromalice965@gmail.com', '2020-02-04 10:08:10'),
	(45, 18, 'racoonyy@mindslab.ai', '$2a$10$qISgbEy8z1Nw9Qoh2GPAWumnTS6GF4QlpGRYi/FYcXw2Pnkhv6nfC', 'racoonyy@mindslab.ai', '', '3', 'Y', '2020-02-04 13:28:34', 'racoonyy@mindslab.ai', '2020-02-04 13:28:34', 'racoonyy@mindslab.ai', '2020-02-04 13:28:34'),
	(46, 18, 'syyoon@mindslab.ai', '$2a$10$WpFc7TU1VrevvL.K/8P6lufUJA54PNqd82slXTCVEP3rtRWrrStZi', 'syyoon@mindslab.ai', '', '3', 'Y', '2020-02-04 14:00:41', 'syyoon@mindslab.ai', '2020-02-04 14:00:41', 'syyoon@mindslab.ai', '2020-02-04 14:00:41'),
	(47, 18, 'flight3051@gmail.com', '$2a$10$nMipa.dVg.IUlcpeWhpDz.IxLQQR9/iG6SQHu2mql1qIyhqoQI2Ay', 'flight3051@gmail.com', '', '3', 'Y', '2020-02-04 16:44:18', 'flight3051@gmail.com', '2020-02-04 16:44:18', 'flight3051@gmail.com', '2020-02-04 16:44:18'),
	(48, 18, 'aimee@mindslab.ai', '$2a$10$oMc9pT9X7Nz.DSp3iHVe2umaMOltXPoo75hgiMZ6sLBdj7XePVHga', 'aimee@mindslab.ai', '', '3', 'Y', '2020-02-05 09:31:15', 'aimee@mindslab.ai', '2020-02-05 09:31:15', 'aimee@mindslab.ai', '2020-02-05 09:31:15'),
	(49, 18, 'lina@mindslab.ai', '$2a$10$BYr.BmWo17GTqWBsNTWC4.x352OUWKQEbz7gxdaomtgxjZVkN5jXm', 'lina@mindslab.ai', '', '3', 'Y', '2020-02-05 16:28:37', 'lina@mindslab.ai', '2020-02-05 16:28:37', 'lina@mindslab.ai', '2020-02-05 16:28:37'),
	(50, 18, 'nadomilka@gmail.com', '$2a$10$dibtxtGXsotOAZ40T9RQz.TDPcSi7J1fTxY9V57wXdesxNb26Kq3S', 'nadomilka@gmail.com', '', '3', 'Y', '2020-02-05 20:57:42', 'nadomilka@gmail.com', '2020-02-05 20:57:42', 'nadomilka@gmail.com', '2020-02-05 20:57:42'),
	(51, 18, 'keytool9@gmail.com', '$2a$10$8EdbB/n5iNjodB5zxtauI..o1bRx9Fh7Px9mjoLzkvCR096eqx/PG', 'keytool9@gmail.com', '', '3', 'Y', '2020-02-06 00:36:47', 'keytool9@gmail.com', '2020-02-06 00:36:47', 'keytool9@gmail.com', '2020-02-06 00:36:47'),
	(52, 18, 'woosanglee2019@gmail.com', '$2a$10$9dAna0UoiphwypDOCi698OYuGSP/Vn6bjy/VVQoByLWfekxViSFTK', 'woosanglee2019@gmail.com', '', '3', 'Y', '2020-02-06 10:48:15', 'woosanglee2019@gmail.com', '2020-02-06 10:48:15', 'woosanglee2019@gmail.com', '2020-02-06 10:48:15'),
	(53, 18, 'parkjinoa@gmail.com', '$2a$10$DdUtqBiU0.OiGXmYdl.yB.9Uj2xKMr5HdXOJCyUdsyDRgU9MsoTdK', 'parkjinoa@gmail.com', '', '3', 'Y', '2020-02-07 02:20:51', 'parkjinoa@gmail.com', '2020-02-07 02:20:51', 'parkjinoa@gmail.com', '2020-02-07 02:20:51'),
	(54, 18, 'daehyun93@mindslab.ai', '$2a$10$9odCrGWpQyBP2/DhBo0AI.DxRe/MogdT3yt/fubLNWa6VVuX1dxvy', 'daehyun93@mindslab.ai', '', '3', 'Y', '2020-02-07 10:25:24', 'daehyun93@mindslab.ai', '2020-02-07 10:25:24', 'daehyun93@mindslab.ai', '2020-02-07 10:25:24'),
	(55, 18, 'boramm@mindslab.ai', '$2a$10$bJyVA1FXVej42ArRlEn8zOP6EgBHIaBnjMCkzlI5c4G8VPbn4hQ9m', 'boramm@mindslab.ai', '', '3', 'Y', '2020-02-07 11:14:57', 'boramm@mindslab.ai', '2020-02-07 11:14:57', 'boramm@mindslab.ai', '2020-02-07 11:14:57'),
	(56, 18, 'bella@aimmo.co.kr', '$2a$10$HUpglaz51na4J03/0wIhu.B7UWcepe8GsXbKzV/mR49nvG3TriNrW', 'bella@aimmo.co.kr', '', '3', 'Y', '2020-02-07 14:43:23', 'bella@aimmo.co.kr', '2020-02-07 14:43:23', 'bella@aimmo.co.kr', '2020-02-07 14:43:23'),
	(57, 18, 'alina@aimmo.co.kr', '$2a$10$5aMNvCJc/iLkTMEjIl2C7.SVxoxLj4cDBvu36iKE2wa8qxHk5Jsq2', 'alina@aimmo.co.kr', '', '3', 'Y', '2020-02-07 15:42:17', 'alina@aimmo.co.kr', '2020-02-07 15:42:17', 'alina@aimmo.co.kr', '2020-02-07 15:42:17'),
	(58, 18, 'jaylee.2984@gmail.com', '$2a$10$ayynLbgcvlKagNbKzKMcR.RLFEtlbDb93/CT7eYT24enmAnwurlC.', 'jaylee.2984@gmail.com', '', '3', 'Y', '2020-02-07 16:38:13', 'jaylee.2984@gmail.com', '2020-02-07 16:38:13', 'jaylee.2984@gmail.com', '2020-02-07 16:38:13'),
	(59, 18, 'chungkyuhong@gmail.com', '$2a$10$hYU.LjUZg8u4KGDCTj0JF.W2bpz4Oea6jtHEYDYL7rQYu0IC0AvNO', 'chungkyuhong@gmail.com', '', '3', 'Y', '2020-02-07 17:02:53', 'chungkyuhong@gmail.com', '2020-02-07 17:02:53', 'chungkyuhong@gmail.com', '2020-02-07 17:02:53'),
	(60, 18, 'n3862001@gmail.com', '$2a$10$WO2S1JKV0sZ1Fk3Su39PeOGlHlIH5V/rzSsowxDaDtgRe63kstxsu', 'n3862001@gmail.com', '', '3', 'Y', '2020-02-09 08:00:51', 'n3862001@gmail.com', '2020-02-09 08:00:51', 'n3862001@gmail.com', '2020-02-09 08:00:51'),
	(61, 18, 'woivescho8@gmail.com', '$2a$10$EUQNKF5aDNwnF8kHUs4btec0RvoYeE9NzZDYfHOkj2vVF0i45iCBG', 'woivescho8@gmail.com', '', '3', 'Y', '2020-02-09 18:25:57', 'woivescho8@gmail.com', '2020-02-09 18:25:57', 'woivescho8@gmail.com', '2020-02-09 18:25:57'),
	(62, 18, 'shakim1991@gmail.com', '$2a$10$9JgFFXSnQESmeZ.s4fDQN.grkKUI/yZwB.CQFuxBhE7GaEhXzTKei', 'shakim1991@gmail.com', '', '3', 'Y', '2020-02-10 09:10:19', 'shakim1991@gmail.com', '2020-02-10 09:10:19', 'shakim1991@gmail.com', '2020-02-10 09:10:19'),
	(63, 18, 's.khang@td7studios.com', '$2a$10$8iKjXNIM6dtDrwMgidBTPODAtZ0BmTqjK4iYjo14SlB6HOvqYUyBm', 's.khang@td7studios.com', '', '3', 'Y', '2020-02-10 10:12:48', 's.khang@td7studios.com', '2020-02-10 10:12:48', 's.khang@td7studios.com', '2020-02-10 10:12:48'),
	(64, 18, 'yenayoo@mindslab.ai', '$2a$10$VF3IiM3xn1StKj.RvwEVJe9.kcX.Po1on4WUzuqSnhpbI.1onRZaq', 'yenayoo@mindslab.ai', '', '3', 'Y', '2020-02-10 11:58:35', 'yenayoo@mindslab.ai', '2020-02-10 11:58:35', 'yenayoo@mindslab.ai', '2020-02-10 11:58:35'),
	(65, 18, 'smson81@gmail.com', '$2a$10$5GiX0Lsey7XOp0FuG0.OCug5oluZCeDzashf31kJ7Qgo1nQ4TQByC', 'smson81@gmail.com', '', '3', 'Y', '2020-02-10 15:04:51', 'smson81@gmail.com', '2020-02-10 15:04:51', 'smson81@gmail.com', '2020-02-10 15:04:51'),
	(66, 18, 'earl@mindslab.ai', '$2a$10$YKbpPgaeLGEmhuVQdkz9M.mTQzRimzdQ30N1CUdXNRIEceWoU2sTy', 'earl@mindslab.ai', '', '3', 'Y', '2020-02-10 17:33:18', 'earl@mindslab.ai', '2020-02-10 17:33:18', 'earl@mindslab.ai', '2020-02-10 17:33:18'),
	(67, 18, 'hy@mindslab.ai', '$2a$10$gggtA.ryAHctnh9OH49KqOqV7eZELlNr9SkfZY6McCkSPg2PjpURS', 'hy@mindslab.ai', '', '3', 'Y', '2020-02-10 18:34:54', 'hy@mindslab.ai', '2020-02-10 18:34:54', 'hy@mindslab.ai', '2020-02-10 18:34:54'),
	(68, 6, 'greenmango', '$2a$10$8FeapkU/UpOUy/DG5y/N9etb4sFOVLLBnQhDDcneGi.TW289hqxBi', 'greenmango', '010-0000-1111', '1', 'Y', '2020-02-11 10:38:25', 'greenmango', '2020-02-11 10:38:25', 'greenmango', '2020-02-11 10:38:25'),
	(69, 18, 'cinemagit@gmail.com', '$2a$10$oZuPbe3oJr2xk8bmtNhr8OhmqgZaNN5quZ7CRPaq/v1nI9Gk0D40S', 'cinemagit@gmail.com', '', '3', 'Y', '2020-02-11 16:01:09', 'cinemagit@gmail.com', '2020-02-11 16:01:09', 'cinemagit@gmail.com', '2020-02-11 16:01:09'),
	(70, 18, 'jwshin12@gmail.com', '$2a$10$PK4Ks6yDeEzi.XsHspfGSu10LVFwmQOIee34ep7mNn1ikCxQFHr4y', 'jwshin12@gmail.com', '', '3', 'Y', '2020-02-11 18:10:17', 'jwshin12@gmail.com', '2020-02-11 18:10:17', 'jwshin12@gmail.com', '2020-02-11 18:10:17'),
	(71, 18, 'mooni@mindslab.ai', '$2a$10$oih6P4KZpD/7nRH1MQK4juiq7pBMD2AWhEEL5sQLeBhVGuZJaNYfO', 'mooni@mindslab.ai', '', '3', 'Y', '2020-02-11 20:29:48', 'mooni@mindslab.ai', '2020-02-11 20:29:48', 'mooni@mindslab.ai', '2020-02-11 20:29:48'),
	(72, 18, 'aabis4444@gmail.com', '$2a$10$Dz88crQ5IGNZcUOUvgaIPezbwoBCYCSuhj4p8Czxh2ks.FfdsRg/G', 'aabis4444@gmail.com', '', '3', 'Y', '2020-02-12 10:19:45', 'aabis4444@gmail.com', '2020-02-12 10:19:45', 'aabis4444@gmail.com', '2020-02-12 10:19:45'),
	(73, 18, 'hdkim0689@gmail.com', '$2a$10$O9fCXs8K.OKSmHzI0RTCsuXwHMpe/DkrP.FKqjmFAPgT2iyJz53Qi', 'hdkim0689@gmail.com', '', '3', 'Y', '2020-02-12 10:45:04', 'hdkim0689@gmail.com', '2020-02-12 10:45:04', 'hdkim0689@gmail.com', '2020-02-12 10:45:04'),
	(74, 18, 'allbigdat@gmail.com', '$2a$10$OwZb8AInhyEbrJC3s9DlGugnh7XF6XUctYeqTbr14buw/hPK8t4vS', 'allbigdat@gmail.com', '', '3', 'Y', '2020-02-12 14:05:12', 'allbigdat@gmail.com', '2020-02-12 14:05:12', 'allbigdat@gmail.com', '2020-02-12 14:05:12'),
	(75, 18, 'cjoowon@gmail.com', '$2a$10$SYvNVvo1ojdIaQYRqdOp1uehfe7m/sGFWu7.6.U5Iq5KmwthQebQe', 'cjoowon@gmail.com', '', '3', 'Y', '2020-02-12 15:18:07', 'cjoowon@gmail.com', '2020-02-12 15:18:07', 'cjoowon@gmail.com', '2020-02-12 15:18:07'),
	(76, 18, 'sjlim@mindslab.ai', '$2a$10$EaZ0HKt7IJwqXUm656K84O1I.GZJnBPeJTAYolV8PkknrGPUlLIUK', 'sjlim@mindslab.ai', '', '3', 'Y', '2020-02-12 17:22:10', 'sjlim@mindslab.ai', '2020-02-12 17:22:10', 'sjlim@mindslab.ai', '2020-02-12 17:22:10'),
	(77, 18, 'ge2ode2@gmail.com', '$2a$10$T1TBsbkC6WKF1M91prneX.ndEO31kVuyLyeZEw9hdCYyGN/Gvptm.', 'ge2ode2@gmail.com', '', '3', 'Y', '2020-02-12 17:27:25', 'ge2ode2@gmail.com', '2020-02-12 17:27:25', 'ge2ode2@gmail.com', '2020-02-12 17:27:25'),
	(78, 18, 'dhguswl12@gmail.com', '$2a$10$h0mjrxZMleMbV.2VlA5TXuxI6wj1cDD1TK3oN5ubkeicHWDOAheDO', 'dhguswl12@gmail.com', '', '3', 'Y', '2020-02-12 18:56:07', 'dhguswl12@gmail.com', '2020-02-12 18:56:07', 'dhguswl12@gmail.com', '2020-02-12 18:56:07'),
	(79, 18, 'cty0414@gmail.com', '$2a$10$TeT5xWk5.a0Jyu0aqiuiou8oYyhRL8kuWEMZA7X4JnjvIxpCrQneC', 'cty0414@gmail.com', '', '3', 'Y', '2020-02-13 11:10:41', 'cty0414@gmail.com', '2020-02-13 11:10:41', 'cty0414@gmail.com', '2020-02-13 11:10:41'),
	(80, 18, 'asdn9353@mindslab.ai', '$2a$10$pkUTa3cCIEvw.KV832VBOukfQuwsuJxLVudeOnH6z874Qyo26QNne', 'asdn9353@mindslab.ai', '', '3', 'Y', '2020-02-14 10:04:03', 'asdn9353@mindslab.ai', '2020-02-14 10:04:03', 'asdn9353@mindslab.ai', '2020-02-14 10:04:03'),
	(81, 18, 'mirye_an@mindslab.ai', '$2a$10$lEs1ARGGVrIjUq8nf5yY/O07TilpctcjNDghNlh2zeZhEPemvclH.', 'mirye_an@mindslab.ai', '', '3', 'Y', '2020-02-14 10:22:07', 'mirye_an@mindslab.ai', '2020-02-14 10:22:07', 'mirye_an@mindslab.ai', '2020-02-14 10:22:07'),
	(82, 18, 'byy0409@gmail.com', '$2a$10$HypsNQUdvxwtle/hhFXk3ONWhNCsdVIaDsolEFq8nk1Xe1vknh5l6', 'byy0409@gmail.com', '', '3', 'Y', '2020-02-14 11:11:22', 'byy0409@gmail.com', '2020-02-14 11:11:22', 'byy0409@gmail.com', '2020-02-14 11:11:22'),
	(83, 18, 'jymkim1@mindslab.ai', '$2a$10$xfYS5/0cXUqoLXjjVQaY3u7/K.Dl1YxMo9MsoeFIZX8cJbuQ0Pwqy', 'jymkim1@mindslab.ai', '', '3', 'Y', '2020-02-14 11:13:20', 'jymkim1@mindslab.ai', '2020-02-14 11:13:20', 'jymkim1@mindslab.ai', '2020-02-14 11:13:20'),
	(84, 18, 'chamchee0@mindslab.ai', '$2a$10$N9RE1aCYbdiGTRY/J5K24ueCGugJaJjH0QOyptlvluFC/zr7YcUNe', 'chamchee0@mindslab.ai', '', '3', 'Y', '2020-02-14 11:14:42', 'chamchee0@mindslab.ai', '2020-02-14 11:14:42', 'chamchee0@mindslab.ai', '2020-02-14 11:14:42'),
	(85, 18, 'jh.park@mindslab.ai', '$2a$10$AkMXKU/M/8wyoBYKxaG26.J5pE8Q/o4DQeXLJwKIljNIXGMhim1Vu', 'jh.park@mindslab.ai', '', '3', 'Y', '2020-02-14 11:23:20', 'jh.park@mindslab.ai', '2020-02-14 11:23:20', 'jh.park@mindslab.ai', '2020-02-14 11:23:20'),
	(86, 18, 'alswlsi7@mindslab.ai', '$2a$10$8kQHssT6qmTl5mXYTn8wUuqvqBuRma4wxnToj2HDvKpBSHquXEFAG', 'alswlsi7@mindslab.ai', '', '3', 'Y', '2020-02-14 12:45:57', 'alswlsi7@mindslab.ai', '2020-02-14 12:45:57', 'alswlsi7@mindslab.ai', '2020-02-14 12:45:57'),
	(87, 18, 'sywsyw1025@gmail.com', '$2a$10$AVQx.LbVHwtB1CJVDYiEw.gwRRdTpwObcQqF5aCLFalMeQNq5OlKO', 'sywsyw1025@gmail.com', '', '3', 'Y', '2020-02-14 12:46:21', 'sywsyw1025@gmail.com', '2020-02-14 12:46:21', 'sywsyw1025@gmail.com', '2020-02-14 12:46:21'),
	(88, 18, 'dmlee@mindslab.ai', '$2a$10$9Kk4tE0EfPuVZKy8nLc50eny8o.kjYyX/kvsaCgVeVqcJFHzDSB4e', 'dmlee@mindslab.ai', '', '3', 'Y', '2020-02-14 13:15:50', 'dmlee@mindslab.ai', '2020-02-14 13:15:50', 'dmlee@mindslab.ai', '2020-02-14 13:15:50'),
	(89, 18, 'sperospera@mindslab.ai', '$2a$10$3nIfRaBYgSfKYOnNk6T9xOcr.EfboQI7bYaqqvi7GprToK0OfP.Nu', 'sperospera@mindslab.ai', '', '3', 'Y', '2020-02-14 13:44:18', 'sperospera@mindslab.ai', '2020-02-14 13:44:18', 'sperospera@mindslab.ai', '2020-02-14 13:44:18'),
	(90, 18, 'jay@mindslab.ai', '$2a$10$WbxPkl/A/joECKI7Y6TJYeF0nQa2sSieN5.C1UnaXafNo4Mbm/s92', 'jay@mindslab.ai', '', '3', 'Y', '2020-02-14 14:19:46', 'jay@mindslab.ai', '2020-02-14 14:19:46', 'jay@mindslab.ai', '2020-02-14 14:19:46'),
	(91, 18, 'tagi@mindslab.ai', '$2a$10$suW0GVvjdLXW2CzfBkz51uvVGuQyV1qlHm5MkLYEnh2PSHIf0utFy', 'tagi@mindslab.ai', '', '3', 'Y', '2020-02-15 14:11:15', 'tagi@mindslab.ai', '2020-02-15 14:11:15', 'tagi@mindslab.ai', '2020-02-15 14:11:15'),
	(92, 18, 'chris@mindslab.ai', '$2a$10$Mc4t8RsTF.X6OVYoep/7zOCY06GJ4R79XAlnjdwoT5Kgl1oaDxumS', 'chris@mindslab.ai', '', '3', 'Y', '2020-02-17 12:04:53', 'chris@mindslab.ai', '2020-02-17 12:04:53', 'chris@mindslab.ai', '2020-02-17 12:04:53'),
	(93, 18, 'shinsegaed1@gmail.com', '$2a$10$9ClXEIhl09BYMst3.RCGqequawAPQxQ192Cn3YRY6AlRBwOa.tovu', 'shinsegaed1@gmail.com', '', '3', 'Y', '2020-02-17 13:34:51', 'shinsegaed1@gmail.com', '2020-02-17 13:34:51', 'shinsegaed1@gmail.com', '2020-02-17 13:34:51'),
	(94, 18, 'jamielee@mindslab.ai', '$2a$10$5.LQGVwdsjaer9D/8FjP4uokoIEkftQvoDDmeDGGe88pjRdFFmTS6', 'jamielee@mindslab.ai', '', '3', 'Y', '2020-02-17 14:11:25', 'jamielee@mindslab.ai', '2020-02-17 14:11:25', 'jamielee@mindslab.ai', '2020-02-17 14:11:25'),
	(95, 18, 'princeps10@mindslab.ai', '$2a$10$hBMpAepIWg.ZUNVAm44NzOejVVq32g.cOdWUWvf6.YCU5/BbSb/lu', 'princeps10@mindslab.ai', '', '3', 'Y', '2020-02-17 15:22:43', 'princeps10@mindslab.ai', '2020-02-17 15:22:43', 'princeps10@mindslab.ai', '2020-02-17 15:22:43'),
	(96, 18, 'misu@mindslab.ai', '$2a$10$nHtRrIOK89nDXqHWCewY/OiUTUQBzmtPahNyQBEh2VXrJLzXgYPYC', 'misu@mindslab.ai', '', '3', 'Y', '2020-02-17 17:46:25', 'misu@mindslab.ai', '2020-02-17 17:46:25', 'misu@mindslab.ai', '2020-02-17 17:46:25'),
	(97, 18, 'joon@mindslab.ai', '$2a$10$.EGQ.Nfy0S7Of3/qNnsJPuR4umuXVNQfsv57s3D1arbF6EXc1hn9a', 'joon@mindslab.ai', '', '3', 'Y', '2020-02-17 17:55:14', 'joon@mindslab.ai', '2020-02-17 17:55:14', 'joon@mindslab.ai', '2020-02-17 17:55:14'),
	(98, 18, 'kyungyoung.co@gmail.com', '$2a$10$L2RWJrA5mZMbvruOGJT90eOIktIbqXZlUq71/FHO8bKglluqosOqO', 'kyungyoung.co@gmail.com', '', '3', 'Y', '2020-02-17 18:49:56', 'kyungyoung.co@gmail.com', '2020-02-17 18:49:56', 'kyungyoung.co@gmail.com', '2020-02-17 18:49:56'),
	(99, 18, 'hahahihiho16@gmail.com', '$2a$10$Cm2T318NtxgE9v9JjZ8esOXCB/1BiFfBXXkcao2E7P/bFNgWtNCZm', 'hahahihiho16@gmail.com', '', '3', 'Y', '2020-02-17 21:17:17', 'hahahihiho16@gmail.com', '2020-02-17 21:17:17', 'hahahihiho16@gmail.com', '2020-02-17 21:17:17'),
	(100, 18, 'baiknseo@gmail.com', '$2a$10$FMEQQxw9OaLGkr/AAbK4dOI9lYpja2BcW.Khx16TqF0sWZUtCv54m', 'baiknseo@gmail.com', '', '3', 'Y', '2020-02-18 09:28:41', 'baiknseo@gmail.com', '2020-02-18 09:28:41', 'baiknseo@gmail.com', '2020-02-18 09:28:41'),
	(101, 18, 'yamjye0818@gmail.com', '$2a$10$EiVSPGdIYaZz9gskUHOVN.XUxWkVXjLY.q3S6Bq9cvnwjMF0WV.G2', 'yamjye0818@gmail.com', '', '3', 'Y', '2020-02-18 10:10:28', 'yamjye0818@gmail.com', '2020-02-18 10:10:28', 'yamjye0818@gmail.com', '2020-02-18 10:10:28'),
	(102, 18, 'hwon@mindslab.ai', '$2a$10$LN6QvHozhCGRo2ox/qoylOGUx3Ku1ZmVgSgyjKJqsZU8nZrxS9AJW', 'hwon@mindslab.ai', '', '3', 'Y', '2020-02-18 11:15:17', 'hwon@mindslab.ai', '2020-02-18 11:15:17', 'hwon@mindslab.ai', '2020-02-18 11:15:17'),
	(103, 18, 'janghun2722@gmail.com', '$2a$10$Yeieq.mUXESnrY2QlBRlDeKzWL/mtadhgXbXLjV4.jLkyZAgzrE06', 'janghun2722@gmail.com', '', '3', 'Y', '2020-02-18 11:29:02', 'janghun2722@gmail.com', '2020-02-18 11:29:02', 'janghun2722@gmail.com', '2020-02-18 11:29:02'),
	(104, 18, 'dwlim@planit.ai', '$2a$10$SO9lYOhfHpy7XkNk7.tTGuQypLnjiz12t2Gi3DnManph7uNiJzUF2', 'dwlim@planit.ai', '', '3', 'Y', '2020-02-18 11:44:19', 'dwlim@planit.ai', '2020-02-18 11:44:19', 'dwlim@planit.ai', '2020-02-18 11:44:19'),
	(105, 18, 'wisdom@mindslab.ai', '$2a$10$pVjZgI6YKimywMT9fe9VCuUJw4rmgTmiYoefjc1pX.FnJiEZTNo9W', 'wisdom@mindslab.ai', '', '3', 'Y', '2020-02-18 13:52:17', 'wisdom@mindslab.ai', '2020-02-18 13:52:17', 'wisdom@mindslab.ai', '2020-02-18 13:52:17'),
	(106, 18, 'ecreative7@gmail.com', '$2a$10$GF2P6G.1RvTt5NAs8w0RQeejH25F4QWGlZh/hZnuNUDEKRMZ/B..e', 'ecreative7@gmail.com', '', '3', 'Y', '2020-02-18 14:14:07', 'ecreative7@gmail.com', '2020-02-18 14:14:07', 'ecreative7@gmail.com', '2020-02-18 14:14:07'),
	(107, 18, 'aeinhugo@gmail.com', '$2a$10$gGEklG65eW/vJMk2LVrf.egSAUNt5zXn/4i/mHLLdnwD2f6JEbzFu', 'aeinhugo@gmail.com', '', '3', 'Y', '2020-02-18 16:07:01', 'aeinhugo@gmail.com', '2020-02-18 16:07:01', 'aeinhugo@gmail.com', '2020-02-18 16:07:01'),
	(108, 18, 'erin.kim.luxrobo@gmail.com', '$2a$10$1gnyLi8PmGgICvJvpKRm8ulijmUED/mYBceezD1XUlxbH5NEioCHK', 'erin.kim.luxrobo@gmail.com', '', '3', 'Y', '2020-02-18 16:24:50', 'erin.kim.luxrobo@gmail.com', '2020-02-18 16:24:50', 'erin.kim.luxrobo@gmail.com', '2020-02-18 16:24:50'),
	(109, 18, 'kimocorea@gmail.com', '$2a$10$uBiniNIrKFcdlG2sRqX4L.cyYnfd9PC6q5nVom8VQPgbOteXPx1EC', 'kimocorea@gmail.com', '', '3', 'Y', '2020-02-18 16:54:15', 'kimocorea@gmail.com', '2020-02-18 16:54:15', 'kimocorea@gmail.com', '2020-02-18 16:54:15'),
	(110, 18, 'brandon@mindslab.ai', '$2a$10$0GmbmfdL.wsdGOOD5emcE.Q5xGrvtwSrolCOaeC16hokPxbefQUqy', 'brandon@mindslab.ai', '', '3', 'Y', '2020-02-18 17:31:06', 'brandon@mindslab.ai', '2020-02-18 17:31:06', 'brandon@mindslab.ai', '2020-02-18 17:31:06'),
	(111, 18, 'hyunjeong.song.ai@gmail.com', '$2a$10$Q3i8LR0W9ooYkfUD8wiePOhFDGmMtuBG1LIw4x7dA8TTOceXJCCnq', 'hyunjeong.song.ai@gmail.com', '', '3', 'Y', '2020-02-18 17:47:35', 'hyunjeong.song.ai@gmail.com', '2020-02-18 17:47:35', 'hyunjeong.song.ai@gmail.com', '2020-02-18 17:47:35'),
	(112, 18, 'eunjee.lee@mindslab.ai', '$2a$10$0qKi9OIcWjbRsJ4sRYHwp.wjIJYJFTD6YDHnRn5GmUG8K.JLJaaAa', 'eunjee.lee@mindslab.ai', '', '3', 'Y', '2020-02-18 17:49:19', 'eunjee.lee@mindslab.ai', '2020-02-18 17:49:19', 'eunjee.lee@mindslab.ai', '2020-02-18 17:49:19'),
	(113, 18, 'whitesun383@gmail.com', '$2a$10$/8FWE7UD8VPVHZy5hNo1KOOgQ0QXqDI4IUTMg7SG0sNnv/IDzOEmq', 'whitesun383@gmail.com', '', '3', 'Y', '2020-02-18 22:02:01', 'whitesun383@gmail.com', '2020-02-18 22:02:01', 'whitesun383@gmail.com', '2020-02-18 22:02:01'),
	(114, 18, 'hyuck1123@gmail.com', '$2a$10$kvygMllpZnO8WNXu7begeewVbxGRNMio/J2YX8X6/.3sI6MXwKG2S', 'hyuck1123@gmail.com', '', '3', 'Y', '2020-02-19 16:05:04', 'hyuck1123@gmail.com', '2020-02-19 16:05:04', 'hyuck1123@gmail.com', '2020-02-19 16:05:04'),
	(115, 18, 'good4ram@gmail.com', '$2a$10$hYTrBKlm2WRHeJ4tdxG4aOVnRF9o.V2meKY3Yfr2Ao.zpK00wKktC', 'good4ram@gmail.com', '', '3', 'Y', '2020-02-19 16:16:01', 'good4ram@gmail.com', '2020-02-19 16:16:01', 'good4ram@gmail.com', '2020-02-19 16:16:01'),
	(116, 18, 'fighterz10050@gmail.com', '$2a$10$/J4/F2g1YPw742OdrrbF1OL/K8S776m/b/90DyL/k8eJhR/i9mv/S', 'fighterz10050@gmail.com', '', '3', 'Y', '2020-02-19 17:38:21', 'fighterz10050@gmail.com', '2020-02-19 17:38:21', 'fighterz10050@gmail.com', '2020-02-19 17:38:21'),
	(117, 18, 'yustephano1970@gmail.com', '$2a$10$Luci8ICAyq4MSp/te.DpXuUeb0PNmtYw8D4tJCgA4xHgHB5Jm7JEe', 'yustephano1970@gmail.com', '', '3', 'Y', '2020-02-19 17:38:51', 'yustephano1970@gmail.com', '2020-02-19 17:38:51', 'yustephano1970@gmail.com', '2020-02-19 17:38:51'),
	(118, 18, 'gamadeus@gmail.com', '$2a$10$bxeg.GUja7Zg4/93YO37sOYzGEJGBCd9kyLDAbqsed8GbbC4kKt82', 'gamadeus@gmail.com', '', '3', 'Y', '2020-02-19 19:44:30', 'gamadeus@gmail.com', '2020-02-19 19:44:30', 'gamadeus@gmail.com', '2020-02-19 19:44:30'),
	(119, 18, 'gongro6639@gmail.com', '$2a$10$GbJ.kiDQgw1xsohJ1Y6LZ.ewCahH0o5B3NFIYPLZLRHkogyl44b9a', 'gongro6639@gmail.com', '', '3', 'Y', '2020-02-20 00:12:00', 'gongro6639@gmail.com', '2020-02-20 00:12:00', 'gongro6639@gmail.com', '2020-02-20 00:12:00'),
	(120, 18, 'sjkim@mindslab.ai', '$2a$10$KKfTZzWheruD1S.FY3SX1.R10UBqAcUy2DXqjEzo8rQsEEozVcuaG', 'sjkim@mindslab.ai', '', '3', 'Y', '2020-02-20 09:32:05', 'sjkim@mindslab.ai', '2020-02-20 09:32:05', 'sjkim@mindslab.ai', '2020-02-20 09:32:05'),
	(121, 18, 'bjlee@mindslab.ai', '$2a$10$r5K5wFpITJ9/lSsHxPGEX.THrPxT8jbUU6.v4YMi0oOtFtuNs32Cu', 'bjlee@mindslab.ai', '', '3', 'Y', '2020-02-20 10:33:01', 'bjlee@mindslab.ai', '2020-02-20 10:33:01', 'bjlee@mindslab.ai', '2020-02-20 10:33:01'),
	(122, 18, 'sarah.lsware@gmail.com', '$2a$10$zjIOLX/G6UDd.ERbufGOfu7kLSGGTlg0jscZjB8wb3nF1KqpEpbHe', 'sarah.lsware@gmail.com', '', '3', 'Y', '2020-02-20 15:52:08', 'sarah.lsware@gmail.com', '2020-02-20 15:52:08', 'sarah.lsware@gmail.com', '2020-02-20 15:52:08'),
	(123, 18, 'blueberry92450@gmail.com', '$2a$10$LqDEakD2zTLjO6ToPUPIYOiD7JCfkMy.OFueeYy6dBWmz8u3LK//W', 'blueberry92450@gmail.com', '', '3', 'Y', '2020-02-20 20:01:40', 'blueberry92450@gmail.com', '2020-02-20 20:01:40', 'blueberry92450@gmail.com', '2020-02-20 20:01:40'),
	(124, 18, 'skyprotoss@gmail.com', '$2a$10$nT557UaZvG8jNhDljtkFbugdrYEGEAhHu1zFoDbJKFxckBbyYRr2O', 'skyprotoss@gmail.com', '', '3', 'Y', '2020-02-21 00:19:29', 'skyprotoss@gmail.com', '2020-02-21 00:19:29', 'skyprotoss@gmail.com', '2020-02-21 00:19:29'),
	(125, 18, 'changyoung0310@gmail.com', '$2a$10$qI4EKdxeKzouszAs1e6Ni.6dXqWXwmqypdi8elNos8wn315UFO4K6', 'changyoung0310@gmail.com', '', '3', 'Y', '2020-02-21 04:38:28', 'changyoung0310@gmail.com', '2020-02-21 04:38:28', 'changyoung0310@gmail.com', '2020-02-21 04:38:28'),
	(126, 18, 'jayroncena@gmail.com', '$2a$10$d784/ad0Yy25vFMnar1J0OJe9tx6DcuXuCRneMd9hXV8tslEikZ6G', 'jayroncena@gmail.com', '', '3', 'Y', '2020-02-21 08:55:51', 'jayroncena@gmail.com', '2020-02-21 08:55:51', 'jayroncena@gmail.com', '2020-02-21 08:55:51'),
	(127, 18, 'ggawoos@gmail.com', '$2a$10$/NYrhIfp/viF/PR4v6DtO.0slaevWtFJq6kuFml.ZEvOFeaNaUiOi', 'ggawoos@gmail.com', '', '3', 'Y', '2020-02-21 09:02:22', 'ggawoos@gmail.com', '2020-02-21 09:02:22', 'ggawoos@gmail.com', '2020-02-21 09:02:22'),
	(128, 18, 'grootergi98@gmail.com', '$2a$10$SPoDotat4heRzTTOPaYvreJHmKb/LrgxaB4mpAVhkoyj2t0//MpEi', 'grootergi98@gmail.com', '', '3', 'Y', '2020-02-21 09:03:26', 'grootergi98@gmail.com', '2020-02-21 09:03:26', 'grootergi98@gmail.com', '2020-02-21 09:03:26'),
	(129, 18, 'maristella@mindslab.ai', '$2a$10$iA2v8TdNyPGo2L/v8lh7peKiQ5PKtZOsOEvDqlQepx8Q4FzcEXqQ2', 'maristella@mindslab.ai', '', '3', 'Y', '2020-02-21 09:15:09', 'maristella@mindslab.ai', '2020-02-21 09:15:09', 'maristella@mindslab.ai', '2020-02-21 09:15:09'),
	(130, 18, '2ghdms@naver.com', '$2a$10$AG97RD9uYKwTaI6UouBzfO6.ZYiNE.M6HxHttxrIR9GU9P.hLgUWq', '2ghdms@naver.com', '', '3', 'Y', '2020-02-21 10:06:08', '2ghdms@naver.com', '2020-02-21 10:06:08', '2ghdms@naver.com', '2020-02-21 10:06:08'),
	(131, 18, 'newsforlemon@gmail.com', '$2a$10$Jt8NOxVdahDOcdy4PGPBaup5CcWWQG/ow9oKYrjlVG9XuZfJ2E8Yu', 'newsforlemon@gmail.com', '', '3', 'Y', '2020-02-21 12:56:22', 'newsforlemon@gmail.com', '2020-02-21 12:56:22', 'newsforlemon@gmail.com', '2020-02-21 12:56:22'),
	(132, 18, 'biztelevision20@gmail.com', '$2a$10$RwbNenK0lMgt6pwOZJcUw.pPUxpeeyHUfyhVLgqcvqiGqh5XrRMJO', 'biztelevision20@gmail.com', '', '3', 'Y', '2020-02-21 15:52:34', 'biztelevision20@gmail.com', '2020-02-21 15:52:34', 'biztelevision20@gmail.com', '2020-02-21 15:52:34'),
	(133, 18, 'parkkar42@gmail.com', '$2a$10$3qeblQYMUEqNSjYoo0qMUe9d9BJslGx486OQCIfqiDdDXLHZjXx0G', 'parkkar42@gmail.com', '', '3', 'Y', '2020-02-21 18:57:16', 'parkkar42@gmail.com', '2020-02-21 18:57:16', 'parkkar42@gmail.com', '2020-02-21 18:57:16'),
	(134, 18, 'hyukjin.kim1989@gmail.com', '$2a$10$SDOOrn2LkqGLJ5g2Va3z7OPmUELdC0796jCVRHteX..Y4qrstzUFG', 'hyukjin.kim1989@gmail.com', '', '3', 'Y', '2020-02-22 01:15:01', 'hyukjin.kim1989@gmail.com', '2020-02-22 01:15:01', 'hyukjin.kim1989@gmail.com', '2020-02-22 01:15:01'),
	(135, 18, '94sabio@gmail.com', '$2a$10$COzY5iR3BUr3xvdlAArSVOA6BWzS9tQuMtG9tU0T/clTab9g.l5.C', '94sabio@gmail.com', '', '3', 'Y', '2020-02-22 01:57:14', '94sabio@gmail.com', '2020-02-22 01:57:14', '94sabio@gmail.com', '2020-02-22 01:57:14'),
	(136, 18, 'me@1kko.com', '$2a$10$.UmAMuW6hTjPdYHtC7Hx5OITAAXu//FeFg6a/OMyiCnKYllOZmaOu', 'me@1kko.com', '', '3', 'Y', '2020-02-22 10:10:13', 'me@1kko.com', '2020-02-22 10:10:13', 'me@1kko.com', '2020-02-22 10:10:13'),
	(137, 18, 'maxoverpro@gmail.com', '$2a$10$mL5B2S8hrRcsv2VQzBoSJuZuYo.Oj9YnDZaLcd2ujlKWaO8WY1moS', 'maxoverpro@gmail.com', '', '3', 'Y', '2020-02-23 00:32:56', 'maxoverpro@gmail.com', '2020-02-23 00:32:56', 'maxoverpro@gmail.com', '2020-02-23 00:32:56'),
	(138, 18, 'mskim@assist.ac.kr', '$2a$10$RT0uyfm59fp0EscpZR14TeTqoFZNwzudKJMfKAWXxVdzggsY7fx8G', 'mskim@assist.ac.kr', '', '3', 'Y', '2020-02-23 02:28:35', 'mskim@assist.ac.kr', '2020-02-23 02:28:35', 'mskim@assist.ac.kr', '2020-02-23 02:28:35'),
	(139, 18, 'youngchul.ahn80@gmail.com', '$2a$10$1ujJcWZ8R8oMomBFmyHPNOmVarU7n1FB5y8RRauenS0NpKSuLYUC2', 'youngchul.ahn80@gmail.com', '', '3', 'Y', '2020-02-23 10:35:16', 'youngchul.ahn80@gmail.com', '2020-02-23 10:35:16', 'youngchul.ahn80@gmail.com', '2020-02-23 10:35:16'),
	(140, 18, 'itkim@etriholdings.com', '$2a$10$h3EXJst3iOahse5cOSJawOqWlG2eotOtEAa3MRtgOBW1VWi3dwjdq', 'itkim@etriholdings.com', '', '3', 'Y', '2020-02-23 13:50:15', 'itkim@etriholdings.com', '2020-02-23 13:50:15', 'itkim@etriholdings.com', '2020-02-23 13:50:15'),
	(141, 18, 'wiseksw@gmail.com', '$2a$10$UTDZgrXoEfb5rPpB/Xy6u.YbYkFf9PehCnhmfgRODranhJkoEGA1y', 'wiseksw@gmail.com', '', '3', 'Y', '2020-02-23 16:30:14', 'wiseksw@gmail.com', '2020-02-23 16:30:14', 'wiseksw@gmail.com', '2020-02-23 16:30:14'),
	(142, 18, 'kantarkorea1@gmail.com', '$2a$10$29QK1HvZCvJ/dcbVBnxdv.vXapuh7RxaaLGyW3iebSDzQmLKrYLoO', 'kantarkorea1@gmail.com', '', '3', 'Y', '2020-02-24 00:10:28', 'kantarkorea1@gmail.com', '2020-02-24 00:10:28', 'kantarkorea1@gmail.com', '2020-02-24 00:10:28'),
	(143, 18, 'kw83.lee@gmail.com', '$2a$10$.VnbbpsmbuQD4VyAwYeuBOUkEa4blpfWyVYtI5tCkLWwjR5Z8HiwG', 'kw83.lee@gmail.com', '', '3', 'Y', '2020-02-24 00:23:53', 'kw83.lee@gmail.com', '2020-02-24 00:23:53', 'kw83.lee@gmail.com', '2020-02-24 00:23:53'),
	(144, 18, 'lazystar20000@gmail.com', '$2a$10$XsnlsTt5s6WrEWgFwq6iouh3YxDXQENglcsLibrnknRElfBcAdBWu', 'lazystar20000@gmail.com', '', '3', 'Y', '2020-02-24 01:07:31', 'lazystar20000@gmail.com', '2020-02-24 01:07:31', 'lazystar20000@gmail.com', '2020-02-24 01:07:31'),
	(145, 18, 'swoosuk2000@gmail.com', '$2a$10$Fzcwxk34p14402yNEhNnyem2Yj87GaTmhOvnVjqECLEzFF3vvTfLO', 'swoosuk2000@gmail.com', '', '3', 'Y', '2020-02-24 11:02:57', 'swoosuk2000@gmail.com', '2020-02-24 11:02:57', 'swoosuk2000@gmail.com', '2020-02-24 11:02:57'),
	(146, 18, 'sylee.cool@gmail.com', '$2a$10$oho1KBTRFLD2c6tzGA6K0u5VSc/diFgHlNkMKJ8wzYl3yCOZsZ4vq', 'sylee.cool@gmail.com', '', '3', 'Y', '2020-02-24 11:04:14', 'sylee.cool@gmail.com', '2020-02-24 11:04:14', 'sylee.cool@gmail.com', '2020-02-24 11:04:14'),
	(147, 18, 'dongs2001@mnms.kr', '$2a$10$fEvNG4z8ysDdX0WhoHlLw.o/5G4YkzGdpeZlPYqRfoPtVV5VJFux6', 'dongs2001@mnms.kr', '', '3', 'Y', '2020-02-24 11:04:17', 'dongs2001@mnms.kr', '2020-02-24 11:04:17', 'dongs2001@mnms.kr', '2020-02-24 11:04:17'),
	(148, 18, 'jeknhjg@gmail.com', '$2a$10$0Z0KPJ5lqbaRx5taRiUFUOqznWvPn/2rcD8LqZyq.wG/KtH.s.09K', 'jeknhjg@gmail.com', '', '3', 'Y', '2020-02-24 11:14:00', 'jeknhjg@gmail.com', '2020-02-24 11:14:00', 'jeknhjg@gmail.com', '2020-02-24 11:14:00'),
	(149, 18, 'younglealee@gmail.com', '$2a$10$96/TUgCbv6oj9T/tQtiSduSejTi3e.iSD/B1EQAnFW./iYXlxKQT2', 'younglealee@gmail.com', '', '3', 'Y', '2020-02-24 11:22:46', 'younglealee@gmail.com', '2020-02-24 11:22:46', 'younglealee@gmail.com', '2020-02-24 11:22:46'),
	(150, 18, 'utyre1357@gmail.com', '$2a$10$hRd2PmuVandeBcboTwexT.lQyeMQmelpACK9iKUoHUFa/fIKAyKr.', 'utyre1357@gmail.com', '', '3', 'Y', '2020-02-24 11:32:47', 'utyre1357@gmail.com', '2020-02-24 11:32:47', 'utyre1357@gmail.com', '2020-02-24 11:32:47'),
	(151, 18, 'drumtj@gmail.com', '$2a$10$95RHnPqYDV7ehuNIrjpV5.DVcvw7Fj0Sabcy0FiKswFU0FW7pgtdG', 'drumtj@gmail.com', '', '3', 'Y', '2020-02-24 11:32:53', 'drumtj@gmail.com', '2020-02-24 11:32:53', 'drumtj@gmail.com', '2020-02-24 11:32:53'),
	(152, 18, 'kipark@ecstel.co.kr', '$2a$10$IxzJh/zw9mxm/xZuE.xNnO1L.5.mQWa3RsEbIF4yV4n2mzZ4XN4s.', 'kipark@ecstel.co.kr', '', '3', 'Y', '2020-02-24 11:57:20', 'kipark@ecstel.co.kr', '2020-02-24 11:57:20', 'kipark@ecstel.co.kr', '2020-02-24 11:57:20'),
	(153, 18, 'qkrrltka36@gmail.com', '$2a$10$nj0XEm4fliAI0u.8U.eqQOWDchJbh54Gkfnd5cRkdpEbeT2Wiva0q', 'qkrrltka36@gmail.com', '', '3', 'Y', '2020-02-24 12:00:47', 'qkrrltka36@gmail.com', '2020-02-24 12:00:47', 'qkrrltka36@gmail.com', '2020-02-24 12:00:47'),
	(154, 18, 'dufaojjang@gmail.com', '$2a$10$UienrHvZfp0VYat/6xmDaePtFBAZIBbV6Rplso7Ha6ccorpR1Bpc6', 'dufaojjang@gmail.com', '', '3', 'Y', '2020-02-24 12:13:07', 'dufaojjang@gmail.com', '2020-02-24 12:13:07', 'dufaojjang@gmail.com', '2020-02-24 12:13:07'),
	(155, 18, 'jsjang@biobrain.kr', '$2a$10$3ccR2i6qEI1FmeqsCdEKxu6B.JFT94Su742l8DrK8SAe0v0IpjxZm', 'jsjang@biobrain.kr', '', '3', 'Y', '2020-02-24 12:31:24', 'jsjang@biobrain.kr', '2020-02-24 12:31:24', 'jsjang@biobrain.kr', '2020-02-24 12:31:24'),
	(156, 18, 'dyeus0509@gmail.com', '$2a$10$rgCrM9cnwvzxwJJAJV./E.65tRzDBFjf9MMAL.1cyZRPPBowAvajO', 'dyeus0509@gmail.com', '', '3', 'Y', '2020-02-24 13:07:51', 'dyeus0509@gmail.com', '2020-02-24 13:07:51', 'dyeus0509@gmail.com', '2020-02-24 13:07:51'),
	(157, 18, 'scientistlee1@gmail.com', '$2a$10$KV83YXG9U4IrQzK.RYz9R./dKwHBbH0XDsjJxeV1hxKDrr5UqQfFy', 'scientistlee1@gmail.com', '', '3', 'Y', '2020-02-24 14:54:27', 'scientistlee1@gmail.com', '2020-02-24 14:54:27', 'scientistlee1@gmail.com', '2020-02-24 14:54:27'),
	(158, 18, 'juhyung.j.kim@pwc.com', '$2a$10$VKMkUWBCE5Q.MgDc0.fk5.bSu1yGkY9.2aE2TnBsAAbBSWHfCtFI2', 'juhyung.j.kim@pwc.com', '', '3', 'Y', '2020-02-24 15:21:50', 'juhyung.j.kim@pwc.com', '2020-02-24 15:21:50', 'juhyung.j.kim@pwc.com', '2020-02-24 15:21:50'),
	(159, 18, 'dodream9373@gmail.com', '$2a$10$Do17fNnowM8ccenlAz5ucOCNeyEt8Haz4rbwqejjj.TYft05IxE0O', 'dodream9373@gmail.com', '', '3', 'Y', '2020-02-24 15:38:05', 'dodream9373@gmail.com', '2020-02-24 15:38:05', 'dodream9373@gmail.com', '2020-02-24 15:38:05'),
	(160, 18, 'sjlim4646@gmail.com', '$2a$10$fFnioERmNaHEO/.EnT6VDuOe3uz5LtxkNRnA92kRZ3CKFcYAeoueW', 'sjlim4646@gmail.com', '', '3', 'Y', '2020-02-24 15:53:08', 'sjlim4646@gmail.com', '2020-02-24 15:53:08', 'sjlim4646@gmail.com', '2020-02-24 15:53:08'),
	(161, 18, 'ducbin@mindslab.ai', '$2a$10$d3kd1djU/ybrxuZ066TwU..0uKNFYtJSSrvf6JT1GZMUFfbTOrR9G', 'ducbin@mindslab.ai', '', '3', 'Y', '2020-02-24 17:20:43', 'ducbin@mindslab.ai', '2020-02-24 17:20:43', 'ducbin@mindslab.ai', '2020-02-24 17:20:43'),
	(162, 18, 'kim.jeongah@gmail.com', '$2a$10$WHx7Ovmkju0YkK0aA6ufeO2eoeYNyR6DNW8xjbFPwQ/f.CUUSEibm', 'kim.jeongah@gmail.com', '', '3', 'Y', '2020-02-24 17:44:56', 'kim.jeongah@gmail.com', '2020-02-24 17:44:56', 'kim.jeongah@gmail.com', '2020-02-24 17:44:56'),
	(163, 18, 'beomsoo199@gmail.com', '$2a$10$OUP.83DHAQX01hSEYC1CnOplC3efnd9fKS8lIIg2bjM71GZb220ne', 'beomsoo199@gmail.com', '', '3', 'Y', '2020-02-24 18:19:38', 'beomsoo199@gmail.com', '2020-02-24 18:19:38', 'beomsoo199@gmail.com', '2020-02-24 18:19:38'),
	(164, 18, 'seon8545@gmail.com', '$2a$10$z8vo4AG9oHBfsZ5FaRX/QevWl4Y3hWf9kfSjEq0kFkf.AoGS0Q0Hq', 'seon8545@gmail.com', '', '3', 'Y', '2020-02-24 18:53:03', 'seon8545@gmail.com', '2020-02-24 18:53:03', 'seon8545@gmail.com', '2020-02-24 18:53:03'),
	(165, 18, 'kevin.oh@onda.me', '$2a$10$UDGN81Phx3U4Qk3ldaNYe.9gSJklaglpNoejVbpudpK5IzPqLfwzC', 'kevin.oh@onda.me', '', '3', 'Y', '2020-02-24 19:13:12', 'kevin.oh@onda.me', '2020-02-24 19:13:12', 'kevin.oh@onda.me', '2020-02-24 19:13:12'),
	(166, 18, 'changho@mindslab.ai', '$2a$10$tiwDf2Mh4vCyPM4YnmeZ5uGE/fgKo9Al7KPiL5C48oBFOAKFsX9/y', 'changho@mindslab.ai', '', '3', 'Y', '2020-02-24 22:20:35', 'changho@mindslab.ai', '2020-02-24 22:20:35', 'changho@mindslab.ai', '2020-02-24 22:20:35'),
	(167, 18, 'moonsuk73@gmail.com', '$2a$10$NMJisvnxITFYv6JenjUINuF3I5YfKQTwW6Bn0HXg.tKm8Fpl1vmtW', 'moonsuk73@gmail.com', '', '3', 'Y', '2020-02-25 08:03:23', 'moonsuk73@gmail.com', '2020-02-25 08:03:23', 'moonsuk73@gmail.com', '2020-02-25 08:03:23'),
	(168, 18, 'kjh01066525201@gmail.com', '$2a$10$oSLMGwVQeN1y1X/VTBhrN.PKknUq950DX5kzf34qeof8Y08CUxhLG', 'kjh01066525201@gmail.com', '', '3', 'Y', '2020-02-25 08:52:55', 'kjh01066525201@gmail.com', '2020-02-25 08:52:55', 'kjh01066525201@gmail.com', '2020-02-25 08:52:55'),
	(169, 18, 'jaimeyoon@gmail.com', '$2a$10$XJtEdrA/SJiJ/7MdrlHqDeFMdMhcaRx7k5z0GGKoAT/GndE8kZEOW', 'jaimeyoon@gmail.com', '', '3', 'Y', '2020-02-25 09:26:31', 'jaimeyoon@gmail.com', '2020-02-25 09:26:31', 'jaimeyoon@gmail.com', '2020-02-25 09:26:31'),
	(170, 18, 'shj7831@gmail.com', '$2a$10$bfoBMKYiSkfbXGqb1DTpc.Jj1FYv5We.6tEDM3jYXJKkpvPPL2Jqi', 'shj7831@gmail.com', '', '3', 'Y', '2020-02-25 10:50:30', 'shj7831@gmail.com', '2020-02-25 10:50:30', 'shj7831@gmail.com', '2020-02-25 10:50:30'),
	(171, 18, 'yungjokim@naver.com', '$2a$10$xj0cIgzNnRzqp.vJp/4ApO/F87w/enVqOs9E.mpv1PsM4E5pQK29S', 'yungjokim@naver.com', '', '3', 'Y', '2020-02-25 11:11:41', 'yungjokim@naver.com', '2020-02-25 11:11:41', 'yungjokim@naver.com', '2020-02-25 11:11:41'),
	(172, 18, 'junhyung.park.aj@gmail.com', '$2a$10$L4LKro.sVVCIJl1MCDq1IekNe9fYEb6JCa44bVpSA6mEoNshXIXeC', 'junhyung.park.aj@gmail.com', '', '3', 'Y', '2020-02-25 12:36:13', 'junhyung.park.aj@gmail.com', '2020-02-25 12:36:13', 'junhyung.park.aj@gmail.com', '2020-02-25 12:36:13'),
	(173, 18, 'king2415@gmail.com', '$2a$10$xi0u..eapFu9S4dnayaCceoxKFeZLppc5Wgap.Axc023ysijtDa3q', 'king2415@gmail.com', '', '3', 'Y', '2020-02-25 12:48:03', 'king2415@gmail.com', '2020-02-25 12:48:03', 'king2415@gmail.com', '2020-02-25 12:48:03'),
	(174, 18, 'regpath@gmail.com', '$2a$10$Guc2OE6PbU6VZUDODfcI8.upK/YEswK.VF63dTQN8YWDym9Xv4uQ2', 'regpath@gmail.com', '', '3', 'Y', '2020-02-25 13:06:57', 'regpath@gmail.com', '2020-02-25 13:06:57', 'regpath@gmail.com', '2020-02-25 13:06:57'),
	(175, 18, 'chief@idixs.com', '$2a$10$j6td07nEVG5govYbUMiW9eDZuzvZSiWShj7Hwcdw6ZRfT.FxZAlJK', 'chief@idixs.com', '', '3', 'Y', '2020-02-25 13:31:58', 'chief@idixs.com', '2020-02-25 13:31:58', 'chief@idixs.com', '2020-02-25 13:31:58'),
	(176, 18, 'mecanthebu2@gmail.com', '$2a$10$oP6eqNfutC5AVCLDK2xsQeBH0gthoDi0/lGklrtHNAMmGk4RmbLl6', 'mecanthebu2@gmail.com', '', '3', 'Y', '2020-02-25 13:46:19', 'mecanthebu2@gmail.com', '2020-02-25 13:46:19', 'mecanthebu2@gmail.com', '2020-02-25 13:46:19'),
	(177, 18, 'youngwoolearning@gmail.com', '$2a$10$egvW8gaI2oPBLORDIs09juPzx7/bZRTQ2m0jZASKGOSGbczHoAu9q', 'youngwoolearning@gmail.com', '', '3', 'Y', '2020-02-25 14:24:05', 'youngwoolearning@gmail.com', '2020-02-25 14:24:05', 'youngwoolearning@gmail.com', '2020-02-25 14:24:05'),
	(178, 18, 'friend3675@gmail.com', '$2a$10$2hN41i/bOXBgW/Aq3TtBve4DwbrWvCYUJFpH6eZ.uIthU6dhXQ5nK', 'friend3675@gmail.com', '', '3', 'Y', '2020-02-25 14:31:56', 'friend3675@gmail.com', '2020-02-25 14:31:56', 'friend3675@gmail.com', '2020-02-25 14:31:56'),
	(179, 18, 'change10@gmail.com', '$2a$10$8mxSK0kdogcH/tNbjN28Re0kUo.rb1tYJoZynjhtmxfQbXiQ2nyPG', 'change10@gmail.com', '', '3', 'Y', '2020-02-25 14:59:10', 'change10@gmail.com', '2020-02-25 14:59:10', 'change10@gmail.com', '2020-02-25 14:59:10'),
	(180, 18, 'littleken@gmail.com', '$2a$10$mq1DZPy6wZmchr.bCWMt7uM6hlSxvE536Dxhws5MkiTkOK6LqvZA6', 'littleken@gmail.com', '', '3', 'Y', '2020-02-25 16:22:10', 'littleken@gmail.com', '2020-02-25 16:22:10', 'littleken@gmail.com', '2020-02-25 16:22:10'),
	(181, 18, 'gksqls415@gmail.com', '$2a$10$AwoXWMoDMEOpzKvE3HMWBeQyhUx93cV28OdQMGWWK8YSFHR5i1HEW', 'gksqls415@gmail.com', '', '3', 'Y', '2020-02-25 16:26:46', 'gksqls415@gmail.com', '2020-02-25 16:26:46', 'gksqls415@gmail.com', '2020-02-25 16:26:46'),
	(182, 18, 'jslim7725@gmail.com', '$2a$10$J1V29gy23O6Pvr8.IXafFuYt.KfbT4Kxy7J3Wl5S8U7skU8DtvKKa', 'jslim7725@gmail.com', '', '3', 'Y', '2020-02-25 16:44:55', 'jslim7725@gmail.com', '2020-02-25 16:44:55', 'jslim7725@gmail.com', '2020-02-25 16:44:55'),
	(183, 18, 'dkhan2050@gmail.com', '$2a$10$ZhKItkNd2rcRZNDoRjajduwxaxyh52gX/RYlQB6VPJgKAvp.0TV.a', 'dkhan2050@gmail.com', '', '3', 'Y', '2020-02-25 16:46:48', 'dkhan2050@gmail.com', '2020-02-25 16:46:48', 'dkhan2050@gmail.com', '2020-02-25 16:46:48'),
	(184, 18, 'lastchoice86@naver.com', '$2a$10$sBkYYG1Fuzu1kkSv3X/Gje0C9CFE4.rdKhbNhV.2Nq.f2nAYwY2F.', 'lastchoice86@naver.com', '', '3', 'Y', '2020-02-25 18:19:44', 'lastchoice86@naver.com', '2020-02-25 18:19:44', 'lastchoice86@naver.com', '2020-02-25 18:19:44'),
	(185, 18, 'kimn310@the-real.co.kr', '$2a$10$E.VngQvkzNFyV/qZZHbvcuvScjO8d5hkezT6oWKRQvAiemPnKA37W', 'kimn310@the-real.co.kr', '', '3', 'Y', '2020-02-26 02:25:17', 'kimn310@the-real.co.kr', '2020-02-26 02:25:17', 'kimn310@the-real.co.kr', '2020-02-26 02:25:17'),
	(186, 18, 'bosco@19media.net', '$2a$10$JKMfqNiskcOHRNCUA/loq.OSK4bsy5BGwU0x7kWk0KXLXw.ufLiA.', 'bosco@19media.net', '', '3', 'Y', '2020-02-26 08:19:41', 'bosco@19media.net', '2020-02-26 08:19:41', 'bosco@19media.net', '2020-02-26 08:19:41');
/*!40000 ALTER TABLE `MINUTES_USER` ENABLE KEYS */;

-- 테이블 minutes.MOD_RESULT 구조 내보내기
CREATE TABLE IF NOT EXISTS `MOD_RESULT` (
  `stt_meta_ser` int(11) NOT NULL DEFAULT '0' COMMENT 'STT META 일련번호',
  `minutes_mic_ser` int(11) NOT NULL DEFAULT '0' COMMENT 'MIC 일련번호',
  `order_no` int(11) NOT NULL DEFAULT '0' COMMENT '정렬순서',
  `sntnc_no` int(11) NOT NULL DEFAULT '0' COMMENT '문장번호',
  `minutes_employee_ser` int(11) DEFAULT NULL COMMENT '사원 일련번호',
  `sntnc_org` varchar(4000) DEFAULT NULL COMMENT '문장내용',
  `fin_yn` varchar(1) DEFAULT NULL COMMENT '종료여부YN',
  `mod_tree` varchar(4000) DEFAULT NULL COMMENT '수정정보',
  `sntnc_start_time` int(11) DEFAULT NULL COMMENT '문장종료시각',
  `sntnc_end_time` int(11) DEFAULT NULL COMMENT '문장종료시각',
  `stt_svr_id` varchar(20) DEFAULT NULL COMMENT 'STT서버ID',
  `create_user` varchar(100) DEFAULT NULL COMMENT '생성자',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '생성일자',
  `update_user` varchar(100) DEFAULT NULL COMMENT '갱신자',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '갱신일시',
  PRIMARY KEY (`stt_meta_ser`,`minutes_mic_ser`,`sntnc_no`,`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='녹음파일/화자별/문장 단위 시간 및 텍스트 정보';

-- 테이블 데이터 minutes.MOD_RESULT:~0 rows (대략적) 내보내기
DELETE FROM `MOD_RESULT`;
/*!40000 ALTER TABLE `MOD_RESULT` DISABLE KEYS */;
/*!40000 ALTER TABLE `MOD_RESULT` ENABLE KEYS */;

-- 테이블 minutes.STT_META 구조 내보내기
CREATE TABLE IF NOT EXISTS `STT_META` (
  `STT_META_SER` int(10) NOT NULL AUTO_INCREMENT,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `MINUTES_USER_SER` int(10) NOT NULL,
  `MINUTES_SITE_SER` int(10) NOT NULL,
  `MINUTES_MEETINGROOM` varchar(100) NOT NULL,
  `MINUTES_MACHINE` varchar(100) NOT NULL,
  `MINUTES_ID` varchar(100) NOT NULL,
  `MINUTES_LANG` varchar(10) NOT NULL DEFAULT 'KOR',
  `MINUTES_NAME` varchar(200) DEFAULT NULL,
  `MINUTES_START_DATE` datetime DEFAULT NULL,
  `MINUTES_TOPIC` varchar(4000) DEFAULT NULL,
  `MINUTES_JOINED_MEM` varchar(4000) DEFAULT NULL,
  `MINUTES_JOINED_CNT` int(11) DEFAULT '0',
  `MINUTES_STATUS` varchar(2) DEFAULT '0',
  `REC_SRC_CD` varchar(10) NOT NULL,
  `START_TIME` varchar(20) NOT NULL,
  `END_TIME` varchar(20) NOT NULL,
  `REC_TIME` int(10) NOT NULL,
  `MEMO` text,
  `SRC_FILE_PATH` varchar(300) DEFAULT NULL,
  `DST_FILE_PATH` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`STT_META_SER`),
  UNIQUE KEY `STT_META_UNIQUE` (`STT_META_SER`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8;

DELETE FROM `STT_META`;

-- 테이블 minutes.STT_MODEL 구조 내보내기
CREATE TABLE IF NOT EXISTS `STT_MODEL` (
  `STT_MODEL_SER` int(10) NOT NULL AUTO_INCREMENT,
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `DEEP_LEARNING_TYPE` varchar(10) NOT NULL DEFAULT 'LSTM',
  `STT_SERVER_IP` varchar(15) NOT NULL DEFAULT '127.0.0.1',
  `STT_SERVER_PORT` varchar(15) NOT NULL DEFAULT '9802',
  `STT_MODEL_NAME` varchar(50) NOT NULL DEFAULT 'baseline',
  `STT_MODEL_LANG` varchar(10) NOT NULL DEFAULT 'kor',
  `STT_MODEL_RATE` varchar(10) NOT NULL DEFAULT '8000',
  `MODEL_DESC` varchar(100) NOT NULL,
  PRIMARY KEY (`STT_MODEL_SER`),
  UNIQUE KEY `STT_MODEL_UNIQUE` (`STT_MODEL_SER`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- 테이블 데이터 minutes.STT_MODEL:~2 rows (대략적) 내보내기
DELETE FROM `STT_MODEL`;
/*!40000 ALTER TABLE `STT_MODEL` DISABLE KEYS */;
INSERT INTO `STT_MODEL` (`STT_MODEL_SER`, `CREATE_USER`, `CREATE_TIME`, `UPDATE_USER`, `UPDATE_TIME`, `DEEP_LEARNING_TYPE`, `STT_SERVER_IP`, `STT_SERVER_PORT`, `STT_MODEL_NAME`, `STT_MODEL_LANG`, `STT_MODEL_RATE`, `MODEL_DESC`) VALUES
	(1, 'KHY', '2020-01-03 11:00:05', 'admin', '2020-01-03 13:41:52', 'CNN', '10.122.64.152', '9802', 'CNN_BASE', 'kor', '8000 ', 'BASELINE CNN'),
	(2, 'KHY', '2020-01-03 10:43:18', 'admin', '2020-01-03 13:41:26', 'LSTM', '127.0.0.1', '16801', 'LSTM_BASE', 'kor', '8000 ', 'BASELINE LSTM');
/*!40000 ALTER TABLE `STT_MODEL` ENABLE KEYS */;

-- 테이블 minutes.STT_RESULT 구조 내보내기
CREATE TABLE IF NOT EXISTS `STT_RESULT` (
  `STT_RESULT_SER` int(10) NOT NULL AUTO_INCREMENT,
  `STT_META_SER` int(10) NOT NULL COMMENT 'STT_META의 STT_META_SER 과 매칭',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `MINUTES_EMPLOYEE` varchar(100) NOT NULL,
  `STT_ORG_RESULT` varchar(2048) NOT NULL,
  `STT_CHG_RESULT` varchar(2048) DEFAULT NULL,
  `STT_RESULT_START` datetime NOT NULL,
  `STT_RESULT_END` datetime NOT NULL,
  `STT_RESULT_START_FLOAT` float DEFAULT NULL,
  PRIMARY KEY (`STT_RESULT_SER`),
  UNIQUE KEY `STT_RESULT_UNIQUE` (`STT_RESULT_SER`)
) ENGINE=InnoDB AUTO_INCREMENT=306491 DEFAULT CHARSET=utf8;
-- 테이블 데이터 minutes.STT_RESULT:~249,805 rows (대략적) 내보내기
DELETE FROM `STT_RESULT`;

CREATE TABLE IF NOT EXISTS `SYSTEM_NAME` (
  `SYS_NM_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT '시스템리소스 이름 일련번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SYSTEM_NAME` varchar(100) NOT NULL COMMENT ' 서버정보',
  PRIMARY KEY (`SYS_NM_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 상태 정보 및 알람설정 정보';


CREATE TABLE IF NOT EXISTS `SYSTEM_RESOURCE_THRESHOLD` (
  `SYS_RSC_THR_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT '시스템리소스 경계값 일련번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATE_USER` varchar(100) DEFAULT NULL,
  `UPDATE_TIME` datetime DEFAULT NULL,
  `SYSTEM_NAME` varchar(100) NOT NULL COMMENT ' 서버정보',
  `SORTATION` varchar(100) NOT NULL COMMENT ' 장비 구분자',
  `THRESHOLD_MINOR` int(10) NOT NULL COMMENT ' 주의단계',
  `THRESHOLD_MAJOR` int(10) NOT NULL COMMENT ' 경고단계',
  `THRESHOLD_CRITICAL` int(10) NOT NULL COMMENT ' 위험단계',
  PRIMARY KEY (`SYS_RSC_THR_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 상태 정보 및 알람설정 정보';


CREATE TABLE IF NOT EXISTS `SYSTEM_RESOURCE` (
  `SYS_RSC_SER` int(10) NOT NULL AUTO_INCREMENT COMMENT '시스템리소스 일련번호',
  `CREATE_USER` varchar(100) NOT NULL,
  `CREATE_TIME` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SYSTEM_NAME` varchar(100) NOT NULL COMMENT '서버 정보 STT/TA/DB/WEB',
  `CPU` float NOT NULL COMMENT 'CPU 사용률(%)', 
  `MEM_TOTAL` bigint(16) NOT NULL COMMENT '메모리 총용량', 
  `MEM_USED` bigint(16) NOT NULL COMMENT '메모리 사용량',
  `DISK_TOTAL` bigint(16) NOT NULL COMMENT '저장공간 총용량', 
  `DISK_USED` bigint(16) NOT NULL COMMENT '저장공간 사용량',
  `GPU` float DEFAULT NULL COMMENT 'GPU 사용률(%)', 
  `GPU_TOTAL` bigint(16) DEFAULT NULL COMMENT 'GPU 총용량', 
  `GPU_USED` bigint(16) DEFAULT NULL COMMENT 'GPU 사용량',
  `GPU2` float DEFAULT NULL COMMENT 'GPU2 사용률(%)', 
  `GPU2_TOTAL` bigint(16) DEFAULT NULL COMMENT 'GPU2 총용량', 
  `GPU2_USED` bigint(16) DEFAULT NULL COMMENT 'GPU2 사용량',
  PRIMARY KEY (`SYS_RSC_SER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='서버 상태 정보 및 알람설정 정보';


DROP EVENT `EXPIRE_SYS_RSC`;

CREATE EVENT IF NOT EXISTS `EXPIRE_SYS_RSC`
	ON SCHEDULE
		EVERY 1 DAY
		STARTS '2020-03-06 01:00:00' 
	DO
		DELETE FROM minutes.SYS_RSC where CREATE_TIME <= date_sub(curdate(), INTERVAL 3 MONTH);
